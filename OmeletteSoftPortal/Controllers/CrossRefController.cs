﻿using OmeletteSoft.BLLayer;
using OmeletteSoft.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace OmeletteSoftPortal.Controllers
{
    [RoutePrefix("CrossRef")]
    public class CrossRefController : ApiController
    {
        [HttpPost]
        [Route("Search")]
        public async Task<CrossRefOutput> Search(SearchInput input)
        {
            CrossRefOutput output = new CrossRefOutput();
            output.Error = new ErrorInfo();
            output.results = new List<CrossRefResultItem>();

            try
            {
                IList<String> list = new List<String>();
                int pageSize = (input.Rows.HasValue ? (input.Rows.Value < ConstantMgr.PubMed.DefaultRows ? input.Rows.Value : ConstantMgr.PubMed.DefaultRows) : ConstantMgr.PubMed.DefaultRows);
                
                string url = String.Format("https://api.crossref.org/works?sort=published&order=desc&query.author={0}&rows={1}", input.Query, pageSize);
                
                WebRequest webRequest = WebRequest.Create(url);
                webRequest.Method = "GET";
                WebResponse webResp = await webRequest.GetResponseAsync();

                using (var reader = new StreamReader(webResp.GetResponseStream(), Encoding.UTF8))
                { 
                    dynamic response = Newtonsoft.Json.JsonConvert.DeserializeObject(reader.ReadToEnd());
                    if (response != null && response["message"] != null)
                    {
                        output.TotalRecords = Convert.ToInt32(response["message"]["total-results"]);
                        dynamic entries = response["message"]["items"];
                        if (entries != null)
                        {
                            if (entries.Count > 0)
                            {
                                foreach (dynamic entry in entries)
                                {
                                    var crossRefItem = new CrossRefResultItem();

                                    if (entry["title"] != null)
                                    {
                                        var title = Newtonsoft.Json.Linq.JArray.Parse(Convert.ToString(entry["title"]));
                                        if(title != null && title.Count > 0)
                                            crossRefItem.Title = Convert.ToString(title[0]);
                                    }
                                    
                                    crossRefItem.DOI = Convert.ToString(entry["DOI"]);
                                    crossRefItem.Volume = Convert.ToString(entry["volume"]);
                                    crossRefItem.Author = new List<string>();
                                    foreach (dynamic author in entry["author"])
                                    {
                                        crossRefItem.Author.Add(string.Format("{0} {1}", Convert.ToString(author["given"]), Convert.ToString(author["family"])));
                                    }

                                    crossRefItem.Page = Convert.ToString(entry["page"]);
                                    if (entry["short-container-title"] != null)
                                    {
                                        var shortTitle = Newtonsoft.Json.Linq.JArray.Parse(Convert.ToString(entry["short-container-title"]));
                                        if (shortTitle != null && shortTitle.Count > 0)
                                            crossRefItem.JournalName = Convert.ToString(shortTitle[0]);
                                    }

                                    crossRefItem.Issue = Convert.ToString(entry["issue"]);
                                    Newtonsoft.Json.Linq.JArray dateParts = null;
                                    if (entry["published-online"] != null)
                                    {
                                        dateParts = Newtonsoft.Json.Linq.JArray.Parse(Convert.ToString(entry["published-online"]["date-parts"]));
                                    }
                                    else
                                    {
                                        if (entry["published-print"] != null)
                                        {
                                            dateParts = Newtonsoft.Json.Linq.JArray.Parse(Convert.ToString(entry["published-print"]["date-parts"]));
                                        }
                                    }

                                    if(dateParts != null && dateParts.Count > 0){
                                        crossRefItem.PublishedOnline = string.Join("-", dateParts[0]);
                                    }
                                    
                                    crossRefItem.URL = Convert.ToString(entry["URL"]);
                                    
                                    if (entry["container-title"] != null)
                                    {
                                        var fullJournalName = Newtonsoft.Json.Linq.JArray.Parse(Convert.ToString(entry["container-title"]));
                                        if (fullJournalName != null && fullJournalName.Count > 0)
                                            crossRefItem.FullJournalName = Convert.ToString(fullJournalName[0]);
                                    }

                                    if (entry["ISSN"] != null)
                                    {
                                        var issn = Newtonsoft.Json.Linq.JArray.Parse(Convert.ToString(entry["ISSN"]));
                                        if (issn != null && issn.Count > 0)
                                            crossRefItem.ISSN = issn[0];


                                        if (entry["issn-type"] != null )
                                        {
                                            var issnType = Newtonsoft.Json.Linq.JArray.Parse(Convert.ToString(entry["issn-type"]));
                                            if(issnType != null && issnType.Count > 0 && crossRefItem.ISSN.Equals(Convert.ToString(issnType[0]["value"])))
                                            {
                                                crossRefItem.ISSNType = Convert.ToString(issnType[0]["type"]);
                                            }
                                            
                                        }
                                    }

                                    crossRefItem.ImpactFactor = "";

                                    output.results.Add(crossRefItem);
                                }

                                //Calculate impact factor
                                try
                                {
                                    var impactFactorList = CompanyMgr.GetImpactFactor(output.results.Where(e=> e.ISSNType != null && e.ISSNType.ToUpper().Equals("PRINT")).Select(e => e.ISSN).ToList(), "Print");
                                    foreach(var resultItem in output.results){
                                        var impactFactorItem = impactFactorList.Where(e2=> e2.ISSN == resultItem.ISSN).FirstOrDefault();
                                        if (impactFactorItem != null)
                                        {
                                            resultItem.ImpactFactor = impactFactorItem.JournalImpactFactor.ToString("####.###");
                                        }
                                    }

                                    //Look for missing ISSN
                                    impactFactorList = CompanyMgr.GetImpactFactor(output.results.Where(e => e.ISSNType != null && e.ISSNType.ToUpper().Equals("ELECTRONIC")).Select(e => e.ISSN).ToList(), "Electronic");
                                    foreach (var resultItem in output.results)
                                    {
                                        var impactFactorItem = impactFactorList.Where(e2 => e2.ISSN == resultItem.ISSN).FirstOrDefault();
                                        if (impactFactorItem != null)
                                        {
                                            resultItem.ImpactFactor = impactFactorItem.JournalImpactFactor.ToString("####.###");
                                        }
                                    }

                                    if (output.results != null)
                                    {

                                        //Look for DOI citation
                                        Parallel.ForEach(output.results, (item) =>
                                        {
                                            if (!string.IsNullOrEmpty(item.DOI))
                                            {
                                                var citationResult = OmeletteSoft.BLLayer.CitationMgr.GetCitationDetails(item.DOI);
                                                if (citationResult != null)
                                                {
                                                    item.Citation = new Citation();
                                                    item.Citation.TimesCited = Convert.ToString(citationResult.times_cited);
                                                    item.Citation.FieldCitationRatio = Convert.ToString(citationResult.relative_citation_ratio);
                                                }
                                            }
                                        });
                                    }
                                }
                                catch (Exception)
                                {
                                    //TODO : We will log impact factor errors
                                    ; 
                                }
                            }
                        }
                    }
                    else
                    {
                        output.Error.ErrorId = -1;
                        output.Error.ErrorDescription = "Failed to search CrossRef database.";
                    }
                }
            }
            catch (Exception ex)
            {
                output.Error.ErrorId = -1;
                output.Error.ErrorDescription = ex.Message;
            }

            return output;
        }

        [HttpPost]
        [Route("ContactUs")]
        public ErrorInfo SaveContactUs(MailInput mailInput)
        {
            ErrorInfo error = new ErrorInfo();
            try
            {
                error.ErrorId = 0;
                error.ErrorDescription = String.Empty;

                StringBuilder sbMailBody = new StringBuilder();
                sbMailBody.Append("<html><body>");
                sbMailBody.Append("<table>");
                sbMailBody.Append(String.Format("<tr><td>Name</td><td>{0}</td></tr>", mailInput.Name));
                sbMailBody.Append(String.Format("<tr><td>Email</td><td>{0}</td></tr>", mailInput.Email));
                sbMailBody.Append(String.Format("<tr><td>Comments</td><td>{0}</td></tr>", mailInput.Comments));
                sbMailBody.Append("{0}");
                sbMailBody.Append("</table>");
                sbMailBody.Append("</body></html>");

                var sbMailAdditionalInfo = new StringBuilder();
                sbMailAdditionalInfo.AppendFormat("<tr><td>Application Name:</td><td>{0}</td></tr>", (mailInput.AppName ?? "CrossRef"));
                sbMailAdditionalInfo.AppendFormat("<tr><td>Phone Type:</td><td>{0}</td></tr>", (mailInput.PhoneType ?? "NA"));

                var smtp = new System.Net.Mail.SmtpClient();
                var host = smtp.Host;
                var ssl = smtp.EnableSsl;
                var port = smtp.Port;

                var credential = (smtp.Credentials as System.Net.NetworkCredential);
                var username = credential.UserName;
                var password = credential.Password;

                OmeletteSoft.BLLayer.MailHelper.SendMail(ConfigMgr.ContactUS_Email, String.Concat("Contact Us request: ", mailInput.Name),
                    string.Format(sbMailBody.ToString(), sbMailAdditionalInfo.ToString()), ConfigMgr.CCList,
                    username, password);

                //Send a copy to actual user
                OmeletteSoft.BLLayer.MailHelper.SendMail(mailInput.Email, "Copy of Contact Us request Email",
                    string.Format(sbMailBody.ToString(), string.Empty), String.Empty,
                    username, password);
            }
            catch (Exception ex)
            {
                error.ErrorId = -1;
                error.ErrorDescription = ex.Message;
            }
            return error;
        }
    }
}
