﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using BLLayer;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using OmeletteSoft.BLLayer;
using OmeletteSoftPortal.Atrributes;
using OmeletteSoftPortal.Models;

namespace OmeletteSoftPortal.Controllers
{
    [UserIdAuthorizeAttribute]
    [System.Web.Http.RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private const string LocalLoginProvider = "Local";
        private ApplicationUserManager _userManager;
        private const int ACTIVAITON_CODE_LENGTH = 6;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }

        
        //// POST api/Account/Register
        [AllowAnonymous]
        [Route("Register")]
        public async Task<UserInfoOutput> Register(RegisterBindingModel model)
        {

            var output = new UserInfoOutput();
            if (!ModelState.IsValid)
            {
                output.Error.Code = -1;
                var sb = new StringBuilder();
                foreach (var item in ModelState)
                {
                    sb.AppendLine($"{item.Key}: {item.Value}");
                }
                output.Error.Message = sb.ToString();
                return output;
            }

            var user = new ApplicationUser() { UserName = model.Email, Email = model.Email };
            IdentityResult result = await UserManager.CreateAsync(user, model.Password);

            
            if (!result.Succeeded)
            {
                output.Error.Code = -1;
                output.Error.Message = string.Join(".", result.Errors);
                return output;
            }
            var userDetails = UserManager.FindByEmail(model.Email);
            
            PubMedMgr.SaveUserDetails(user.Id, model.Prefix, model.FirstName, model.MiddleName, model.LastName, null);
            return await ResendCode(userDetails.Id);
        }
        
        [AllowAnonymous]
        [HttpGet]
        [Route("ResendCode")]
        public async Task<UserInfoOutput> ResendCode(string id)
        {
            var output = new UserInfoOutput();
            if (string.IsNullOrEmpty(id))
            {
                output.Error.Code = -1;
                var sb = new StringBuilder();
                foreach (var item in ModelState)
                {
                    sb.AppendLine($"{item.Key}: {item.Value}");
                }
                output.Error.Message = sb.ToString();
                return output;
            }


            //Check if user is valid
            var applicationUser = UserManager.FindById(id);
            if (applicationUser == null)
            {
                output.Error.Code = -1;
                output.Error.Message = "Invalid User. Failed to identify User.";
                return output;
            }
            var saveActivationCodeResult = PubMedMgr.SaveActivationCode(id, CommonMgr.GenerateCode(ACTIVAITON_CODE_LENGTH));
            //Send Activation code
            try
            {
                var smtp = new System.Net.Mail.SmtpClient();
                var host = smtp.Host;
                var ssl = smtp.EnableSsl;
                var port = smtp.Port;

                var credential = (smtp.Credentials as System.Net.NetworkCredential);
                var username = credential.UserName;
                var password = credential.Password;

                MailHelper.SendMail(applicationUser.UserName, "PubMed Activation Code", $"<html><body>Activation code for PubMed: <h1>{saveActivationCodeResult.ActivationCode}</h1></body></html>", string.Empty,
                    username, password);
            }
            catch (Exception)
            {

            }
            finally { }

            var userDeails = PubMedMgr.GetUserDetails(applicationUser.Id);
            output.UserInfo = new UserInfoViewModel2
            {
                Email = applicationUser.UserName,
                IsActivated = applicationUser.EmailConfirmed,
                UserId = applicationUser.Id,
                Prefix = userDeails?.Prefix,
                FirstName = userDeails?.FirstName,
                MiddleName = userDeails?.MiddleName,
                LastName = userDeails?.LastName
            };
            return output;
        }

        //// POST api/Account/Logout
        [Route("Logout")]
        public IHttpActionResult Logout()
        {
            Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            return Ok();
        }
        //// POST api/Account/ChangePassword
        [Route("ChangePassword")]
        public async Task<Error> ChangePassword(ChangePasswordBindingModel model)
        {
            var error = new Error();
            if (!ModelState.IsValid)
            {
                error.Code = -1;
                var sb = new StringBuilder();
                foreach (var item in ModelState)
                {
                    sb.AppendLine($"{item.Key}: {item.Value}");
                }
                error.Message = sb.ToString();
                return error;
            }

            IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword,
                model.NewPassword);

            if (!result.Succeeded)
            {
                error.Code = -1;
                error.Message = string.Join(".", result.Errors);
                return error;
            }

            return error;
        }

        // GET api/Account/UserInfo
        [Route("UserInfo")]
        public UserInfoOutput GetUserInfo()
        {
            var output = new UserInfoOutput();
            var userLoginDetails = UserManager.FindById(User.Identity.GetUserId());
            var userDetails = PubMedMgr.GetUserDetails(User.Identity.GetUserId());
            output.UserInfo =  new UserInfoViewModel2
            {
                Email = User.Identity.GetUserName(),
                IsActivated = userLoginDetails.EmailConfirmed,
                UserId = userLoginDetails.Id,
                Prefix = userDetails?.Prefix,
                FirstName = userDetails?.FirstName,
                MiddleName = userDetails?.MiddleName,
                LastName = userDetails?.LastName,
                ImagePath = string.IsNullOrEmpty(userDetails?.ImagePath) ? string.Empty : Url.Content(userDetails?.ImagePath)
            };

            return output;
        }

        [HttpPost]
        [Route("VerifyCode")]
        [AllowAnonymous]
        public async Task<Error> VerifyCode(VerifyCodeViewBindingModel model)
        {
            var error = new Error();
            if (!ModelState.IsValid)
            {
                error.Code = -1;
                var sb = new StringBuilder();
                foreach (var item in ModelState)
                {
                    sb.AppendLine($"{item.Key}: {item.Value}");
                }
                error.Message = sb.ToString();
                return error;
            }

            var result = BLLayer.PubMedMgr.MarkActivationCode(model.UserId, model.Code);

            if(result != null)
            {
                var userDetails = UserManager.FindById(model.UserId);
                if(userDetails != null)
                {
                    userDetails.EmailConfirmed = true;
                    var userUpdateResult = UserManager.Update(userDetails);
                    if (userUpdateResult.Succeeded)
                    {
                        error.Code = 0;
                        error.Message = "Code validated successfully.";
                        return error;
                    }
                    else
                    {
                        error.Code = -1;
                        error.Message = string.Join(".", userUpdateResult.Errors);
                        return error;
                    }
                        
                }
                else
                {
                    error.Code = -1;
                    error.Message = "Failed to retrieve User details.";
                    return error;
                }
            }
            else
            {
                error.Code = -1;
                error.Message = "Invalid User / Code.";
                return error;
            }
        }
        
        [HttpPost]
        [Route("Update")]
        public async Task<UserInfoOutput> UpdateUserInfo(UserDetailsBindingModel model)
        {
            var output = new UserInfoOutput();

            if (!ModelState.IsValid)
            {
                output.Error.Code = -1;
                var sb = new StringBuilder();
                foreach (var item in ModelState)
                {
                    sb.AppendLine($"{item.Key}: {item.Value}");
                }
                output.Error.Message = sb.ToString();
                return output;
            }

            var userDetails = PubMedMgr.SaveUserDetails(User.Identity.GetUserId(), model.Prefix, model.FirstName, model.MiddleName, model.LastName, model.ImageId);
            if (!string.IsNullOrEmpty(userDetails.ImagePath))
                userDetails.ImagePath = Url.Content(userDetails.ImagePath);

            output.UserInfo = new UserInfoViewModel2() { 
                FirstName = userDetails.FirstName,
                ImagePath = userDetails.ImagePath,
                LastName = userDetails.LastName,
                MiddleName = userDetails.MiddleName,
                Prefix = userDetails.Prefix,
                UserId = User.Identity.GetUserId(),
                Email = User.Identity.GetUserName()
            };
            return output;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("Validate")]
        public async Task<ValidateOutput> ValidateUser(ValidateBindingModel model)
        {
            var output = new ValidateOutput();
            if (!ModelState.IsValid)
            {
                output.Error.Code = -1;
                var sb = new StringBuilder();
                foreach (var item in ModelState)
                {
                    sb.AppendLine($"{item.Key}: {item.Value}");
                }
                output.Error.Message = sb.ToString();
                return output;
            }

            try
            {
                var userDetails = UserManager.FindByEmail(model.Email);
                var validUser = UserManager.CheckPassword(userDetails, model.Password);
                if (validUser)
                {
                    var tempUserInfo = PubMedMgr.GetUserDetails(userDetails.Id);
                    output.UserInfo = new UserInfo()
                    {
                        AspnetUserId = tempUserInfo.AspnetUserId,
                        CreatedDate = tempUserInfo.CreatedDate,
                        FirstName = tempUserInfo.FirstName,
                        ImagePath = !string.IsNullOrEmpty(tempUserInfo.ImagePath) ? Url.Content(tempUserInfo.ImagePath) : string.Empty,
                        LastName = tempUserInfo.LastName,
                        MiddleName = tempUserInfo.MiddleName,
                        ModifiedDate = tempUserInfo.ModifiedDate,
                        Prefix = tempUserInfo.Prefix,
                        EmailConfirmed = userDetails.EmailConfirmed
                    };
                    return output;
                }
                else
                {
                    output.Error.Code = -1;
                    output.Error.Message = "Failed to Validate User.";
                }
            }
            catch (Exception)
            {
                output.Error.Code = -1;
                output.Error.Message = "Server Error. Please try again.";
            }

            return output;
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("ForgotPasswordCode")]
        public async Task<Error> ForgotPasswordCode(string email)
        {
            var errorCode = new Error();
            if (string.IsNullOrEmpty(email))
            {
                errorCode.Code = -1;
                errorCode.Message = "Invalid Input";
                return errorCode;
            }

            //Check if user is valid
            var applicationUser = UserManager.FindByEmail(email);
            if (applicationUser == null)
            {
                errorCode.Code = -1;
                errorCode.Message = "Invalid User. Failed to identify User.";
                return errorCode;
            }

            var saveActivationCodeResult = PubMedMgr.SaveActivationCode(applicationUser.Id, CommonMgr.GenerateCode(ACTIVAITON_CODE_LENGTH));
            //Send Activation code
            try
            {
                var smtp = new System.Net.Mail.SmtpClient();
                var host = smtp.Host;
                var ssl = smtp.EnableSsl;
                var port = smtp.Port;

                var credential = (smtp.Credentials as System.Net.NetworkCredential);
                var username = credential.UserName;
                var password = credential.Password;

                MailHelper.SendMail(applicationUser.UserName, "PubMed Hub: Forgot Password Code", $"<html><body>Forgot Password One Time Code for PubMed Hub: <h1>{saveActivationCodeResult.ActivationCode}</h1></body></html>", string.Empty,
                    username, password, ssl, port, host);
            }
            catch (Exception ex)
            {
                errorCode.Code = -1;
                errorCode.Message = $"Failed to send email. {ex.Message}. {ex.InnerException?.Message}";
                return errorCode;
            }
            finally { }

            errorCode.Message = "Please check for your Email for One Time Code and use it for Forgot Password screen.";
            return errorCode;
        }

        [HttpPost]
        [Route("SetPassword")]
        [AllowAnonymous]
        public async Task<Error> SetPassword(SetPasswordViewBindingModel model)
        {
            var result = new Error();
            if (!ModelState.IsValid)
            {
                result.Code = -1;
                var sb = new StringBuilder();
                foreach (var item in ModelState)
                {
                    sb.AppendLine($"{item.Key}: {item.Value}");
                }
                result.Message = sb.ToString();
                return result;
            }
            //Check if user is valid
            var userDetails = UserManager.FindByEmail(model.Email);
            if (userDetails == null)
            {
                result.Code = -1;
                result.Message = "Invalid User. Failed to identify User.";
                return result;
            }

            var activationResult = BLLayer.PubMedMgr.MarkActivationCode(userDetails.Id, model.Code);

            if (activationResult != null)
            {
                string resetToken = await UserManager.GeneratePasswordResetTokenAsync(userDetails.Id);
                IdentityResult passwordChangeResult = await UserManager.ResetPasswordAsync(userDetails.Id, resetToken, model.NewPassword);

                if (passwordChangeResult.Succeeded)
                {
                    result.Code = 0;
                    result.Message = "Password updated successfully.";
                    return result;
                }
                else
                {
                    result.Code = -1;
                    result.Message = string.Join(". ", passwordChangeResult.Errors);
                    return result;
                }
            }
            else
            {
                result.Code = -1;
                result.Message = "Invalid Code. Please check your Email for Code.";
                return result;
            }
        }

        #region Commented Methods


        //// POST api/Account/SetPassword
        //[Route("SetPassword")]
        //public async Task<IHttpActionResult> SetPassword(SetPasswordBindingModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);

        //    if (!result.Succeeded)
        //    {
        //        return GetErrorResult(result);
        //    }

        //    return Ok();
        //}

        // GET api/Account/UserInfo
        //[HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        //[Route("UserInfo")]
        //public UserInfoViewModel GetUserInfo()
        //{
        //    ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

        //    return new UserInfoViewModel
        //    {
        //        Email = User.Identity.GetUserName(),
        //        HasRegistered = externalLogin == null,
        //        LoginProvider = externalLogin != null ? externalLogin.LoginProvider : null
        //    };
        //}


        //// GET api/Account/ManageInfo?returnUrl=%2F&generateState=true
        //[Route("ManageInfo")]
        //public async Task<ManageInfoViewModel> GetManageInfo(string returnUrl, bool generateState = false)
        //{
        //    IdentityUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

        //    if (user == null)
        //    {
        //        return null;
        //    }

        //    List<UserLoginInfoViewModel> logins = new List<UserLoginInfoViewModel>();

        //    foreach (IdentityUserLogin linkedAccount in user.Logins)
        //    {
        //        logins.Add(new UserLoginInfoViewModel
        //        {
        //            LoginProvider = linkedAccount.LoginProvider,
        //            ProviderKey = linkedAccount.ProviderKey
        //        });
        //    }

        //    if (user.PasswordHash != null)
        //    {
        //        logins.Add(new UserLoginInfoViewModel
        //        {
        //            LoginProvider = LocalLoginProvider,
        //            ProviderKey = user.UserName,
        //        });
        //    }

        //    return new ManageInfoViewModel
        //    {
        //        LocalLoginProvider = LocalLoginProvider,
        //        Email = user.UserName,
        //        Logins = logins,
        //        ExternalLoginProviders = GetExternalLogins(returnUrl, generateState)
        //    };
        //}


        //// POST api/Account/AddExternalLogin
        //[Route("AddExternalLogin")]
        //public async Task<IHttpActionResult> AddExternalLogin(AddExternalLoginBindingModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);

        //    AuthenticationTicket ticket = AccessTokenFormat.Unprotect(model.ExternalAccessToken);

        //    if (ticket == null || ticket.Identity == null || (ticket.Properties != null
        //        && ticket.Properties.ExpiresUtc.HasValue
        //        && ticket.Properties.ExpiresUtc.Value < DateTimeOffset.UtcNow))
        //    {
        //        return BadRequest("External login failure.");
        //    }

        //    ExternalLoginData externalData = ExternalLoginData.FromIdentity(ticket.Identity);

        //    if (externalData == null)
        //    {
        //        return BadRequest("The external login is already associated with an account.");
        //    }

        //    IdentityResult result = await UserManager.AddLoginAsync(User.Identity.GetUserId(),
        //        new UserLoginInfo(externalData.LoginProvider, externalData.ProviderKey));

        //    if (!result.Succeeded)
        //    {
        //        return GetErrorResult(result);
        //    }

        //    return Ok();
        //}

        //// POST api/Account/RemoveLogin
        //[Route("RemoveLogin")]
        //public async Task<IHttpActionResult> RemoveLogin(RemoveLoginBindingModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    IdentityResult result;

        //    if (model.LoginProvider == LocalLoginProvider)
        //    {
        //        result = await UserManager.RemovePasswordAsync(User.Identity.GetUserId());
        //    }
        //    else
        //    {
        //        result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(),
        //            new UserLoginInfo(model.LoginProvider, model.ProviderKey));
        //    }

        //    if (!result.Succeeded)
        //    {
        //        return GetErrorResult(result);
        //    }

        //    return Ok();
        //}

        //// GET api/Account/ExternalLogin
        //[OverrideAuthentication]
        //[HostAuthentication(DefaultAuthenticationTypes.ExternalCookie)]
        //[AllowAnonymous]
        //[Route("ExternalLogin", Name = "ExternalLogin")]
        //public async Task<IHttpActionResult> GetExternalLogin(string provider, string error = null)
        //{
        //    if (error != null)
        //    {
        //        return Redirect(Url.Content("~/") + "#error=" + Uri.EscapeDataString(error));
        //    }

        //    if (!User.Identity.IsAuthenticated)
        //    {
        //        return new ChallengeResult(provider, this);
        //    }

        //    ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

        //    if (externalLogin == null)
        //    {
        //        return InternalServerError();
        //    }

        //    if (externalLogin.LoginProvider != provider)
        //    {
        //        Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
        //        return new ChallengeResult(provider, this);
        //    }

        //    ApplicationUser user = await UserManager.FindAsync(new UserLoginInfo(externalLogin.LoginProvider,
        //        externalLogin.ProviderKey));

        //    bool hasRegistered = user != null;

        //    if (hasRegistered)
        //    {
        //        Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);

        //         ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(UserManager,
        //            OAuthDefaults.AuthenticationType);
        //        ClaimsIdentity cookieIdentity = await user.GenerateUserIdentityAsync(UserManager,
        //            CookieAuthenticationDefaults.AuthenticationType);

        //        AuthenticationProperties properties = ApplicationOAuthProvider.CreateProperties(user.UserName);
        //        Authentication.SignIn(properties, oAuthIdentity, cookieIdentity);
        //    }
        //    else
        //    {
        //        IEnumerable<Claim> claims = externalLogin.GetClaims();
        //        ClaimsIdentity identity = new ClaimsIdentity(claims, OAuthDefaults.AuthenticationType);
        //        Authentication.SignIn(identity);
        //    }

        //    return Ok();
        //}

        //// GET api/Account/ExternalLogins?returnUrl=%2F&generateState=true
        //[AllowAnonymous]
        //[Route("ExternalLogins")]
        //public IEnumerable<ExternalLoginViewModel> GetExternalLogins(string returnUrl, bool generateState = false)
        //{
        //    IEnumerable<AuthenticationDescription> descriptions = Authentication.GetExternalAuthenticationTypes();
        //    List<ExternalLoginViewModel> logins = new List<ExternalLoginViewModel>();

        //    string state;

        //    if (generateState)
        //    {
        //        const int strengthInBits = 256;
        //        state = RandomOAuthStateGenerator.Generate(strengthInBits);
        //    }
        //    else
        //    {
        //        state = null;
        //    }

        //    foreach (AuthenticationDescription description in descriptions)
        //    {
        //        ExternalLoginViewModel login = new ExternalLoginViewModel
        //        {
        //            Name = description.Caption,
        //            Url = Url.Route("ExternalLogin", new
        //            {
        //                provider = description.AuthenticationType,
        //                response_type = "token",
        //                client_id = Startup.PublicClientId,
        //                redirect_uri = new Uri(Request.RequestUri, returnUrl).AbsoluteUri,
        //                state = state
        //            }),
        //            State = state
        //        };
        //        logins.Add(login);
        //    }

        //    return logins;
        //}



        //// POST api/Account/RegisterExternal
        //[OverrideAuthentication]
        //[HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        //[Route("RegisterExternal")]
        //public async Task<IHttpActionResult> RegisterExternal(RegisterExternalBindingModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var info = await Authentication.GetExternalLoginInfoAsync();
        //    if (info == null)
        //    {
        //        return InternalServerError();
        //    }

        //    var user = new ApplicationUser() { UserName = model.Email, Email = model.Email };

        //    IdentityResult result = await UserManager.CreateAsync(user);
        //    if (!result.Succeeded)
        //    {
        //        return GetErrorResult(result);
        //    }

        //    result = await UserManager.AddLoginAsync(user.Id, info.Login);
        //    if (!result.Succeeded)
        //    {
        //        return GetErrorResult(result); 
        //    }
        //    return Ok();
        //}

        #endregion
        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

        #region Helpers

        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        private class ExternalLoginData
        {
            public string LoginProvider { get; set; }
            public string ProviderKey { get; set; }
            public string UserName { get; set; }

            public IList<Claim> GetClaims()
            {
                IList<Claim> claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.NameIdentifier, ProviderKey, null, LoginProvider));

                if (UserName != null)
                {
                    claims.Add(new Claim(ClaimTypes.Name, UserName, null, LoginProvider));
                }

                return claims;
            }

            public static ExternalLoginData FromIdentity(ClaimsIdentity identity)
            {
                if (identity == null)
                {
                    return null;
                }

                Claim providerKeyClaim = identity.FindFirst(ClaimTypes.NameIdentifier);

                if (providerKeyClaim == null || String.IsNullOrEmpty(providerKeyClaim.Issuer)
                    || String.IsNullOrEmpty(providerKeyClaim.Value))
                {
                    return null;
                }

                if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
                {
                    return null;
                }

                return new ExternalLoginData
                {
                    LoginProvider = providerKeyClaim.Issuer,
                    ProviderKey = providerKeyClaim.Value,
                    UserName = identity.FindFirstValue(ClaimTypes.Name)
                };
            }
        }

        private static class RandomOAuthStateGenerator
        {
            private static RandomNumberGenerator _random = new RNGCryptoServiceProvider();

            public static string Generate(int strengthInBits)
            {
                const int bitsPerByte = 8;

                if (strengthInBits % bitsPerByte != 0)
                {
                    throw new ArgumentException("strengthInBits must be evenly divisible by 8.", "strengthInBits");
                }

                int strengthInBytes = strengthInBits / bitsPerByte;

                byte[] data = new byte[strengthInBytes];
                _random.GetBytes(data);
                return HttpServerUtility.UrlTokenEncode(data);
            }
        }

        #endregion

        #region AppUser Methods
        //[System.Web.Http.HttpPost]
        //[System.Web.Http.Route("Register")]
        //public Error RegisterAppUser(AppUser input)
        //{
        //    var output = new Error();
        //    try
        //    {
        //        EnumMgr.ApplicationEnum application;
        //        if (!Enum.TryParse(input.Application, out application))
        //        {
        //            application = EnumMgr.ApplicationEnum.PubMed;
        //        }
        //        var registerAppUserOutput = AppUserMgr.RegisterAppUser(input.LastName, input.FirstName, input.Email, (short)application, 
        //            input.Prefix, input.ImageId,
        //            input.DeviceId, input.DeviceInfo);
        //        if(registerAppUserOutput.Verified.Value == false)
        //        {
        //            return SendActiviationEmail(input);
        //        }
        //    }
        //    catch(Exception ex)
        //    {
        //        output.Code = -1;
        //        output.Message = "Error occured while registering User.";
        //    }

        //    return output;
        //}

        //[System.Web.Http.HttpPost]
        //[System.Web.Http.Route("RegisterDevice")]
        //public Error SendActiviationEmail(AppUser input)
        //{
        //    var output = new Error();
        //    try
        //    {
        //        EnumMgr.ApplicationEnum application;
        //        if(!Enum.TryParse(input.Application, out application))
        //        {
        //            application = EnumMgr.ApplicationEnum.PubMed;
        //        }
        //        var saveAppUserDeviceOutput = AppUserMgr.SaveAppUserDevice((short)application, input.Email, input.DeviceId, input.DeviceInfo);
        //        if (saveAppUserDeviceOutput.Verified.Value == false)
        //        {
        //            //var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);

        //            //var emailConfirmationToken = Url.Content("~/Home/ConfirmEmail?Id=") + AppUserMgr.GenerateEmailConfirmationToken(saveAppUserDeviceOutput.AppUserDeviceId);
        //            ////Send activation email
        //            //StringBuilder sbMailBody = new StringBuilder();
        //            //sbMailBody.Append("<html><body>");
        //            //sbMailBody.Append("<table>");
        //            //sbMailBody.Append(String.Format("<tr><td>Please click <a href='{0}'>Here</a> for activating new Device.", emailConfirmationToken));
        //            //sbMailBody.Append("</table>");
        //            //sbMailBody.Append("</body></html>");

        //            //var smtp = new System.Net.Mail.SmtpClient();
        //            //var host = smtp.Host;
        //            //var ssl = smtp.EnableSsl;
        //            //var port = smtp.Port;

        //            //var credential = (smtp.Credentials as System.Net.NetworkCredential);
        //            //var username = credential.UserName;
        //            //var password = credential.Password;

        //            //MailHelper.SendMail(input.Email, $"Device Activation Email for {input.Application}",
        //            //    sbMailBody.ToString(), string.Empty,
        //            //    username, password);

        //            //output.Code = 0;
        //            //output.Message = "Please check your email for Activation Link.";

        //        }
        //    }
        //    catch (Exception)
        //    {
        //        output.Code = -1;
        //        output.Message = "Failed to send Activation Email.";
        //    }

        //    return output;
        //}

        //[System.Web.Http.HttpPost]
        //[System.Web.Http.Route("Validate")]
        //public AppUserOutput ValidateAppUser(AppUser input)
        //{
        //    var output = new AppUserOutput();
        //    try
        //    {
        //        EnumMgr.ApplicationEnum application;
        //        if (!Enum.TryParse(input.Application, out application))
        //        {
        //            application = EnumMgr.ApplicationEnum.PubMed;
        //        }

        //        var validateResult = AppUserMgr.ValidateAppUser(input.Email, (short)application, input.DeviceId);
        //        if(validateResult == null)
        //        {
        //            output.ErrorDetails.Code = -1;
        //            output.ErrorDetails.Message = "Invalid Email/DeviceId. Please use RegisterDevice route for adding New device.";
        //            return output;

        //        }
        //        if(validateResult.IsActive.Value == true){
        //            output.UserInfo.Application = input.Application;
        //            output.UserInfo.Email = validateResult.EmailAddress;
        //            output.UserInfo.FirstName = validateResult.FirstName;
        //            output.UserInfo.LastName = validateResult.LastName;
        //            output.UserInfo.UserId = validateResult.UserId.Value.ToString();
        //            output.UserInfo.AppDeviceId = validateResult.AppUserDeviceId;
        //            output.UserInfo.AppUserId = validateResult.AppUserId;
        //        }
        //        else
        //        {
        //            output.ErrorDetails.Code = -1;
        //            output.ErrorDetails.Message = "Failed to validate User.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        output.ErrorDetails.Code = -1;
        //        output.ErrorDetails.Message = "Error occured while validating User.";
        //    }

        //    return output;
        //}

        //[System.Web.Http.HttpPost]
        //[System.Web.Http.Route("Update")]
        //public AppUserOutput UpdateAppUserDetails(AppUser input)
        //{
        //    var output = new AppUserOutput();
        //    try
        //    {
        //        EnumMgr.ApplicationEnum application;
        //        if (!Enum.TryParse(input.Application, out application))
        //        {
        //            application = EnumMgr.ApplicationEnum.PubMed;
        //        }

        //        var validateResult = AppUserMgr.ValidateAppUser(input.Email, (short)application, input.DeviceId);
        //        if (validateResult.IsActive.Value == true)
        //        {
        //            AppUserMgr.UpdateAppUserDetails(input.LastName, input.FirstName, input.Email, (short) application, input.Prefix, input.ImageId);
        //            validateResult = AppUserMgr.ValidateAppUser(input.Email, (short)application, input.DeviceId);
        //            output.UserInfo.Application = input.Application;
        //            output.UserInfo.Email = validateResult.EmailAddress;
        //            output.UserInfo.FirstName = validateResult.FirstName;
        //            output.UserInfo.LastName = validateResult.LastName;
        //            output.UserInfo.UserId = validateResult.UserId.Value.ToString();
        //        }
        //        else
        //        {
        //            output.ErrorDetails.Code = -1;
        //            output.ErrorDetails.Message = "Failed to update User details.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        output.ErrorDetails.Code = -1;
        //        output.ErrorDetails.Message = "Error occured while updating User details.";
        //    }

        //    return output;
        //}

        #endregion
    }
}
