﻿using DALayer;
using Newtonsoft.Json;
using OmeletteSoft.BLLayer;
using OmeletteSoft.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace OmeletteSoft.Controllers
{
    [RoutePrefix("api")]
    public class CompanyController : ApiController
    {
        /// <summary>
        /// Not required for Mobile
        /// </summary>
        /// <param name="inputRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("CompanyInfo")]
        public Company GetCompanyInfo(InputRequest inputRequest)
        {
            Company company = new Company();
            company.Error = new ErrorInfo();

            try
            {
                GetCompanyInfo_Result result = CompanyMgr.GetCompanyInfo(inputRequest.CompanyId);
                if (result != null)
                {
                    company.Address = result.Address;
                    company.andriodEnabled = result.andriodEnabled;
                    company.CompanyId = result.Code;
                    company.Email = result.Email;
                    company.iPhoneEnabled = result.iPhoneEnabled;
                    company.Name = result.Name;
                    company.Phone = result.Phone;
                }

                company.Error.ErrorId = 0;
                company.Error.ErrorDescription = String.Empty;
            }
            catch (Exception ex)
            {
                company.Error.ErrorId = -1;
                company.Error.ErrorDescription = ex.Message;
            }


            return company;
        }

        /// <summary>
        /// Used for retrieving List of Journals
        /// </summary>
        /// <param name="inputRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Journals")]
        public JournalInfoOutput GetCompanyJournals(InputRequest inputRequest)
        {
            JournalInfoOutput journalOutput = new JournalInfoOutput();
            journalOutput.Error = new ErrorInfo();

            try
            {
                journalOutput.Journals = (from j in CompanyMgr.GetCompanyJournals(inputRequest.CompanyId)
                                          select new CompanyJournal()
                                          {
                                              JournalId = j.JournalId,
                                              JournalName = j.JournalName
                                          }).ToList();

                journalOutput.StaticLinks = (from l in CompanyMgr.GetCompanyLinks(inputRequest.CompanyId)
                                             select new StaticLink()
                                             {
                                                 LinkName = l.LinkName,
                                                 LinkDetails = l.LinkDetails
                                             }).ToList();

                journalOutput.Error.ErrorId = 0;
                journalOutput.Error.ErrorDescription = String.Empty;
            }
            catch (Exception ex)
            {
                journalOutput.Error.ErrorId = -1;
                journalOutput.Error.ErrorDescription = ex.Message;
            }

            return journalOutput;
        }

        /// <summary>
        /// Used for retrieving Journal Details (Input1 = JournalId)
        /// </summary>
        /// <param name="inputRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("JournalInfo")]
        public async Task<JournalDetailsOutput> GetJournalDetails(InputRequest inputRequest)
        {
            JournalDetailsOutput journalDetailsOutput = new JournalDetailsOutput();
            journalDetailsOutput.Error = new ErrorInfo();
            try
            {
                GetCompanyInfo_Result company = CompanyMgr.GetCompanyInfo(inputRequest.CompanyId);

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(company.ServiceEndPoint);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    // New code:
                    HttpResponseMessage response = await client.GetAsync(String.Concat(inputRequest.Input1, ConstantMgr.REST_ROUTE_SERVICE, ConstantMgr.Routes.JOURNAL_INFO));
                    if (response.IsSuccessStatusCode)
                    {
                        //journalDetailsOutput.JournalDetails = await response.Content.ReadAsAsync<JournalDetails>();
                        journalDetailsOutput.JournalDetails = JsonConvert.DeserializeObject<JournalDetails>(CommonMgr.StripTagsCharArray(await response.Content.ReadAsStringAsync()));
                        response.EnsureSuccessStatusCode();
                    }
                }

                journalDetailsOutput.Error.ErrorId = 0;
                journalDetailsOutput.Error.ErrorDescription = String.Empty;
            }
            catch (Exception ex)
            {
                journalDetailsOutput.Error.ErrorId = -1;
                journalDetailsOutput.Error.ErrorDescription = ex.Message;
            }

            return journalDetailsOutput;
        }

        [HttpPost]
        [Route("CurrentIssueData")]
        public async Task<CurrentIssueDataOutput> GetCurrentIssueData(InputRequest inputRequest)
        {
            CurrentIssueDataOutput currentIssueDataOutput = new CurrentIssueDataOutput();
            currentIssueDataOutput.Error = new ErrorInfo();
            try
            {
                GetCompanyInfo_Result company = CompanyMgr.GetCompanyInfo(inputRequest.CompanyId);

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(company.ServiceEndPoint);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    // New code:
                    HttpResponseMessage response = await client.GetAsync(String.Concat(inputRequest.Input1, ConstantMgr.REST_ROUTE_SERVICE, ConstantMgr.Routes.CURRENT_ISSUE_DATA));
                    if (response.IsSuccessStatusCode)
                    {
                        currentIssueDataOutput.CurrentIssueData = JsonConvert.DeserializeObject<CurrentIssueData>(CommonMgr.StripTagsCharArray(await response.Content.ReadAsStringAsync()));
                    }
                }

                currentIssueDataOutput.Error.ErrorId = 0;
                currentIssueDataOutput.Error.ErrorDescription = String.Empty;
            }
            catch (Exception ex)
            {
                currentIssueDataOutput.Error.ErrorId = -1;
                currentIssueDataOutput.Error.ErrorDescription = ex.Message;
            }

            return currentIssueDataOutput;
        }

        /// <summary>
        /// Used for retrieving Current Isssue of the Journal and related articles
        /// </summary>
        /// <param name="inputRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("CurrentIssueWithArticles")]
        public async Task<CurrentIssueWithArticlesOutput> GetCurrentIssueWithArticles(InputRequest inputRequest)
        {
            CurrentIssueWithArticlesOutput currentIssueWithArticlesOutput = new CurrentIssueWithArticlesOutput();
            currentIssueWithArticlesOutput.Error = new ErrorInfo();
            try
            {
                GetCompanyInfo_Result company = CompanyMgr.GetCompanyInfo(inputRequest.CompanyId);

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(company.ServiceEndPoint);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    // New code:
                    HttpResponseMessage response = await client.GetAsync(String.Concat(inputRequest.Input1, ConstantMgr.REST_ROUTE_SERVICE, ConstantMgr.Routes.CURRENT_ISSUE_DATA_WITH_ARTICLES));
                    if (response.IsSuccessStatusCode)
                    {
                        //currentIssueWithArticlesOutput.CurrentIssueWithArticles = await response.Content.ReadAsAsync<CurrentIssueWithArticles>();
                        currentIssueWithArticlesOutput.CurrentIssueWithArticles = JsonConvert.DeserializeObject<CurrentIssueWithArticles>(CommonMgr.StripTagsCharArray(await response.Content.ReadAsStringAsync()));
                    }
                }

                currentIssueWithArticlesOutput.Error.ErrorId = 0;
                currentIssueWithArticlesOutput.Error.ErrorDescription = String.Empty;
            }
            catch (Exception ex)
            {
                currentIssueWithArticlesOutput.Error.ErrorId = -1;
                currentIssueWithArticlesOutput.Error.ErrorDescription = ex.Message;
            }

            return currentIssueWithArticlesOutput;
        }

        /// <summary>
        /// Not in use for Mobile
        /// </summary>
        /// <param name="inputRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("AllIssueData")]
        public async Task<IssuesDataOutput> GetIssuesData(InputRequest inputRequest)
        {
            IssuesDataOutput issueDataOutput = new IssuesDataOutput();
            issueDataOutput.Error = new ErrorInfo();
            try
            {
                GetCompanyInfo_Result company = CompanyMgr.GetCompanyInfo(inputRequest.CompanyId);

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(company.ServiceEndPoint);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.PostAsync(String.Concat(inputRequest.Input1, ConstantMgr.REST_ROUTE_SERVICE, ConstantMgr.Routes.ALL_ISSUE_DATA), null);
                    if (response.IsSuccessStatusCode)
                    {
                        issueDataOutput.IssueData = JsonConvert.DeserializeObject<List<IssueData>>(CommonMgr.StripTagsCharArray(await response.Content.ReadAsStringAsync()));
                    }
                }

                issueDataOutput.Error.ErrorId = 0;
                issueDataOutput.Error.ErrorDescription = String.Empty;
            }
            catch (Exception ex)
            {
                issueDataOutput.Error.ErrorId = -1;
                issueDataOutput.Error.ErrorDescription = ex.Message;
            }

            return issueDataOutput;
        }

        /// <summary>
        /// Get Issue data and related articles
        /// </summary>
        /// <param name="inputRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("AllIssueDataWithArticles")]
        public async Task<AllIssueDataWithArticlesOutput> GetIssuesDataWithArticles(InputRequest inputRequest)
        {
            AllIssueDataWithArticlesOutput allIssueDataWithArticlesOutput = new AllIssueDataWithArticlesOutput();
            allIssueDataWithArticlesOutput.Error = new ErrorInfo();
            try
            {
                GetCompanyInfo_Result company = CompanyMgr.GetCompanyInfo(inputRequest.CompanyId);

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(company.ServiceEndPoint);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    // New code:
                    HttpResponseMessage response = await client.GetAsync(String.Concat(inputRequest.Input1, ConstantMgr.REST_ROUTE_SERVICE, ConstantMgr.Routes.ALL_ISSUE_DATA_WITH_ARTICLES));
                    if (response.IsSuccessStatusCode)
                    {
                        allIssueDataWithArticlesOutput.IssuesWithArticles = JsonConvert.DeserializeObject<List<CurrentIssueWithArticles>>(CommonMgr.StripTagsCharArray(await response.Content.ReadAsStringAsync()));
                    }
                }

                allIssueDataWithArticlesOutput.Error.ErrorId = 0;
                allIssueDataWithArticlesOutput.Error.ErrorDescription = String.Empty;
            }
            catch (Exception ex)
            {
                allIssueDataWithArticlesOutput.Error.ErrorId = -1;
                allIssueDataWithArticlesOutput.Error.ErrorDescription = ex.Message;
            }

            return allIssueDataWithArticlesOutput;
        }

        [HttpPost]
        [Route("Announcements")]
        public async Task<AnnouncementOutput> GetAnnouncements(InputRequest inputRequest)
        {
            AnnouncementOutput announcementOutput = new AnnouncementOutput();
            announcementOutput.Error = new ErrorInfo();
            try
            {
                GetCompanyInfo_Result company = CompanyMgr.GetCompanyInfo(inputRequest.CompanyId);

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(company.ServiceEndPoint);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    // New code:
                    HttpResponseMessage response = await client.GetAsync(String.Concat(inputRequest.Input1, ConstantMgr.REST_ROUTE_SERVICE, ConstantMgr.Routes.ANNOUNCEMENTS));
                    if (response.IsSuccessStatusCode)
                    {
                        announcementOutput.Announcements = JsonConvert.DeserializeObject<List<Announcement>>(CommonMgr.StripTagsCharArray(await response.Content.ReadAsStringAsync()));
                    }
                }

                announcementOutput.Error.ErrorId = 0;
                announcementOutput.Error.ErrorDescription = String.Empty;
            }
            catch (Exception ex)
            {
                announcementOutput.Error.ErrorId = -1;
                announcementOutput.Error.ErrorDescription = ex.Message;
            }

            return announcementOutput;
        }

        /// <summary>
        /// Used for retrieving Article Info
        /// </summary>
        /// <param name="inputRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("ArticleInfo")]
        public async Task<ArticleInfoOutput> GetArticleInfo(InputRequest inputRequest)
        {
            ArticleInfoOutput articleInfoOutput = new ArticleInfoOutput();
            articleInfoOutput.Error = new ErrorInfo();
            try
            {
                GetCompanyInfo_Result company = CompanyMgr.GetCompanyInfo(inputRequest.CompanyId);

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(company.ServiceEndPoint);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    // New code:
                    HttpResponseMessage response = await client.GetAsync(String.Concat(inputRequest.Input1, ConstantMgr.REST_ROUTE_SERVICE, ConstantMgr.Routes.ARTICLE_INFO, inputRequest.Input2));
                    if (response.IsSuccessStatusCode)
                    {
                        articleInfoOutput.ArticleInfo = JsonConvert.DeserializeObject<ArticleInfo>(CommonMgr.StripTagsCharArray(await response.Content.ReadAsStringAsync()));
                    }
                }

                articleInfoOutput.Error.ErrorId = 0;
                articleInfoOutput.Error.ErrorDescription = String.Empty;
            }
            catch (Exception ex)
            {
                articleInfoOutput.Error.ErrorId = -1;
                articleInfoOutput.Error.ErrorDescription = ex.Message;
            }

            return articleInfoOutput;
        }

        /// <summary>
        /// Not in Use for Mobile
        /// </summary>
        /// <param name="inputRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("IssueData")]
        public async Task<IssueDataOutput> GetIssueData(InputRequest inputRequest)
        {
            IssueDataOutput issueDataOutput = new IssueDataOutput();
            issueDataOutput.Error = new ErrorInfo();
            try
            {
                GetCompanyInfo_Result company = CompanyMgr.GetCompanyInfo(inputRequest.CompanyId);

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(company.ServiceEndPoint);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    // New code:
                    HttpResponseMessage response = await client.GetAsync(String.Concat(inputRequest.Input1, ConstantMgr.REST_ROUTE_SERVICE, ConstantMgr.Routes.ISSUE_DATA, inputRequest.Input2));
                    if (response.IsSuccessStatusCode)
                    {
                        issueDataOutput.IssueData = JsonConvert.DeserializeObject<IssueData>(CommonMgr.StripTagsCharArray(await response.Content.ReadAsStringAsync()));
                    }
                }

                issueDataOutput.Error.ErrorId = 0;
                issueDataOutput.Error.ErrorDescription = String.Empty;
            }
            catch (Exception ex)
            {
                issueDataOutput.Error.ErrorId = -1;
                issueDataOutput.Error.ErrorDescription = ex.Message;
            }

            return issueDataOutput;
        }

        /// <summary>
        /// Issue data with List of Articles
        /// </summary>
        /// <param name="inputRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("IssueDataWithArticles")]
        public async Task<CurrentIssueWithArticlesOutput> GetIssueDataWithArticles(InputRequest inputRequest)
        {
            CurrentIssueWithArticlesOutput currentIssueWithArticlesOutput = new CurrentIssueWithArticlesOutput();
            currentIssueWithArticlesOutput.Error = new ErrorInfo();
            try
            {
                GetCompanyInfo_Result company = CompanyMgr.GetCompanyInfo(inputRequest.CompanyId);

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(company.ServiceEndPoint);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    // New code:
                    HttpResponseMessage response = await client.GetAsync(String.Concat(inputRequest.Input1, ConstantMgr.REST_ROUTE_SERVICE, ConstantMgr.Routes.ISSUE_DATA_WITH_ARTICLES, inputRequest.Input2));
                    if (response.IsSuccessStatusCode)
                    {
                        currentIssueWithArticlesOutput.CurrentIssueWithArticles = JsonConvert.DeserializeObject<CurrentIssueWithArticles>(CommonMgr.StripTagsCharArray(await response.Content.ReadAsStringAsync()));
                    }
                }

                currentIssueWithArticlesOutput.Error.ErrorId = 0;
                currentIssueWithArticlesOutput.Error.ErrorDescription = String.Empty;
            }
            catch (Exception ex)
            {
                currentIssueWithArticlesOutput.Error.ErrorId = -1;
                currentIssueWithArticlesOutput.Error.ErrorDescription = ex.Message;
            }

            return currentIssueWithArticlesOutput;
        }

        [HttpPost]
        [Route("SaveContactUs")]
        public ErrorInfo SaveContactUs(MailInput mailInput)
        {
            ErrorInfo error = new ErrorInfo();
            try
            {
                error.ErrorId = 0;
                error.ErrorDescription = String.Empty;

                //Get Company Details
                GetCompanyInfo_Result companyInfo = BLLayer.CompanyMgr.GetCompanyInfo(mailInput.CompanyId.Value);
                if (companyInfo != null)
                {
                    StringBuilder sbMailBody = new StringBuilder();
                    sbMailBody.Append("<html><body>");
                    sbMailBody.Append("<table>");
                    sbMailBody.Append(String.Format("<tr><td>Name</td><td>{0}</td></tr>", mailInput.Name));
                    sbMailBody.Append(String.Format("<tr><td>Email</td><td>{0}</td></tr>", mailInput.Email));
                    sbMailBody.Append(String.Format("<tr><td>Comments</td><td>{0}</td></tr>", mailInput.Comments));
                    sbMailBody.Append("<tr><th colspan='2' align='center'>Company Info</th></tr>");
                    sbMailBody.Append(String.Format("<tr><td>Name</td><td>{0}</td></tr>", companyInfo.Name));
                    sbMailBody.Append(String.Format("<tr><td>Address</td><td>{0}</td></tr>", companyInfo.Address));
                    sbMailBody.Append(String.Format("<tr><td>Phone</td><td>{0}</td></tr>", companyInfo.Phone));
                    sbMailBody.Append(String.Format("<tr><td>Is iPhone Enabled</td><td>{0}</td></tr>", companyInfo.iPhoneEnabled));
                    sbMailBody.Append(String.Format("<tr><td>Andriod Enabled</td><td>{0}</td></tr>", companyInfo.andriodEnabled));
                    sbMailBody.Append("</table>");
                    sbMailBody.Append("</body></html>");
                    //BLLayer.MailHelper.SendMail(ConfigMgr.ContactUS_Email, String.Concat("Contact US request : ", mailInput.Name), sbMailBody.ToString() , ConfigMgr.CCList);
                    BLLayer.MailHelper.SendMail(companyInfo.ToEmail, String.Concat("Contact US request : ", mailInput.Name), sbMailBody.ToString(), String.Empty,
                        companyInfo.FromEmail, companyInfo.FromPassword);

                    //Send a copy to actual user
                    BLLayer.MailHelper.SendMail(mailInput.Email, "Copy of Contact US request Email", sbMailBody.ToString(), String.Empty,
                        companyInfo.FromEmail, companyInfo.FromPassword);


                }
                else
                {
                    error.ErrorId = -1;
                    error.ErrorDescription = "Unable to fetch Company Details.";
                }
            }
            catch (Exception ex)
            {
                error.ErrorId = -1;
                error.ErrorDescription = ex.Message;
            }
            return error;
        }

    }
}