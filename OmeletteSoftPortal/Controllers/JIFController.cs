﻿using BLLayer;
using OmeletteSoft.BLLayer;
using OmeletteSoft.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace OmeletteSoftPortal.Controllers
{
    [RoutePrefix("jif")]
    public class JIFController : ApiController
    {
        [HttpPost]
        [Route("Search")]
        public async Task<JIFHeaderOutput> Search(SearchInput input)
        {
            var output = new JIFHeaderOutput();
            output.Error = new ErrorInfo();
            output.results = new List<JIFHeaderResulItem>();

            try
            {
                string sortColumn = "Title", sortOrder = "ASC";
                if(!string.IsNullOrEmpty(input.SortField))
                {
                    if(input.SortField.Equals("Title", StringComparison.InvariantCultureIgnoreCase))
                    {
                        sortColumn = "Title";
                        sortOrder = "ASC";
                    }
                    if (input.SortField.Equals("ImpactFactor", StringComparison.InvariantCultureIgnoreCase))
                    {
                        sortColumn = "ImpactFactor";
                        sortOrder = "DESC";
                    }

                }
                var rows = input.Rows.HasValue ? input.Rows.Value : 500;
                output.results = JIFMgr.SearchJournal(input.Query, rows,0, sortColumn, sortOrder).Select(e => new JIFHeaderResulItem()
                {
                    Id = e.JournalHeaderId,
                    Category = e.JournalCategoryName,
                    Country = e.CountryName,
                    EISSN = e.EISSN,
                    ISSN = e.ISSN,
                    Language = e.LanguageName,
                    Publisher = e.PublisherName,
                    Title = e.Title,
                    Title20 = e.Title20,
                    Title29 = e.Title29,
                    Website = e.WebSite,
                    Frequency = e.Frequency,
                    SocietyName =e.SocietyName,
                    CurrentYear = e.CurrentYear.Value,
                    ImpactFactor = e.ImpactFactor,
                    APC = e.APC,
                    APCAmount =e.APCAmount,
                    Currency =e.Currency,
                    SubmissionFee=e.SubmissionFee,
                    Keywords=e.Keywords,
                    ReviewProcess=e.ReviewProcess,
                    OpenAccess = e.OpenAccess,
                    Counters = new DALayer.JournalHeaderCounters()
                    {
                        MarkedAsFavorites = e.MarkedAsFavorite,
                        Shared = e.Shared,
                        Viewed = e.Viewed
                    }
                }).ToList();
            }
            catch (Exception ex)
            {
                output.Error.ErrorId = -1;
                output.Error.ErrorDescription = ex.Message;
            }
            return output;
        }

        [HttpGet]
        [Route("factor")]
        public async Task<JIFImpactFactorOutput> Factor(long? id)
        {
            var output = new JIFImpactFactorOutput();
            output.Error = new ErrorInfo();
            output.results = new List<JIFImpactFactor>();

            try
            {
                if (id.HasValue)
                {
                    output.results = JIFMgr.GetJournalImpactFactor(id.Value).Select(e => new JIFImpactFactor()
                    {
                        Year = e.JournalYear.Value,
                        Factor = e.ImpactFactor.Value
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                output.Error.ErrorId = -1;
                output.Error.ErrorDescription = ex.Message;
            }
            return output;
        }

        [HttpPost]
        [Route("ImpactFactors")]
        public async Task<ISSNImpactFactorOutput> GetISSNImpactFactors(ISSNImpactFactorInput input)
        {
            var output = new ISSNImpactFactorOutput();
            output.Error = new ErrorInfo();
            output.Items = new List<ISSNImpactFactor>();

            try
            {
                if (input.ISSN.Any())
                {
                    output.Items.AddRange(CompanyMgr.GetImpactFactor(input.ISSN, "Print").Select(e=> new ISSNImpactFactor() {
                        ISSN = e.ISSN,
                        Year =e.JournalYear,
                        ImpactFactor = e.JournalImpactFactor.ToString("####.###")
                }));
                }
                if (input.EISSN.Any())
                {
                    output.Items.AddRange(CompanyMgr.GetImpactFactor(input.EISSN, "Electronic").Select(e => new ISSNImpactFactor()
                    {
                        EISSN = e.ISSN,
                        Year = e.JournalYear,
                        ImpactFactor = e.JournalImpactFactor.ToString("####.###")
                    }));
                }
            }
            catch (Exception ex)
            {
                output.Error.ErrorId = -1;
                output.Error.ErrorDescription = ex.Message;
            }
            return output;
        }
    }
}