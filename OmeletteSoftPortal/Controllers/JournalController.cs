﻿using OmeletteSoft.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OmeletteSoft.Controllers
{
    public class JournalController : Controller
    {
        // GET: Journal
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Info(String id)
        {
            object temp = id;
            return View(temp);
        }
        public ActionResult CurrentIssue(String id)
        {
            object temp = id;
            return View(temp);
        }
        public ActionResult Archive(String id)
        {
            object temp = id;
            return View(temp);
        }
        public ActionResult IssueData(String jid, String iid)
        {
            InputRequest inputRequest = new InputRequest { Input1 = jid, Input2 = iid };
            return View(inputRequest);
        }
        public ActionResult ArticleInfo(String jid, String aid)
        {
            InputRequest inputRequest = new InputRequest() { Input1 = jid, Input2 = aid };
            return View(inputRequest);
        }
    }
}