﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using BLLayer;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using OmeletteSoft.BLLayer;
using OmeletteSoft.Models;
using OmeletteSoftPortal;
using OmeletteSoftPortal.Models;
using System.Linq;
using System.Xml.Serialization;
using System.IO;
using System.Xml.Linq;
using System.Net.Http.Headers;
using OmeletteSoftPortal.Atrributes;

namespace OmeletteSoft.Controllers
{
    [UserIdAuthorizeAttribute]
    [RoutePrefix("pubmed")]
    public class PubMedController : ApiController
    {
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public PubMedController()
        {

        }
        public PubMedController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }
        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }

        [AllowAnonymous]
        [HttpPost]
        [Route("Search")]
        public async Task<SearchOutput> Search(SearchInput input)
        {
            SearchOutput output = new SearchOutput();
            output.Error = new ErrorInfo();

            try
            {
                String db = "pubmed";
                
                IList<String> list = new List<String>();
                if(input.Ids != null && input.Ids.Any())
                {
                    //We got pmid's from mobile, only return data for list of pmid's
                    list = input.Ids;
                }
                else
                {
                    int pageSize = (input.Rows.HasValue ? (input.Rows.Value < ConstantMgr.PubMed.DefaultRows ? input.Rows.Value : ConstantMgr.PubMed.DefaultRows) : ConstantMgr.PubMed.DefaultRows);
                    int startRecordCount = 0;
                    if (input.Page.HasValue && input.Page.Value > 0)
                    {
                        startRecordCount = ((input.Page.Value - 1) * pageSize);
                    }
                    //https://dataguide.nlm.nih.gov/eutilities/utilities.html#esearch
                    //pub+date
                    string url = String.Format("{0}/esearch.fcgi?db={1}&term={2}&usehistory=y&retmode=xml&retStart={3}&retMax={4}&sort=most+recent",
                        ConfigMgr.PubMedRootURL, db, input.Query, startRecordCount, pageSize);
                    XElement XMLElement = XElement.Load(url);

                    //<Count>97</Count>
                    //<RetMax>96</RetMax>
                    //<RetStart>1</RetStart>
                    //<QueryKey>1</QueryKey>

                    output.TotalRecords = Convert.ToInt32(XMLElement.Element("Count").Value);
                    output.RetStart = Convert.ToInt32(XMLElement.Element("RetStart").Value);
                    output.RetMax = Convert.ToInt32(XMLElement.Element("RetMax").Value);

                    list = (from c in XMLElement.Element("IdList").DescendantNodes().OfType<XText>()
                            where c.Value.Trim().Length > 0
                            select c.Value.ToString()).ToList<String>();
                }

                if (list != null)
                {
                    PubMedModel response2 = new PubMedModel();

                    //Fetch Details
                    using (var client2 = new HttpClient())
                    {
                        //Fetch Details
                        var fetchDetailsURL = String.Format("{0}/efetch.fcgi?db=pubmed&retmode=xml&rettype=abstract&id={1}",
                                ConfigMgr.PubMedRootURL,
                                String.Join(",", list)
                                );
                        client2.BaseAddress = new Uri(ConfigMgr.PubMedRootURL);
                        client2.DefaultRequestHeaders.Accept.Clear();
                        client2.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        // New code:
                        HttpResponseMessage detailsResponse = client2.GetAsync(fetchDetailsURL).Result;
                        if (detailsResponse.IsSuccessStatusCode)
                        {
                            var serializer = new XmlSerializer(typeof(OmeletteSoftPortal.Models.PubMed.PubmedArticleSet));
                            OmeletteSoftPortal.Models.PubMed.PubmedArticleSet result;

                            var ms = new MemoryStream();
                            await detailsResponse.Content.CopyToAsync(ms);
                            ms.Seek(0, SeekOrigin.Begin);

                            var content = detailsResponse.Content.ReadAsStringAsync().Result;
                            content = content.Replace("<sub>", "&lt;sub&gt;").Replace("</sub>", "&lt;/sub&gt;")
                                .Replace("<i>", "&lt;i&gt;").Replace("</i>", "&lt;/i&gt;")
                                .Replace("<b>", "&lt;b&gt;").Replace("</b>", "&lt;/b&gt;")
                                .Replace("<sup>", "&lt;sup&gt;").Replace("</sup>", "&lt;/sup&gt;")
                                .Replace("<sup/>", "&lt;sup/&gt;").Replace("</sup>", "&lt;/sup&gt;");

                            using (TextReader reader = new StringReader(content))
                            {
                                result = (OmeletteSoftPortal.Models.PubMed.PubmedArticleSet)serializer.Deserialize(reader);
                            }

                            if (result != null) {
                                output.results = new List<SearchResultItem>();
                                DateTime temp;
                                foreach(var resultItem in result.PubmedArticle){
                                    SearchResultItem searchResultItem = new SearchResultItem();
                                    searchResultItem.PMID = resultItem.MedlineCitation.PMID.Text;

                                    List<OmeletteSoftPortal.Models.PubMed.AbstractText> abstractNodes = new List<OmeletteSoftPortal.Models.PubMed.AbstractText>();

                                    if (resultItem.MedlineCitation != null &&
                                        resultItem.MedlineCitation.Article != null &&
                                        resultItem.MedlineCitation.Article.Abstract != null &&
                                        resultItem.MedlineCitation.Article.Abstract.AbstractText != null)
                                    {
                                        abstractNodes = resultItem.MedlineCitation.Article.Abstract.AbstractText;
                                    }
                                    StringBuilder sbAbstract = new StringBuilder();
                                    foreach (var abstractNode in abstractNodes) {
                                        sbAbstract.AppendFormat("<b>{0}</b> : {1}</br>", abstractNode.Label, abstractNode.Text);
                                    }
                                    searchResultItem.Abstract = sbAbstract.ToString();

                                    if (resultItem.MedlineCitation != null &&
                                        resultItem.MedlineCitation.Article != null &&
                                        resultItem.MedlineCitation.Article.Abstract != null &&
                                        resultItem.MedlineCitation.Article.Abstract.CopyrightInformation != null) {
                                            searchResultItem.CopyrightInformation = resultItem.MedlineCitation.Article.Abstract.CopyrightInformation;
                                    }
                                    searchResultItem.Authors = new List<string>();
                                    if (resultItem.MedlineCitation.Article != null)
                                    {
                                        if (resultItem.MedlineCitation.Article.Pagination != null)
                                        {
                                            searchResultItem.Pages = resultItem.MedlineCitation.Article.Pagination.MedlinePgn ?? string.Empty;
                                        }

                                        if (resultItem.MedlineCitation.Article.AuthorList != null)
                                        {
                                            foreach (var author in resultItem.MedlineCitation.Article.AuthorList.Author)
                                            {
                                                /*searchResultItem.Authors.Add(String.Format("{0} {1} {2} {3} {4}", 
                                                    (!String.IsNullOrEmpty(author.Suffix) ? author.Suffix : String.Empty),
                                                    (!String.IsNullOrEmpty(author.ForeName) ? author.ForeName : String.Empty),
                                                    (!String.IsNullOrEmpty(author.LastName) ? author.LastName : String.Empty),
                                                    (!String.IsNullOrEmpty(author.Initials) ? String.Concat("(", author.Initials, ")") : String.Empty),
                                                    (author.AffiliationInfo != null && !String.IsNullOrEmpty(author.AffiliationInfo.Affiliation) ? String.Concat("[", author.AffiliationInfo.Affiliation, "]") : String.Empty)).Trim());
                                                 * */
                                                if (!String.IsNullOrEmpty(author.ForeName))
                                                {

                                                    searchResultItem.Authors.Add(String.Format("{0} {1}", author.ForeName, author.LastName));
                                                }
                                            }
                                        }
                                    }

                                    if (resultItem.PubmedData != null && resultItem.PubmedData.ArticleIdList != null && resultItem.PubmedData.ArticleIdList.ArticleId != null)
                                    {
                                        foreach (var articleId in resultItem.PubmedData.ArticleIdList.ArticleId)
                                        {
                                            switch (articleId.IdType.ToLower())
                                            {
                                                case "pii":
                                                    searchResultItem.PII = articleId.Text;
                                                    break;
                                                case "doi":
                                                    searchResultItem.DOI = articleId.Text;
                                                    break;
                                                case "pubmed":
                                                    searchResultItem.PMID = articleId.Text;
                                                    break;
                                                case "pmcid":
                                                    searchResultItem.PMCID = articleId.Text;
                                                    break;
                                                case "pmc":
                                                    searchResultItem.PMC = articleId.Text;
                                                    break;
                                                case "eid":
                                                    searchResultItem.EID = articleId.Text;
                                                    break;
                                            }
                                        }
                                    }
                                    
                                    searchResultItem.Id = resultItem.MedlineCitation.PMID.Text;
                                    searchResultItem.Journal = new Journal();
                                    
                                    if(resultItem != null && resultItem.MedlineCitation != null &&
                                        resultItem.MedlineCitation.Article != null &&
                                        resultItem.MedlineCitation.Article.Journal != null){



                                        if(resultItem.MedlineCitation.Article.Journal.ISOAbbreviation != null)
                                            searchResultItem.Journal.ISOAbbreviation = resultItem.MedlineCitation.Article.Journal.ISOAbbreviation;

                                        if(resultItem.MedlineCitation.Article.Journal.ISSN != null &&
                                            resultItem.MedlineCitation.Article.Journal.ISSN.Text != null)
                                        {
                                            searchResultItem.Journal.ISSN = resultItem.MedlineCitation.Article.Journal.ISSN.Text;
                                            searchResultItem.Journal.ISSNType = resultItem.MedlineCitation.Article.Journal.ISSN.IssnType;
                                        }
                                            

                                        if(resultItem.MedlineCitation.Article.Journal.JournalIssue != null &&
                                            resultItem.MedlineCitation.Article.Journal.JournalIssue.Volume != null){

                                            searchResultItem.Journal.Volume = resultItem.MedlineCitation.Article.Journal.JournalIssue.Volume;

                                            if (resultItem.MedlineCitation.Article.Journal.JournalIssue.Issue != null)
                                            {
                                                searchResultItem.Journal.Issue = String.Format("Volume : {0}, Issue : {1}",
                                                        resultItem.MedlineCitation.Article.Journal.JournalIssue.Volume,
                                                        resultItem.MedlineCitation.Article.Journal.JournalIssue.Issue);
                                            }
                                        }
                                            

                                        if(resultItem.MedlineCitation.Article.Journal.Title != null)
                                            searchResultItem.Journal.Title = resultItem.MedlineCitation.Article.Journal.Title;



                                    }
                                    
                                    try
                                    {
                                        if (DateTime.TryParse(String.Concat(resultItem.MedlineCitation.Article.Journal.JournalIssue.PubDate.Month, "/",
                                            (!String.IsNullOrEmpty(resultItem.MedlineCitation.Article.Journal.JournalIssue.PubDate.Day) ? resultItem.MedlineCitation.Article.Journal.JournalIssue.PubDate.Day : "01"),
                                            "/", resultItem.MedlineCitation.Article.Journal.JournalIssue.PubDate.Year), out temp))
                                        {
                                            searchResultItem.Journal.PubDate = temp;
                                            searchResultItem.PubDate = searchResultItem.Journal.PubDate;
                                        }

                                    }
                                    catch(Exception) { 
                                    }
                                    
                                    searchResultItem.Title = resultItem.MedlineCitation.Article.ArticleTitle;

                                    try
                                    {
                                        if (resultItem.MedlineCitation.Article.ArticleDate != null && DateTime.TryParse(String.Concat(resultItem.MedlineCitation.Article.ArticleDate.Month, "/",
                                            (!String.IsNullOrEmpty(resultItem.MedlineCitation.Article.ArticleDate.Day) ? resultItem.MedlineCitation.Article.ArticleDate.Day : "01"),
                                            "/", resultItem.MedlineCitation.Article.ArticleDate.Year), out temp))
                                        {
                                            searchResultItem.ArticleDate = temp;
                                        }
                                    }
                                    catch (Exception)
                                    {
                                    }
                                    
                                    output.results.Add(searchResultItem);
                                }
                            }
                        }
                    }
                    try
                    {
                        //Retrieve ImpactFactor for available ISSN's
                        var impactFactorList = CompanyMgr.GetImpactFactor(output.results.Where(e => e.Journal.ISSNType != null && e.Journal.ISSNType.ToUpper().Equals("PRINT")).Select(e => e.Journal.ISSN).ToList(), "Print");
                        foreach (var journalItem in output.results)
                        {
                            var impactFactorItem = impactFactorList.Where(e2 => e2.ISSN == journalItem.Journal.ISSN).FirstOrDefault();
                            if (impactFactorItem != null)
                            {
                                journalItem.Journal.ImpactFactor = impactFactorItem.JournalImpactFactor.ToString("####.###");
                            }
                        }

                        impactFactorList = CompanyMgr.GetImpactFactor(output.results.Where(e => e.Journal.ISSNType != null && e.Journal.ISSNType.ToUpper().Equals("ELECTRONIC")).Select(e => e.Journal.ISSN).ToList(), "Electronic");
                        foreach (var journalItem in output.results)
                        {
                            var impactFactorItem = impactFactorList.Where(e2 => e2.ISSN == journalItem.Journal.ISSN).FirstOrDefault();
                            if (impactFactorItem != null && string.IsNullOrEmpty(journalItem.Journal.ImpactFactor))
                            {
                                journalItem.Journal.ImpactFactor = impactFactorItem.JournalImpactFactor.ToString("####.###");
                            }
                        }

                        //Log any missing ISSN or EISSN
                        var missingISSNs = output.results.Where(e => string.IsNullOrEmpty(e.Journal.ImpactFactor)).Select(e => new Tuple<string, string>(e.Journal.ISSNType, e.Journal.ISSN)).ToList();
                        if (missingISSNs.Any())
                        {
                            CompanyMgr.SaveMissingISSNs(missingISSNs);
                        }
                        if(output.results != null)
                        {

                            //Look for DOI citation
                            Parallel.ForEach(output.results, (item) =>
                            {
                                if (!string.IsNullOrEmpty(item.DOI))
                                {
                                    var citationResult = BLLayer.CitationMgr.GetCitationDetails(item.DOI);
                                    if (citationResult != null)
                                    {
                                        item.Citation = new Citation();
                                        item.Citation.TimesCited = Convert.ToString(citationResult.times_cited);
                                        item.Citation.FieldCitationRatio = Convert.ToString(citationResult.relative_citation_ratio);
                                    }
                                }
                            });
                        }

                    }
                    catch (Exception) { }
                    output.Error.ErrorId = 0;
                    output.Error.ErrorDescription = String.Empty;
                }
                else
                {
                    output.Error.ErrorId = -1;
                    output.Error.ErrorDescription = "Failed to search PubMed database.";
                }
            }
            catch (Exception ex)
            {
                output.Error.ErrorId = -1;
                output.Error.ErrorDescription = ex.Message;
            }

            return output;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("ContactUs")]
        public ErrorInfo SaveContactUs(MailInput mailInput)
        {
            ErrorInfo error = new ErrorInfo();
            try
            {
                error.ErrorId = 0;
                error.ErrorDescription = String.Empty;

                StringBuilder sbMailBody = new StringBuilder();
                sbMailBody.Append("<html><body>");
                sbMailBody.Append("<table>");
                sbMailBody.Append(String.Format("<tr><td>Name:</td><td>{0}</td></tr>", mailInput.Name));
                sbMailBody.Append(String.Format("<tr><td>Email:</td><td>{0}</td></tr>", mailInput.Email));
                sbMailBody.Append(String.Format("<tr><td>Comments:</td><td>{0}</td></tr>", mailInput.Comments));
                sbMailBody.Append("{0}");
                sbMailBody.Append("</table>");
                sbMailBody.Append("</body></html>");

                var sbMailAdditionalInfo = new StringBuilder();
                sbMailAdditionalInfo.AppendFormat("<tr><td>Application Name:</td><td>{0}</td></tr>", (mailInput.AppName ?? "PubMed"));
                sbMailAdditionalInfo.AppendFormat("<tr><td>Phone Type:</td><td>{0}</td></tr>", (mailInput.PhoneType ?? "NA"));

                var smtp = new System.Net.Mail.SmtpClient();
                var host = smtp.Host;
                var ssl = smtp.EnableSsl;
                var port = smtp.Port;

                var credential = (smtp.Credentials as System.Net.NetworkCredential);
                var username = credential.UserName;
                var password = credential.Password;

                BLLayer.MailHelper.SendMail(ConfigMgr.ContactUS_Email, String.Concat("Contact Us request : ", mailInput.Name), 
                    string.Format( sbMailBody.ToString(), sbMailAdditionalInfo.ToString()), ConfigMgr.CCList,
                    username, password, ssl, port, host);

                //Send a copy to actual user
                BLLayer.MailHelper.SendMail(mailInput.Email, "Copy of Contact Us request Email", string.Format( sbMailBody.ToString(), string.Empty), String.Empty,
                    username, password, ssl, port, host);
            }
            catch (Exception ex)
            {
                error.ErrorId = -1;
                error.ErrorDescription = ex.Message;
            }
            return error;
        }

        [HttpPost]
        [Route("MyFavorites")]
        public FavoritesOutput GetFavorites(AppUser input)
        {
            var output = new FavoritesOutput();
            try
            {
                BLLayer.EnumMgr.ApplicationEnum application;
                if (!Enum.TryParse(input.Application, true, out application))
                {
                    application = BLLayer.EnumMgr.ApplicationEnum.PubMed;
                }
                BLLayer.EnumMgr.FavoriteType favoriteType;
                if (!Enum.TryParse(input.FavoriteType, true, out favoriteType))
                {
                    favoriteType = EnumMgr.FavoriteType.NotSet;
                }
                var userDetails = UserManager.FindById(User.Identity.GetUserId());
                if(userDetails == null)
                {
                    output.ErrorDetails.Code = -1;
                    output.ErrorDetails.Message = "Invalid user details.";
                    return output;
                }

                foreach(var favoriteItem in BLLayer.AppUserMgr.GetAppUserFavorites(userDetails.Id, (short)favoriteType, null))
                {
                    var favoriteTypeResult = (BLLayer.EnumMgr.FavoriteType)favoriteItem.FavoriteTypeId.Value;

                    var favorite = output.Favorites.FirstOrDefault(e=> e.FavoriteType == favoriteTypeResult.ToString());
                    if(favorite == null)
                    {
                        favorite = new Favorite();
                        favorite.FavoriteType = favoriteTypeResult.ToString();
                        favorite.FavoriteValues = new List<UploadedFileDetails>();
                        output.Favorites.Add(favorite);
                    }

                    favorite.FavoriteValues.Add(new UploadedFileDetails() {
                        UploadPath = !string.IsNullOrEmpty(favoriteItem.ImagePath) ? Url.Content(favoriteItem.ImagePath) : string.Empty,
                        FavoriteValue = favoriteItem.FavoriteValue,
                        UploadId = favoriteItem.ImageId.HasValue ? favoriteItem.ImageId.Value.ToString() : string.Empty,
                        FileName = favoriteItem.OriginalFileName
                    });

                }
                //Get Details from PUBMED
                var pmidItems = output.Favorites.Where(e => e.FavoriteType.Equals(EnumMgr.FavoriteType.Favorites.ToString(), StringComparison.InvariantCultureIgnoreCase)
                || e.FavoriteType.Equals(EnumMgr.FavoriteType.History.ToString(), StringComparison.InvariantCultureIgnoreCase)
                || e.FavoriteType.Equals(EnumMgr.FavoriteType.MyArticles.ToString(), StringComparison.InvariantCultureIgnoreCase)).ToList();
                /*
                if (pmidItems.Any())
                {
                    var pmids = new List<int>();
                    foreach(var pmid in pmidItems.SelectMany(e => e.FavoriteValues.Select(e2 => e2.FavoriteValue)))
                    {
                        var parseResult = -1;
                        if (int.TryParse(pmid, out parseResult))
                            pmids.Add(parseResult);
                    }
                    if (pmids.Any())
                    {
                        var searchResult = Search(new SearchInput()
                        {
                            Ids = pmids.Select(e => e.ToString()).ToList()
                        }).Result;

                        foreach (var favoriteItemType in pmidItems)
                        {
                            foreach (var favoriteItem in favoriteItemType.FavoriteValues)
                            {
                                favoriteItem.Details = searchResult.results.FirstOrDefault(e => e.PMID.Equals(favoriteItem.FavoriteValue));
                            }
                        }
                    }
                }
                */
            }
            catch (Exception)
            {
                output.ErrorDetails.Code = -1;
                output.ErrorDetails.Message = "Error occured by retrieving Favorites";
            }
            return output;
        }

        [HttpPost]
        [Route("DeleteFavorites")]
        public FavoritesOutput DeleteFavorites(FavoriteInput input)
        {
            var output = new FavoritesOutput();
            try
            {
                BLLayer.EnumMgr.ApplicationEnum application;
                if (!Enum.TryParse(input.Application, true, out application))
                {
                    application = BLLayer.EnumMgr.ApplicationEnum.PubMed;
                }
                BLLayer.EnumMgr.FavoriteType favoriteType;
                if (!Enum.TryParse(input.FavoriteType, true, out favoriteType))
                {
                    favoriteType = EnumMgr.FavoriteType.NotSet;
                }
                var userDetails = UserManager.FindById(User.Identity.GetUserId());

                if (userDetails == null)
                {
                    output.ErrorDetails.Code = -1;
                    output.ErrorDetails.Message = "Invalid User.";
                    return output;
                }
                if (!userDetails.EmailConfirmed)
                {
                    output.ErrorDetails.Code = -1;
                    output.ErrorDetails.Message = "User is not active. Please verify code before using App.";
                    return output;
                }

                //Delete Favorite
                var deletedFiles = BLLayer.AppUserMgr.DeleteFavorites(userDetails.Id, input.FavoriteValues, (byte)favoriteType);
                foreach(var deletedFile in deletedFiles)
                {
                    try
                    {
                        System.IO.File.Delete(HttpContext.Current.Server.MapPath(deletedFile));
                    }
                    catch (Exception)
                    {
                        BLLayer.AppUserMgr.SaveFailedDeletionRecord(userDetails.Id, deletedFile);
                    }
                }
                return GetFavorites(new AppUser() {Application = input.Application, FavoriteType = input.FavoriteType});
            }
            catch (Exception)
            {
                output.ErrorDetails.Code = -1;
                output.ErrorDetails.Message = "Error occured by deleting Favorites";
            }
            return output;
        }
    }
}
