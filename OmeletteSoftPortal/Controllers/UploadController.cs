﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using BLLayer;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using OmeletteSoft.BLLayer;
using OmeletteSoft.Models;
using OmeletteSoftPortal;
using OmeletteSoftPortal.Models;
using OmeletteSoftPortal.Atrributes;

namespace OmeletteSoftPortal.Controllers
{
    [UserIdAuthorizeAttribute]
    [RoutePrefix("upload")]
    public class UploadController : ApiController
    {
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public UploadController()
        {

        }
        public UploadController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }
        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }

        [HttpPost]
        [Route("File")]
        public Error UploadFile(string app, string uploadType)
        {
            var error = new Error();
            EnumMgr.ApplicationEnum application;
            if (!Enum.TryParse(app, true, out application))
            {
                application = EnumMgr.ApplicationEnum.PubMed;
            }

            EnumMgr.FavoriteType favoriteType;
            if (!Enum.TryParse(uploadType, true, out favoriteType))
            {
                error.Code = -1;
                error.Message = "Invalid Upload Type.";
                return error;
            }

            HttpResponseMessage result = null;
            var userDetails = UserManager.FindById(User.Identity.GetUserId());

            if(userDetails == null)
            {
                error.Code = -1;
                error.Message = "Invalid User Context.";
                return error;
            }
            if (!userDetails.EmailConfirmed)
            {
                error.Code = -1;
                error.Message = "Invalid User Context. Please Confirm Email.";
                return error;
            }

            var httpRequest = HttpContext.Current.Request;
            if (httpRequest.Files.Count > 0)
            {
                var docfiles = new List<string>();
                foreach (string file in httpRequest.Files)
                {
                    //Make sure path exists
                    var relativeFolderPath = $"~/Files/{application.ToString()}/{userDetails.Id.ToString()}/{favoriteType.ToString()}";
                    var folderName = HttpContext.Current.Server.MapPath(relativeFolderPath);
                    if (!Directory.Exists(folderName))
                        Directory.CreateDirectory(folderName);

                    var postedFile = httpRequest.Files[file];
                    var fileId = Guid.NewGuid();
                    var extension = Path.GetExtension(postedFile.FileName);
                    if (string.IsNullOrEmpty(extension))
                    {
                        //We dont have extension
                        switch(favoriteType)
                        {
                            case EnumMgr.FavoriteType.AudioBox:
                                extension = ".mp3";
                                break;
                            case EnumMgr.FavoriteType.Image:
                                extension = ".png";
                                break;
                            case EnumMgr.FavoriteType.PDFBox:
                                extension = ".pdf";
                                break;
                        }
                    }
                    var fileName = $"{fileId}{extension}";
                    if(favoriteType == EnumMgr.FavoriteType.Image)
                    {
                        fileName = userDetails.Id + extension;
                    }
                    var originalFileName = Path.GetFileName(file);
                    
                    var filePath = $"{folderName}/{fileName}";
                    var relativePath = $"{relativeFolderPath}/{fileName}";
                    var fileSizeInKB = (int)Math.Ceiling((double)postedFile.ContentLength / 1024);
                    postedFile.SaveAs(filePath);


                    AppUserMgr.SaveAppUserUpload(userDetails.Id, fileId, relativePath, originalFileName, fileSizeInKB);
                    AppUserMgr.SaveAppUserFavorites(userDetails.Id, (short)favoriteType, new List<string>() { fileId.ToString() });
                    docfiles.Add(fileId.ToString());

                    if(favoriteType  == EnumMgr.FavoriteType.Image)
                    {
                        
                        //Update profile picture
                        var userProfileDetails = PubMedMgr.GetUserDetails(User.Identity.GetUserId());
                        PubMedMgr.SaveUserDetails(userProfileDetails.AspnetUserId, userProfileDetails.Prefix, userProfileDetails.FirstName,
                            userProfileDetails.MiddleName, userProfileDetails.LastName, fileId);
                        foreach (var item in AppUserMgr.GetAppUserFavorites(userProfileDetails.AspnetUserId, (byte)EnumMgr.FavoriteType.Image, null))
                        {
                            if(!Path.GetFileName(item.ImagePath).Equals(fileName, StringComparison.InvariantCultureIgnoreCase))
                            {
                                try
                                {
                                    System.IO.File.Delete(HttpContext.Current.Server.MapPath(item.ImagePath));
                                }
                                finally
                                {

                                }
                                AppUserMgr.DeleteFavorites(userProfileDetails.AspnetUserId, new List<string>() { item.ImageId.ToString() }, (byte)EnumMgr.FavoriteType.Image);
                            }
                        }

                    }
                }
                error.Code = 0;
                error.Message = $"Completed uploaded. Files: {string.Join(",",docfiles)}";
                return error;
            }
            else
            {
                error.Code = -1;
                error.Message = $"No files to save.";
                return error;
            }
        }

        [HttpPost]
        [Route("favorites")]
        public Error UploadFavorites(FavoriteInput input)
        {
            Error error = new Error();
            EnumMgr.FavoriteType favoriteType;
            if (!Enum.TryParse(input.FavoriteType, true, out favoriteType))
            {
                error.Code = -1;
                error.Message="Invalid Favorite Type.";
                return error;
            }
            var userDetails = UserManager.FindById(User.Identity.GetUserId());

            if (userDetails == null)
            {
                error.Code = -1;
                error.Message = "Invalid Request.";
                return error;
            }
            if (!userDetails.EmailConfirmed)
            {
                error.Code = -1;
                error.Message = "Invalid User Context. Please Confirm Email.";
                return error;
            }

            HttpResponseMessage result = null;
            
            if (!input.FavoriteValues.Any())
            {
                error.Code = -1;
                error.Message = "Missing Favorite Items.";
                return error;
            }

            try
            {
                OmeletteSoft.BLLayer.AppUserMgr.SaveAppUserFavorites(userDetails.Id, (short)favoriteType, input.FavoriteValues);
                error.Code = 0;
                error.Message = "Saved Favorites successfully.";
                return error;
            }
            catch (Exception)
            {
                error.Code = -1;
                error.Message = "Error occured during saving of favorites.";
                return error;
            }
        }

    }
}