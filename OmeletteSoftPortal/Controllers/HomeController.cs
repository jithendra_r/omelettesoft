﻿using OmeletteSoft.BLLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebSiteWithApi.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        public ActionResult ConfirmEmail(string Id)
        {
            var result = AppUserMgr.DecodeEmailConfirmationToken(Id);
            if(result == -1)
            {
                ViewBag.Message = "Invalid or Expired Link";
            }
            else
            {
                //AppUserMgr.VerifyAppUserDevice(result, true);
                ViewBag.Message = "Hurray!! Email is confirmed. Go back to App.";
            }
            return View();
        }
    }
}
