﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace OmeletteSoftPortal.Models.PubMed
{
    [XmlRoot(ElementName = "PMID")]
    public class PMID
    {
        [XmlAttribute(AttributeName = "Version")]
        public string Version { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "DateCreated")]
    public class DateCreated
    {
        [XmlElement(ElementName = "Year")]
        public string Year { get; set; }
        [XmlElement(ElementName = "Month")]
        public string Month { get; set; }
        [XmlElement(ElementName = "Day")]
        public string Day { get; set; }
    }

    [XmlRoot(ElementName = "DateCompleted")]
    public class DateCompleted
    {
        [XmlElement(ElementName = "Year")]
        public string Year { get; set; }
        [XmlElement(ElementName = "Month")]
        public string Month { get; set; }
        [XmlElement(ElementName = "Day")]
        public string Day { get; set; }
    }

    [XmlRoot(ElementName = "DateRevised")]
    public class DateRevised
    {
        [XmlElement(ElementName = "Year")]
        public string Year { get; set; }
        [XmlElement(ElementName = "Month")]
        public string Month { get; set; }
        [XmlElement(ElementName = "Day")]
        public string Day { get; set; }
    }

    [XmlRoot(ElementName = "ISSN")]
    public class ISSN
    {
        [XmlAttribute(AttributeName = "IssnType")]
        public string IssnType { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "PubDate")]
    public class PubDate
    {
        [XmlElement(ElementName = "Year")]
        public string Year { get; set; }
        [XmlElement(ElementName = "Month")]
        public string Month { get; set; }
        [XmlElement(ElementName = "Day")]
        public string Day { get; set; }
    }

    [XmlRoot(ElementName = "JournalIssue")]
    public class JournalIssue
    {
        [XmlElement(ElementName = "Volume")]
        public string Volume { get; set; }
        [XmlElement(ElementName = "PubDate")]
        public PubDate PubDate { get; set; }
        [XmlAttribute(AttributeName = "CitedMedium")]
        public string CitedMedium { get; set; }
        [XmlElement(ElementName = "Issue")]
        public string Issue { get; set; }
    }

    [XmlRoot(ElementName = "Journal")]
    public class Journal
    {
        [XmlElement(ElementName = "ISSN")]
        public ISSN ISSN { get; set; }
        [XmlElement(ElementName = "JournalIssue")]
        public JournalIssue JournalIssue { get; set; }
        [XmlElement(ElementName = "Title")]
        public string Title { get; set; }
        [XmlElement(ElementName = "ISOAbbreviation")]
        public string ISOAbbreviation { get; set; }
    }

    [XmlRoot(ElementName = "Pagination")]
    public class Pagination
    {
        [XmlElement(ElementName = "MedlinePgn")]
        public string MedlinePgn { get; set; }
    }

    [XmlRoot(ElementName = "ELocationID")]
    public class ELocationID
    {
        [XmlAttribute(AttributeName = "EIdType")]
        public string EIdType { get; set; }
        [XmlAttribute(AttributeName = "ValidYN")]
        public string ValidYN { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "AbstractText")]
    public class AbstractText
    {
        [XmlAttribute(AttributeName = "Label")]
        public string Label { get; set; }
        [XmlAttribute(AttributeName = "NlmCategory")]
        public string NlmCategory { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "Abstract")]
    public class Abstract
    {
        [XmlElement(ElementName = "AbstractText")]
        public List<AbstractText> AbstractText { get; set; }
        [XmlElement(ElementName = "CopyrightInformation")]
        public string CopyrightInformation { get; set; }
    }

    [XmlRoot(ElementName = "AffiliationInfo")]
    public class AffiliationInfo
    {
        [XmlElement(ElementName = "Affiliation")]
        public string Affiliation { get; set; }
    }

    [XmlRoot(ElementName = "Author")]
    public class Author
    {
        [XmlElement(ElementName = "LastName")]
        public string LastName { get; set; }
        [XmlElement(ElementName = "ForeName")]
        public string ForeName { get; set; }
        [XmlElement(ElementName = "Initials")]
        public string Initials { get; set; }
        [XmlElement(ElementName = "AffiliationInfo")]
        public AffiliationInfo AffiliationInfo { get; set; }
        [XmlAttribute(AttributeName = "ValidYN")]
        public string ValidYN { get; set; }
        [XmlElement(ElementName = "Suffix")]
        public string Suffix { get; set; }
        [XmlElement(ElementName = "Identifier")]
        public Identifier Identifier { get; set; }
    }

    [XmlRoot(ElementName = "AuthorList")]
    public class AuthorList
    {
        [XmlElement(ElementName = "Author")]
        public List<Author> Author { get; set; }
        [XmlAttribute(AttributeName = "CompleteYN")]
        public string CompleteYN { get; set; }
    }

    [XmlRoot(ElementName = "PublicationType")]
    public class PublicationType
    {
        [XmlAttribute(AttributeName = "UI")]
        public string UI { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "PublicationTypeList")]
    public class PublicationTypeList
    {
        [XmlElement(ElementName = "PublicationType")]
        public List<PublicationType> PublicationType { get; set; }
    }

    [XmlRoot(ElementName = "ArticleDate")]
    public class ArticleDate
    {
        [XmlElement(ElementName = "Year")]
        public string Year { get; set; }
        [XmlElement(ElementName = "Month")]
        public string Month { get; set; }
        [XmlElement(ElementName = "Day")]
        public string Day { get; set; }
        [XmlAttribute(AttributeName = "DateType")]
        public string DateType { get; set; }
    }

    [XmlRoot(ElementName = "Article")]
    public class Article
    {
        [XmlElement(ElementName = "Journal")]
        public Journal Journal { get; set; }
        [XmlElement(ElementName = "ArticleTitle")]
        public string ArticleTitle { get; set; }
        [XmlElement(ElementName = "Pagination")]
        public Pagination Pagination { get; set; }
        [XmlElement(ElementName = "ELocationID")]
        public List<ELocationID> ELocationID { get; set; }
        [XmlElement(ElementName = "Abstract")]
        public Abstract Abstract { get; set; }
        [XmlElement(ElementName = "AuthorList")]
        public AuthorList AuthorList { get; set; }
        [XmlElement(ElementName = "Language")]
        public string Language { get; set; }
        [XmlElement(ElementName = "PublicationTypeList")]
        public PublicationTypeList PublicationTypeList { get; set; }
        [XmlElement(ElementName = "ArticleDate")]
        public ArticleDate ArticleDate { get; set; }
        [XmlAttribute(AttributeName = "PubModel")]
        public string PubModel { get; set; }
        [XmlElement(ElementName = "GrantList")]
        public GrantList GrantList { get; set; }
        [XmlElement(ElementName = "DataBankList")]
        public DataBankList DataBankList { get; set; }
    }

    [XmlRoot(ElementName = "MedlineJournalInfo")]
    public class MedlineJournalInfo
    {
        [XmlElement(ElementName = "Country")]
        public string Country { get; set; }
        [XmlElement(ElementName = "MedlineTA")]
        public string MedlineTA { get; set; }
        [XmlElement(ElementName = "NlmUniqueID")]
        public string NlmUniqueID { get; set; }
        [XmlElement(ElementName = "ISSNLinking")]
        public string ISSNLinking { get; set; }
    }

    [XmlRoot(ElementName = "NameOfSubstance")]
    public class NameOfSubstance
    {
        [XmlAttribute(AttributeName = "UI")]
        public string UI { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "Chemical")]
    public class Chemical
    {
        [XmlElement(ElementName = "RegistryNumber")]
        public string RegistryNumber { get; set; }
        [XmlElement(ElementName = "NameOfSubstance")]
        public NameOfSubstance NameOfSubstance { get; set; }
    }

    [XmlRoot(ElementName = "ChemicalList")]
    public class ChemicalList
    {
        [XmlElement(ElementName = "Chemical")]
        public List<Chemical> Chemical { get; set; }
    }

    [XmlRoot(ElementName = "CommentsCorrections")]
    public class CommentsCorrections
    {
        [XmlElement(ElementName = "RefSource")]
        public string RefSource { get; set; }
        [XmlElement(ElementName = "PMID")]
        public PMID PMID { get; set; }
        [XmlAttribute(AttributeName = "RefType")]
        public string RefType { get; set; }
    }

    [XmlRoot(ElementName = "CommentsCorrectionsList")]
    public class CommentsCorrectionsList
    {
        [XmlElement(ElementName = "CommentsCorrections")]
        public List<CommentsCorrections> CommentsCorrections { get; set; }
    }

    [XmlRoot(ElementName = "DescriptorName")]
    public class DescriptorName
    {
        [XmlAttribute(AttributeName = "MajorTopicYN")]
        public string MajorTopicYN { get; set; }
        [XmlAttribute(AttributeName = "UI")]
        public string UI { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "QualifierName")]
    public class QualifierName
    {
        [XmlAttribute(AttributeName = "MajorTopicYN")]
        public string MajorTopicYN { get; set; }
        [XmlAttribute(AttributeName = "UI")]
        public string UI { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "MeshHeading")]
    public class MeshHeading
    {
        [XmlElement(ElementName = "DescriptorName")]
        public DescriptorName DescriptorName { get; set; }
        [XmlElement(ElementName = "QualifierName")]
        public List<QualifierName> QualifierName { get; set; }
    }

    [XmlRoot(ElementName = "MeshHeadingList")]
    public class MeshHeadingList
    {
        [XmlElement(ElementName = "MeshHeading")]
        public List<MeshHeading> MeshHeading { get; set; }
    }

    [XmlRoot(ElementName = "OtherID")]
    public class OtherID
    {
        [XmlAttribute(AttributeName = "Source")]
        public string Source { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "MedlineCitation")]
    public class MedlineCitation
    {
        [XmlElement(ElementName = "PMID")]
        public PMID PMID { get; set; }
        [XmlElement(ElementName = "DateCreated")]
        public DateCreated DateCreated { get; set; }
        [XmlElement(ElementName = "DateCompleted")]
        public DateCompleted DateCompleted { get; set; }
        [XmlElement(ElementName = "DateRevised")]
        public DateRevised DateRevised { get; set; }
        [XmlElement(ElementName = "Article")]
        public Article Article { get; set; }
        [XmlElement(ElementName = "MedlineJournalInfo")]
        public MedlineJournalInfo MedlineJournalInfo { get; set; }
        [XmlElement(ElementName = "ChemicalList")]
        public ChemicalList ChemicalList { get; set; }
        [XmlElement(ElementName = "CitationSubset")]
        public List<string> CitationSubset { get; set; }
        [XmlElement(ElementName = "CommentsCorrectionsList")]
        public CommentsCorrectionsList CommentsCorrectionsList { get; set; }
        [XmlElement(ElementName = "MeshHeadingList")]
        public MeshHeadingList MeshHeadingList { get; set; }
        [XmlElement(ElementName = "OtherID")]
        public OtherID OtherID { get; set; }
        [XmlAttribute(AttributeName = "Owner")]
        public string Owner { get; set; }
        [XmlAttribute(AttributeName = "Status")]
        public string Status { get; set; }
        [XmlElement(ElementName = "KeywordList")]
        public KeywordList KeywordList { get; set; }
    }

    [XmlRoot(ElementName = "PubMedPubDate")]
    public class PubMedPubDate
    {
        [XmlElement(ElementName = "Year")]
        public string Year { get; set; }
        [XmlElement(ElementName = "Month")]
        public string Month { get; set; }
        [XmlElement(ElementName = "Day")]
        public string Day { get; set; }
        [XmlAttribute(AttributeName = "PubStatus")]
        public string PubStatus { get; set; }
        [XmlElement(ElementName = "Hour")]
        public string Hour { get; set; }
        [XmlElement(ElementName = "Minute")]
        public string Minute { get; set; }
    }

    [XmlRoot(ElementName = "History")]
    public class History
    {
        [XmlElement(ElementName = "PubMedPubDate")]
        public List<PubMedPubDate> PubMedPubDate { get; set; }
    }

    [XmlRoot(ElementName = "ArticleId")]
    public class ArticleId
    {
        [XmlAttribute(AttributeName = "IdType")]
        public string IdType { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "ArticleIdList")]
    public class ArticleIdList
    {
        [XmlElement(ElementName = "ArticleId")]
        public List<ArticleId> ArticleId { get; set; }
    }

    [XmlRoot(ElementName = "PubmedData")]
    public class PubmedData
    {
        [XmlElement(ElementName = "History")]
        public History History { get; set; }
        [XmlElement(ElementName = "PublicationStatus")]
        public string PublicationStatus { get; set; }
        [XmlElement(ElementName = "ArticleIdList")]
        public ArticleIdList ArticleIdList { get; set; }
    }

    [XmlRoot(ElementName = "PubmedArticle")]
    public class PubmedArticle
    {
        [XmlElement(ElementName = "MedlineCitation")]
        public MedlineCitation MedlineCitation { get; set; }
        [XmlElement(ElementName = "PubmedData")]
        public PubmedData PubmedData { get; set; }
    }

    [XmlRoot(ElementName = "Grant")]
    public class Grant
    {
        [XmlElement(ElementName = "GrantID")]
        public string GrantID { get; set; }
        [XmlElement(ElementName = "Acronym")]
        public string Acronym { get; set; }
        [XmlElement(ElementName = "Agency")]
        public string Agency { get; set; }
        [XmlElement(ElementName = "Country")]
        public string Country { get; set; }
    }

    [XmlRoot(ElementName = "GrantList")]
    public class GrantList
    {
        [XmlElement(ElementName = "Grant")]
        public List<Grant> Grant { get; set; }
        [XmlAttribute(AttributeName = "CompleteYN")]
        public string CompleteYN { get; set; }
    }

    [XmlRoot(ElementName = "Keyword")]
    public class Keyword
    {
        [XmlAttribute(AttributeName = "MajorTopicYN")]
        public string MajorTopicYN { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "KeywordList")]
    public class KeywordList
    {
        [XmlElement(ElementName = "Keyword")]
        public List<Keyword> Keyword { get; set; }
        [XmlAttribute(AttributeName = "Owner")]
        public string Owner { get; set; }
    }

    [XmlRoot(ElementName = "Identifier")]
    public class Identifier
    {
        [XmlAttribute(AttributeName = "Source")]
        public string Source { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "AccessionNumberList")]
    public class AccessionNumberList
    {
        [XmlElement(ElementName = "AccessionNumber")]
        public string AccessionNumber { get; set; }
    }

    [XmlRoot(ElementName = "DataBank")]
    public class DataBank
    {
        [XmlElement(ElementName = "DataBankName")]
        public string DataBankName { get; set; }
        [XmlElement(ElementName = "AccessionNumberList")]
        public AccessionNumberList AccessionNumberList { get; set; }
    }

    [XmlRoot(ElementName = "DataBankList")]
    public class DataBankList
    {
        [XmlElement(ElementName = "DataBank")]
        public DataBank DataBank { get; set; }
        [XmlAttribute(AttributeName = "CompleteYN")]
        public string CompleteYN { get; set; }
    }

    [XmlRoot(ElementName = "PubmedArticleSet")]
    public class PubmedArticleSet
    {
        [XmlElement(ElementName = "PubmedArticle")]
        public List<PubmedArticle> PubmedArticle { get; set; }
    }

}