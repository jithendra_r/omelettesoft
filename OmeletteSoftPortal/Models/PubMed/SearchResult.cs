﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OmeletteSoftPortal.Models.PubMed
{
    public class Header
    {
        public string type { get; set; }
        public string version { get; set; }
    }

    public class Translationset
    {
        public string from { get; set; }
        public string to { get; set; }
    }

    public class Esearchresult
    {
        public string count { get; set; }
        public string retmax { get; set; }
        public string retstart { get; set; }
        public List<string> idlist { get; set; }
        public List<Translationset> translationset { get; set; }
        public List<object> translationstack { get; set; }
        public string querytranslation { get; set; }
    }

    public class SearchResult
    {
        public Header header { get; set; }
        public Esearchresult esearchresult { get; set; }
    }
}