﻿using Newtonsoft.Json;
using OmeletteSoft.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OmeletteSoftPortal.Models
{
    public class AppUser
    {
        public string UserId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Email { get; set; }
        public string Prefix { get; set; }
        public string ImageId { get; set; }
        public string DeviceId { get; set; }
        public string DeviceInfo { get; set; }
        public string Application { get; set; }
        public bool? Verified { get; set; }
        [JsonIgnore]
        public int? AppDeviceId { get; set; }
        [JsonIgnore]
        public int? AppUserId { get; set; }
        public string FavoriteType { get; set; }
    }

    public class Error
    {
        public Error()
        {
            this.Code = 0;
            this.Message = string.Empty;
        }
        public int Code { get; set; }
        public string Message { get; set; }
    }

    public class AppUserOutput
    {
        public AppUserOutput()
        {
            this.UserInfo = new AppUser();
            this.ErrorDetails = new Error();
        }
        public AppUser UserInfo { get; set; }
        public Error ErrorDetails { get; set; }
    }

    public class FavoriteInput
    {
        public string Application { get; set; }
        public string FavoriteType { get; set; }
        public List<string> FavoriteValues { get; set; }
    }

    public class Favorite
    {
        public Favorite()
        {
            this.FavoriteType = string.Empty;
            this.FavoriteValues = new List<UploadedFileDetails>();
        }
        public string FavoriteType { get; set; }
        public List<UploadedFileDetails> FavoriteValues { get; set; }
    }

    public class UploadedFileDetails
    {
        public string FavoriteValue {get;set;}
        public string UploadId { get; set; }
        public string UploadPath { get; set; }
        public string FileName { get; set; }
        public SearchResultItem Details { get; set; }
    }
    public class FavoritesOutput
    {
        public FavoritesOutput()
        {
            this.Favorites = new List<Favorite>();
            this.ErrorDetails = new Error();
        }
        public List<Favorite> Favorites { get; set; }
        public Error ErrorDetails { get; set; }
    }
}