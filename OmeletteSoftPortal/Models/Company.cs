﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OmeletteSoft.Models
{
    //public class Company
    //{
    //    public Company() {
    //        this.CompanyId = 0;
    //    }
    //    public int CompanyId { get; set; }
    //    public Guid? PublicProfileId { get; set; }


    //    public int CompanyTypeId { get; set; }
    //    [Required(AllowEmptyStrings=false, ErrorMessage="Please enter Company Name.")]
    //    public String Name { get; set; }
    //    public String Description { get; set; }
    //    public String ImageURL { get; set; }
    //    public Guid? AdminUserId { get; set; }
    //    public String CompanyTypeName { get; set; }

    //    public String AdminUsername { get; set; }
    //    public String AdminPassword { get; set; }
    //}

    public class CompanyMember {
        public CompanyMember() {
            this.Phone = String.Empty;
        }
        public int UserId { get; set; }
        public Guid UniqueId { get; set; }
        public String Phone { get; set; }
        public String Email { get; set; }
        public int PhoneTypeId { get; set; }
        public int UserStatusId { get; set; }
        public String UserStatusText { get; set; }
        public decimal? Longitude { get; set; }
        public decimal? Latitude { get; set; }
        public String LocationDetails { get; set; }
        public DateTimeOffset? LastSyncDate { get; set; }
        public bool? IsSyncAllowed { get; set; }
        public byte? SyncInterval { get; set; }
        public bool? SettingsEnabled { get; set; }
        public int? VisibilityOptionId { get; set; }
        public String PhoneTypeName { get; set; }
        public String VisibilityOptionName { get; set; }
        public int CompanyMemberId { get; set; }
        public int CompanyMemberStatusId { get; set; }
        public String CompanyMemberStatusName { get; set; }
        public String MemberName { get; set; }
    }

    public class CompanyMemberInput {
        public int Id { get; set; }
        public String Phone { get; set; }
        public String Name { get; set; }
        public String CountryCode { get; set; }
    }
}