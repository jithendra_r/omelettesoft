﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OmeletteSoft.Models
{
    public class Affiliation
    {
        public string en_US { get; set; }
    }

    public class CompetingInterests
    {
        public string en_US { get; set; }
    }

    public class Biography
    {
        public string en_US { get; set; }
    }

    public class Author
    {
        public string firstName { get; set; }
        public string middleName { get; set; }
        public string lastName { get; set; }
        public Affiliation affiliation { get; set; }
        public string country { get; set; }
        public string countryLocalized { get; set; }
        public string email { get; set; }
        public string url { get; set; }
        public CompetingInterests competingInterests { get; set; }
        public Biography biography { get; set; }
        public bool primaryContact { get; set; }
    }

    public class Galley
    {
        public string id { get; set; }
        public string label { get; set; }
        public string url { get; set; }
    }

    public class Article
    {
        public string url { get; set; }
        public string id { get; set; }
        public string title { get; set; }
        public string @abstract { get; set; }
        public string authorString { get; set; }
        public List<Author> authors { get; set; }
        public string sectionId { get; set; }
        public string sectionTitle { get; set; }
        public List<Galley> galleys { get; set; }
        public string citations { get; set; }
        public string sponsor { get; set; }
    }

    public class CurrentIssueWithArticles
    {
        public string url { get; set; }
        public string issueId { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string identification { get; set; }
        public string volume { get; set; }
        public string number { get; set; }
        public string year { get; set; }
        public string datePublished { get; set; }
        public string imageUrl { get; set; }
        public string imageDescription { get; set; }
        public List<Article> articles { get; set; }
    }

    public class CurrentIssueWithArticlesOutput
    {
        public CurrentIssueWithArticles CurrentIssueWithArticles { get; set; }
        public ErrorInfo Error { get; set; }
    }

    public class AllIssueDataWithArticlesOutput
    {
        public List<CurrentIssueWithArticles> IssuesWithArticles { get; set; }
        public ErrorInfo Error { get; set; }
    }

    public class Announcement
    {
        public string url { get; set; }
        public string id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string datePosted { get; set; }
    }

    public class AnnouncementOutput
    {
        public List<Announcement> Announcements { get; set; }
        public ErrorInfo Error { get; set; }
    }

    public class ArticleInfo
    {
        public string url { get; set; }
        public int id { get; set; }
        public string title { get; set; }
        public string @abstract { get; set; }
        public string authorString { get; set; }
        public List<Author> authors { get; set; }
        public string sectionId { get; set; }
        public string sectionTitle { get; set; }
        public List<Galley> galleys { get; set; }
        public string citations { get; set; }
    }

    public class ArticleInfoOutput
    {
        public ArticleInfo ArticleInfo { get; set; }
        public ErrorInfo Error { get; set; }
    }

    public class IssueDataOutput
    {
        public IssueData IssueData { get; set; }
        public ErrorInfo Error { get; set; }
    }
}