﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OmeletteSoft.Models
{
    public class PubMedModel
    {
    }

    public class SearchResultItem
    {
        public String PMID { get; set; }
        public String Id { get; set; }
        public DateTime? PubDate { get; set; }
        public DateTime? EPubDate { get; set; }
        public String Source { get; set; }
        public List<String> Authors { get; set; }
        public String Title { get; set; }
        public String Pages { get; set; }
        public String DOI { get; set; }
        public String SO { get; set; }
        public String PMCID { get; set; }
        public String PMC { get; set; }
        public String EID { get; set; }

        public string PII { get; set; }

        public String Abstract { get; set; }
        public String CopyrightInformation { get; set; }
        public DateTime? ArticleDate { get; set; }
        public Journal Journal { get; set; }
        public Citation Citation { get; set; }
    }
    public class Citation
    {
        public string TimesCited { get; set; }
        public string FieldCitationRatio { get; set; }
    }

    public class Journal
    {
        public Journal()
        {
            this.ImpactFactor = string.Empty;
            this.ISSNType = "Print";
        }
        public String ISSN { get; set; }
        public String ISSNType { get; set; }
        public String Volume { get; set; }
        public String Issue { get; set; }
        public DateTime? PubDate { get; set; }
        public String Title { get; set; }
        public String ISOAbbreviation { get; set; }
        public String ImpactFactor { get; set; }
    }
    public class SearchOutput
    {
        public int? TotalRecords { get; set; }
        public int? RetStart { get; set; }
        public int? RetMax { get; set; }
        public List<SearchResultItem> results { get; set; }
        public ErrorInfo Error { get; set; }
    }
    public class SearchInput
    {
        public String Query { get; set; }
        public int? Page { get; set; }
        public int? Rows { get; set; }
        public List<string> Ids { get; set; }
        public string SortField {get;set;}
    }
    public class ArticleId
    {
        public String pmid { get; set; }
        public String doi { get; set; }
        public String pmcid { get; set; }
    }

    public class CrossRefResultItem
    {
        public string Title {get;set;}
        public string DOI {get;set;}
        public string Volume {get;set;}
        public List<string> Author {get;set;}
        public string Page {get;set;}
        public string JournalName {get;set;}
        public string Issue { get; set; }
        public string PublishedOnline { get; set; }
        public string URL { get; set; }
        public string FullJournalName { get; set; }
        public string ISSN { get; set; }
        public string ISSNType { get; set; }
        public string ImpactFactor { get; set; }
        public Citation Citation { get; set; }
    }
    public class CrossRefOutput
    {
        public int? TotalRecords { get; set; }
        public int? RetStart { get; set; }
        public int? RetMax { get; set; }
        public List<CrossRefResultItem> results { get; set; }
        public ErrorInfo Error { get; set; }
    }

    public class JIFHeaderResulItem
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Title20 { get; set; }
        public string Title29 { get; set; }
        public string ISSN { get; set; }
        public string EISSN { get; set; }
        public string Category { get; set; }
        public string Country { get; set; }
        public string Publisher { get; set; }
        public string Language { get; set; }
        public string Website { get; set; }
        public string Frequency { get; set; }
        public string SocietyName { get; set; }
        public int CurrentYear { get; set; }
        public decimal ImpactFactor { get; set; }
        public string APC { get; set; }
        public string APCAmount { get; set; }
        public string Currency { get; set; }
        public string SubmissionFee { get; set; }
        public string Keywords { get; set; }
        public string ReviewProcess { get; set; }
        public string OpenAccess { get; set; }
        public DALayer.JournalHeaderCounters Counters { get; set; }
    }

    public class JIFHeaderOutput
    {
        public List<JIFHeaderResulItem> results { get; set; }
        public ErrorInfo Error { get; set; }
    }

    public class JIFImpactFactorOutput
    {
        public List<JIFImpactFactor> results { get; set; }
        public ErrorInfo Error { get; set; }
    }
    public class JIFImpactFactor
    {
        public int Year { get; set; }
        public decimal Factor { get; set; }
    }

    public class ISSNImpactFactor
    {
        public string ISSN { get; set; }
        public string EISSN { get; set; }
        public int Year { get; set; }
        public string ImpactFactor { get; set; }
    }

    public class ISSNImpactFactorOutput
    {
        public List<ISSNImpactFactor> Items { get; set; }
        public ErrorInfo Error { get; set; }
    }

    public class ISSNImpactFactorInput
    {
        public List<string> ISSN { get; set; }
        public List<string> EISSN { get; set; }
    }

}