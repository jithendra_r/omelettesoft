﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace OmeletteSoftPortal.Models
{
    // Models used as parameters to AccountController actions.

    public class AddExternalLoginBindingModel
    {
        [Required]
        [Display(Name = "External access token")]
        public string ExternalAccessToken { get; set; }
    }

    public class ChangePasswordBindingModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class RegisterBindingModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [StringLength(10, ErrorMessage = "The {0} should not exceed 10 characters.")]
        [DataType(DataType.Text)]
        [Display(Name = "Prefix")]
        public string Prefix { get; set; }

        [Required]
        [StringLength(200, ErrorMessage = "The {0} should not exceed 200 characters.")]
        [DataType(DataType.Text)]
        [Display(Name = "FirstName")]
        public string FirstName { get; set; }

        [StringLength(100, ErrorMessage = "The {0} should not exceed 100 characters.")]
        [DataType(DataType.Text)]
        [Display(Name = "MiddleName")]
        public string MiddleName { get; set; }

        [Required]
        [StringLength(200, ErrorMessage = "The {0} should not exceed 200 characters.")]
        [DataType(DataType.Text)]
        [Display(Name = "LastName")]
        public string LastName { get; set; }
    }

    public class UserDetailsBindingModel
    {
        [Required]
        [StringLength(10, ErrorMessage = "The {0} should not exceed 10 characters.")]
        [DataType(DataType.Text)]
        [Display(Name = "Prefix")]
        public string Prefix { get; set; }

        [Required]
        [StringLength(200, ErrorMessage = "The {0} should not exceed 200 characters.")]
        [DataType(DataType.Text)]
        [Display(Name = "FirstName")]
        public string FirstName { get; set; }

        [StringLength(100, ErrorMessage = "The {0} should not exceed 100 characters.")]
        [DataType(DataType.Text)]
        [Display(Name = "MiddleName")]
        public string MiddleName { get; set; }

        [Required]
        [StringLength(200, ErrorMessage = "The {0} should not exceed 200 characters.")]
        [DataType(DataType.Text)]
        [Display(Name = "LastName")]
        public string LastName { get; set; }

        public Guid? ImageId { get; set; }
    }

    public class RegisterExternalBindingModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class RemoveLoginBindingModel
    {
        [Required]
        [Display(Name = "Login provider")]
        public string LoginProvider { get; set; }

        [Required]
        [Display(Name = "Provider key")]
        public string ProviderKey { get; set; }
    }

    public class SetPasswordBindingModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }


    public class VerifyCodeViewBindingModel
    {
        [Required]
        [Display(Name = "User Id")]
        public string UserId { get; set; }
        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
    }

    public class ValidateOutput
    {
        public ValidateOutput()
        {
            this.UserInfo = new UserInfo();
            this.Error = new Error();
        }
        public UserInfo UserInfo { get; set; }
        public Error Error { get; set; }
    }

    public class UserInfo
    {
        public string AspnetUserId { get; set; }
        public string Prefix { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ImagePath { get; set; }
        public bool? EmailConfirmed { get; set; }
    }
    public class ValidateBindingModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }
    public class SetPasswordViewBindingModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        [Required]
        [Display(Name = "New Password")]
        public string NewPassword { get; set; }
        [Required]
        [Compare("NewPassword", ErrorMessage = "New Password and Confirm Password are not same")]
        [Display(Name = "Confirm New Password")]
        public string ConfirmNewPassword { get; set; }
    }
}
