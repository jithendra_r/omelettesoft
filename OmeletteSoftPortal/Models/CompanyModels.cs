﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OmeletteSoft.Models
{
    public class ErrorInfo
    {
        public int ErrorId { get; set; }
        public String ErrorDescription { get; set; }
    }
    public class Company
    {
        public Guid CompanyId { get; set; }
        public String Name { get; set; }
        public String Address { get; set; }
        public String Phone { get; set; }
        public String Email { get; set; }
        public bool iPhoneEnabled { get; set; }
        public bool andriodEnabled { get; set; }

        public ErrorInfo Error { get; set; }
    }

    public class CompanyJournal
    {
        public String JournalId { get; set; }
        public String JournalName { get; set; }
    }

    public class JournalInfoOutput
    {
        public List<CompanyJournal> Journals { get; set; }
        public List<StaticLink> StaticLinks { get; set; }
        public ErrorInfo Error { get; set; }
    }

    public class StaticLink
    {
        public String LinkName { get; set; }
        public String LinkDetails { get; set; }
    }

    public class InputRequest
    {
        public Guid CompanyId { get; set; }
        public String Input1 { get; set; }
        public String Input2 { get; set; }

    }

    public class JournalDetails
    {
        public string id { get; set; }
        public string title { get; set; }
        public string url { get; set; }
        public string initials { get; set; }
        public string description { get; set; }
    }
    public class JournalDetailsOutput
    {
        public JournalDetails JournalDetails { get; set; }
        public ErrorInfo Error { get; set; }
    }

    public class IssueData
    {
        public string url { get; set; }
        public string issueId { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string identification { get; set; }
        public string volume { get; set; }
        public string number { get; set; }
        public string year { get; set; }
        public string datePublished { get; set; }
        public string imageUrl { get; set; }
        public string imageDescription { get; set; }
    }

    public class IssuesDataOutput
    {
        public List<IssueData> IssueData { get; set; }
        public ErrorInfo Error { get; set; }
    }

    public class CurrentIssueData
    {
        public string url { get; set; }
        public string issueId { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string identification { get; set; }
        public string volume { get; set; }
        public string number { get; set; }
        public string year { get; set; }
        public string datePublished { get; set; }
        public string imageUrl { get; set; }
        public string imageDescription { get; set; }
    }

    public class CurrentIssueDataOutput
    {
        public CurrentIssueData CurrentIssueData { get; set; }
        public ErrorInfo Error { get; set; }
    }
    public class MailInput
    {

        public String Name { get; set; }
        public String Email { get; set; }
        public String Comments { get; set; }
        public Guid? CompanyId { get; set; }
        public string AppName { get; set; }
        public string PhoneType { get; set; }
    }
}