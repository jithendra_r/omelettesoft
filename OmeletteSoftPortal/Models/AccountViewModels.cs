﻿using System;
using System.Collections.Generic;

namespace OmeletteSoftPortal.Models
{
    // Models returned by AccountController actions.

    public class ExternalLoginViewModel
    {
        public string Name { get; set; }

        public string Url { get; set; }

        public string State { get; set; }
    }

    public class ManageInfoViewModel
    {
        public string LocalLoginProvider { get; set; }

        public string Email { get; set; }

        public IEnumerable<UserLoginInfoViewModel> Logins { get; set; }

        public IEnumerable<ExternalLoginViewModel> ExternalLoginProviders { get; set; }
    }

    public class UserInfoViewModel
    {
        public string Email { get; set; }

        public bool HasRegistered { get; set; }

        public string LoginProvider { get; set; }
    }
    public class UserInfoOutput
    {
        public UserInfoOutput()
        {
            UserInfo = new UserInfoViewModel2();
            Error = new Error();
        }
        public UserInfoViewModel2 UserInfo { get; set; }
        public Error Error { get; set; }
    }
    public class UserInfoViewModel2
    {
        public string UserId { get; set; }
        public string Email { get; set; }
        public bool IsActivated { get; set; }
        public string Prefix { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string ImagePath { get; set; }

    }

    public class UserLoginInfoViewModel
    {
        public string LoginProvider { get; set; }

        public string ProviderKey { get; set; }
    }

}
