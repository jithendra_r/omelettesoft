﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImportUtility
{
    public partial class fromImportUtil : Form
    {
        enum ImportType
        {
            None=0,
            JournalImpactType =1
        }
        private ImportType importType;
        public fromImportUtil()
        {
            InitializeComponent();
        }
        private Entities dbContext = new Entities();
        private void button1_Click(object sender, EventArgs e)
        {
            importType = ImportType.JournalImpactType;
            var fileDialogOutput = openFileDialog1.ShowDialog();
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            switch (importType)
            {
                case ImportType.JournalImpactType:
                    ImportJournalImpactFactor(openFileDialog1.FileNames.First());
                    break;
            }
        }
        private void ImportJournalImpactFactor(string fileName)
        {
            try
            {
                var jifDataTable = ReadDataTableFromCSV(fileName);
                int totalCount = jifDataTable.Rows.Count;
                int rowCount = 1;
                DataTable dtHeader = new DataTable();
                dtHeader.Columns.Add("Title", typeof(string));
                dtHeader.Columns.Add("Title20", typeof(string));
                dtHeader.Columns.Add("Title29", typeof(string));
                dtHeader.Columns.Add("ISSN", typeof(string));
                dtHeader.Columns.Add("EISSN", typeof(string));
                dtHeader.Columns.Add("JournalCategoryName", typeof(string));
                dtHeader.Columns.Add("CountryName", typeof(string));
                dtHeader.Columns.Add("PublisherName", typeof(string));
                dtHeader.Columns.Add("LanguageName", typeof(string));
                dtHeader.Columns.Add("WebSite", typeof(string));
                dtHeader.Columns.Add("Frequency", typeof(string));

                DataTable dtImpactFactor = new DataTable();
                dtImpactFactor.Columns.Add("Title", typeof(string));
                dtImpactFactor.Columns.Add("JournalYear", typeof(int));
                dtImpactFactor.Columns.Add("ImpactFactor", typeof(decimal));

                foreach (DataRow dataRow in jifDataTable.Rows)
                {
                    UpdateStatusLabel(string.Format("Processing row {0}/{1}", rowCount, totalCount));

                    var headerRow = dtHeader.NewRow();
                    headerRow["Title"] = Convert.ToString(dataRow["Full Journal Title"]).ToUpper();
                    headerRow["Title20"] = Convert.ToString(dataRow["Title20"]).ToUpper();
                    headerRow["Title29"] = Convert.ToString(dataRow["Title29"]).ToUpper();
                    headerRow["ISSN"] = Convert.ToString(dataRow["ISSN"]).ToUpper();
                    headerRow["EISSN"] = Convert.ToString(dataRow["EISSN"]).ToUpper();
                    headerRow["JournalCategoryName"] = Convert.ToString(dataRow["Category name"]).ToUpper();
                    headerRow["CountryName"] = Convert.ToString(dataRow["Country"]).ToUpper();
                    headerRow["PublisherName"] = "NA";
                    headerRow["LanguageName"] = "NA";
                    headerRow["Website"] = Convert.ToString(dataRow["Website"]).ToUpper();
                    headerRow["Frequency"] = Convert.ToString(dataRow["Frequency"]).ToUpper();
                    dtHeader.Rows.Add(headerRow);

                    foreach (DataColumn dataColumn in jifDataTable.Columns)
                    {
                        var columnName = dataColumn.ColumnName.ToUpper();
                        if (columnName.StartsWith("JOURNAL IF "))
                        {
                            decimal impactFactor;
                            if (Decimal.TryParse(Convert.ToString(dataRow[columnName]), out impactFactor))
                            { 
                                var ifRow = dtImpactFactor.NewRow();
                                ifRow["Title"] = Convert.ToString(dataRow["Full Journal Title"]);
                                ifRow["JournalYear"] = Convert.ToInt32(columnName.Replace("JOURNAL IF ", string.Empty));
                                ifRow["ImpactFactor"] = impactFactor;
                                dtImpactFactor.Rows.Add(ifRow);
                            }
                        }
                    }

                    rowCount++;

                    if (rowCount % 100 == 0)
                    {
                        SaveJournalHeader(dtHeader);
                        SaveJournalImpactFactors(dtImpactFactor);
                    }
                }

                SaveJournalHeader(dtHeader);
                SaveJournalImpactFactors(dtImpactFactor);
            }
            catch (Exception ex)
            {
                UpdateStatusLabel(string.Format("Error : {0}. {1}", ex.Message, ex.StackTrace));
            }
        }

        private void SaveJournalHeader(DataTable dt)
        {
            if (dt.Rows.Count > 0)
            {
                using (var db = new Entities())
                {
                    var parameter = new SqlParameter("@JIFHeaderTVP", SqlDbType.Structured);
                    parameter.Value = dt.Copy();
                    parameter.TypeName = "JIFHeaderTVP";

                    var rows = db.Database.ExecuteSqlCommand("EXEC dbo.SaveJIFHeader @JIFHeaderTVP", parameter);

                    dt.Rows.Clear();
                }
                
            }
        }

        private void SaveJournalImpactFactors(DataTable dt)
        {
            if (dt.Rows.Count > 0)
            {
                using (var db = new Entities())
                {
                    var parameter = new SqlParameter("@JIFChildTVP", SqlDbType.Structured);
                    parameter.Value = dt.Copy();
                    parameter.TypeName = "JIFChildTVP1";

                    dbContext.Database.ExecuteSqlCommand("exec dbo.SaveJIFYearFactors @JIFChildTVP", parameter);

                    dt.Rows.Clear();
                }
            }
        }

        private void SaveJournalImpactFactorAndYear(long journalHeaderId, int year, string impactFactorString)
        {
            decimal impactFactor;
            if (Decimal.TryParse(impactFactorString, out impactFactor))
            {
                dbContext.SaveJournalYearAndImpactFactor(journalHeaderId, year, impactFactor);
            }
        }

        private DataTable ReadDataTableFromCSV(string filePath)
        {
            if (!File.Exists(filePath))
                return new DataTable();

            string[] Lines = File.ReadAllLines(filePath);
            string[] Fields;
            Fields = Lines[0].Split(new char[] { ',' });
            int Cols = Fields.GetLength(0);
            DataTable dt = new DataTable();
            //1st row must be column names; force lower case to ensure matching later on.
            for (int i = 0; i < Cols; i++)
                dt.Columns.Add(Fields[i].ToUpper(), typeof(string));
            DataRow Row;
            for (int i = 1; i < Lines.Count(); i++)
            {
                //Fields = Lines[i].Split(new char[] { ',' });
                Regex CSVParser = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
                Fields = CSVParser.Split(Lines[i]);
                
                Row = dt.NewRow();
                for (int f = 0; f < Cols - 1; f++)
                    Row[f] = Fields[f];
                dt.Rows.Add(Row);
            }

            return dt;
        }
        private void UpdateStatusLabel(string message)
        {
            lblStatus.Text = message;
            lblStatus.Refresh();
        }
    }
}
