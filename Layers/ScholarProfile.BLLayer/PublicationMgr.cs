﻿using ScholarProfile.DLLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScholarProfile.BLLayer
{
    public class PublicationMgr
    {
        public static void SaveUserDOI(string userId, string doi, bool isActive)
        {
            using (var publicationsDA = new PublicationsDA())
            {
                publicationsDA.SaveUserDOI(userId, doi, isActive);
            }
        }

        public static List<GetUserPublications_Result> GetUserPublications(string userId)
        {
            using (var publicationsDA = new PublicationsDA())
            {
                return publicationsDA.GetUserPublications(userId);
            }
        }
    }
}
