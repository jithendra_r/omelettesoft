﻿using ScholarProfile.DLLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScholarProfile.BLLayer
{
    public class CommonMgr
    {
        public static List<GetCountries_Result> GetCountries()
        {
            using (var commonDA = new CommonDA())
            {
                return commonDA.GetCountries();
            }
        }
    }
}
