﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScholarProfile.DLLayer
{
    public class BaseDA : IDisposable
    {
        public Entities dbContext = new Entities();
        public BaseDA()
        {
            dbContext = new Entities();
        }
        public void Dispose()
        {

        }
    }
}
