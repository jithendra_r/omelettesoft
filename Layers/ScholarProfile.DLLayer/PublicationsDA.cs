﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScholarProfile.DLLayer
{
    public class PublicationsDA : BaseDA
    {
        public void SaveUserDOI(string userId, string doi, bool isActive)
        {
            dbContext.SaveUserToDOI(userId, doi, isActive);
        }

        public List<GetUserPublications_Result> GetUserPublications(string userId)
        {
            return dbContext.GetUserPublications(userId).ToList();
        }
    }
}
