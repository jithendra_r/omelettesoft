﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScholarProfile.DLLayer
{
    public class CommonDA : BaseDA
    {
        public List<GetCountries_Result> GetCountries()
        {
            return dbContext.GetCountries().ToList();
        }
    }
}
