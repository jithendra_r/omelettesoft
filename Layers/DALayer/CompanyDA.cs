﻿using DALayer;
using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALLayer
{
    public class CompanyDA : BaseDA
    {
        public List<GetCompanyInfo_Result> GetCompanyInfo(Guid companyId)
        {
            return dbContext.GetCompanyInfo(companyId).ToList();
        }

        public List<GetCompanyJournals_Result> GetCompanyJournals(Guid companyId)
        {
            return dbContext.GetCompanyJournals(companyId).ToList();
        }

        public List<GetCompanyLinks_Result> GetCompanyLinks(Guid companyId)
        {
            return dbContext.GetCompanyLinks(companyId).ToList();
        }

        public List<ImpactFactor> GetImpactFactor(List<String> iSSNList, string iSSNType)
        {
            var result = new List<ImpactFactor>();

            if (!iSSNList.Any())
                return result;

            var parameter = new SqlParameter("@ISSN", SqlDbType.Structured);
            parameter.Value = CreateSqlDataRecords(iSSNList);
            parameter.TypeName = "dbo.StringTVP";
            parameter.SqlDbType = SqlDbType.Structured;

            using (OmelleteSoftEntities db = new OmelleteSoftEntities())
            {
                var command = db.Database.Connection.CreateCommand();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "dbo.GetImpactFactorBasedISSN";
                command.Parameters.Add(parameter);
                command.Parameters.Add(new SqlParameter("ISSNType", iSSNType));
                try
                {

                    command.Connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            var issnPosition = reader.GetOrdinal("ISSN");
                            var impactFactorPosition = reader.GetOrdinal("JournalImpactFactor");
                            var journalYearPosition = reader.GetOrdinal("JournalYear");
                            while (reader.Read())
                            {
                                result.Add(new ImpactFactor()
                                {
                                    ISSN = reader.GetString(issnPosition),
                                    JournalImpactFactor = Convert.ToDecimal(reader.GetValue(impactFactorPosition)),
                                    JournalYear = Convert.ToInt32(reader.GetValue(journalYearPosition))
                                });
                            }
                        }
                    }
                }
                finally
                {
                    if(command.Connection.State != ConnectionState.Closed)
                        command.Connection.Close();
                }
            }

            return result;
        }


        public void SaveMissingISSNs(List<Tuple<String, String>> iSSNList)
        {
            var result = new List<ImpactFactor>();

            if (!iSSNList.Any())
                return;

            var parameter = new SqlParameter("@ISSN", SqlDbType.Structured);
            parameter.Value = CreateSqlDataRecords(iSSNList);
            parameter.TypeName = "dbo.StringStringTVP";
            parameter.SqlDbType = SqlDbType.Structured;

            using (OmelleteSoftEntities db = new OmelleteSoftEntities())
            {
                var command = db.Database.Connection.CreateCommand();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "dbo.SaveMissingISSNs";
                command.Parameters.Add(parameter);
                try
                {

                    command.Connection.Open();
                    command.ExecuteNonQuery();
                }
                finally
                {
                    if (command.Connection.State != ConnectionState.Closed)
                        command.Connection.Close();
                }
            }
        }

        private  IEnumerable<SqlDataRecord> CreateSqlDataRecords(IEnumerable<string> ids)
        {
            SqlMetaData[] metaData = new SqlMetaData[1];
            metaData[0] = new SqlMetaData("Value1",SqlDbType.VarChar, 250);
            SqlDataRecord record = new SqlDataRecord(metaData);
            foreach (var id in ids.Where(e=> e!=null))
            {
                record.SetString(0, id);
                yield return record;
            }
        }

        private IEnumerable<SqlDataRecord> CreateSqlDataRecords(IEnumerable<Tuple<string, string>> list)
        {
            SqlMetaData[] metaData = new SqlMetaData[2];
            metaData[0] = new SqlMetaData("Value1", SqlDbType.VarChar, 50);
            metaData[1] = new SqlMetaData("Value2", SqlDbType.VarChar, 50);
            SqlDataRecord record = new SqlDataRecord(metaData);
            foreach (var id in list)
            {
                record.SetString(0, id.Item1);
                record.SetString(1, id.Item2);
                yield return record;
            }
        }

        
    }
    public class ImpactFactor
    {
        public string ISSN { get; set; }
        public decimal JournalImpactFactor { get; set; }
        public int JournalYear { get; set; }

    }
}

