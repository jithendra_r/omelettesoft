﻿using DALLayer;
using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALayer
{
    public class AppUserDA : BaseDA
    {

        public void SaveAppUserUpload(string userId, Guid imageId, string path, string originalFileName, long fileSizeInKB)
        {
            dbContext.SaveAppUserUpload(userId, imageId, path, originalFileName, fileSizeInKB);
        }
        public void SaveAppUserFavorites(string userId, short favoriteTypeId, List<string> favoriteValues)
        {
            if (!favoriteValues.Any())
                return;

            var parameter = new SqlParameter("@FavoriteValues", SqlDbType.Structured);
            parameter.Value = CreateSqlDataRecords(favoriteValues);
            parameter.TypeName = "dbo.StringTVP";
            parameter.SqlDbType = SqlDbType.Structured;

            using (OmelleteSoftEntities db = new OmelleteSoftEntities())
            {
                var command = db.Database.Connection.CreateCommand();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "dbo.SaveAppUserFavorites";
                command.Parameters.Add(parameter);
                command.Parameters.Add(new SqlParameter("UserId", userId));
                command.Parameters.Add(new SqlParameter("FavoriteTypeId", favoriteTypeId));
                try
                {

                    command.Connection.Open();
                    command.ExecuteNonQuery();
                }
                finally
                {
                    if (command.Connection.State != ConnectionState.Closed)
                        command.Connection.Close();
                }
            }
        }

        public List<GetAppUserFavorites_Result> GetAppUserFavorites(string userId, short? favoriteTypeId, Guid? imageId)
        {
            return dbContext.GetAppUserFavorites(userId, favoriteTypeId, imageId).ToList();
        }

        private IEnumerable<SqlDataRecord> CreateSqlDataRecords(IEnumerable<string> ids)
        {
            SqlMetaData[] metaData = new SqlMetaData[1];
            metaData[0] = new SqlMetaData("Value1", SqlDbType.VarChar, 250);
            SqlDataRecord record = new SqlDataRecord(metaData);
            foreach (var id in ids.Where(e => e != null))
            {
                record.SetString(0, id);
                yield return record;
            }
        }

        public List<GetPubMedAdminDashboard_Result> GetPubMedAdminDashboard()
        {
            return dbContext.GetPubMedAdminDashboard().ToList();
        }

        public List<SearchUser_Result> SearchUser(Guid? userId, string searchString, int resultCount)
        {
            return dbContext.SearchUser(userId, searchString, resultCount).ToList();
        }
        public List<string> DeleteFavorites(string userId, List<string> favoriteValues, byte favoriteTypeId)
        {
            var result = new List<string>();
            if (!favoriteValues.Any())
                return result;

            var parameter = new SqlParameter("@FavoriteValues", SqlDbType.Structured);
            parameter.Value = CreateSqlDataRecords(favoriteValues);
            parameter.TypeName = "dbo.StringTVP";
            parameter.SqlDbType = SqlDbType.Structured;

            using (OmelleteSoftEntities db = new OmelleteSoftEntities())
            {
                var command = db.Database.Connection.CreateCommand();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "dbo.DeleteFavorites";
                command.Parameters.Add(parameter);
                command.Parameters.Add(new SqlParameter("UserId", userId));
                command.Parameters.Add(new SqlParameter("FavoriteTypeId", favoriteTypeId));

                try
                {

                    command.Connection.Open();
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            result.Add(reader["ImagePath"].ToString());
                        }
                    }
                }
                finally
                {
                    if (command.Connection.State != ConnectionState.Closed)
                        command.Connection.Close();
                }
            }
            return result;
        }

        public void DeleteFailedRecord(int deletionFailedFileId)
        {
            dbContext.DeleteFailedDeletionRecord(deletionFailedFileId);
        }
        public List<GetFailedDeletionList_Result> GetFailedDeletionList(int rows = 50)
        {
            return dbContext.GetFailedDeletionList(rows).ToList();
        }
        public void SaveFailedDeletionRecord(string userId, string failedFilePath)
        {
            dbContext.SaveFailedDeletionRecord(failedFilePath, userId);
        }
    }
}
