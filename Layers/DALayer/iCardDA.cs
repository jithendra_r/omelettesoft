﻿using Entities.iCard;
using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALayer
{
    public class iCardDA : IDisposable
    {
        private omelette_CardsEntities dbContext;
        public iCardDA()
        {
            dbContext = new omelette_CardsEntities();
        }

        public void DeleteContactCommunication(Guid contactUniqueId, short communicationType)
        {
            dbContext.DeleteContactCommunications(contactUniqueId, communicationType);
        }

        public void DeleteContactSocial(Guid contactUniqueId, short socialType)
        {
            dbContext.DeleteContactSocial(contactUniqueId, socialType);
        }

        public void DeleteContatLink(Guid contactUniqueId, Guid linkUniqueId)
        {
            dbContext.DeleteContactLink(contactUniqueId, linkUniqueId);
        }

        public ContactDetails GetContactDetails(Guid contactUniqueId)
        {
            var result = new ContactDetails();
            using (var db = new omelette_CardsEntities())
            {
                // If using Code First we need to make sure the model is built before we open the connection
                // This isn't required for models created with the EF Designer
                db.Database.Initialize(force: false);

                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "EXEC [dbo].[GetContactDetails] @ContactUniqueId";
                cmd.CommandType = System.Data.CommandType.Text;
                var param = cmd.CreateParameter();
                param.ParameterName = "@ContactUniqueId";
                param.Value = contactUniqueId;
                param.DbType = System.Data.DbType.Guid;
                param.Direction = System.Data.ParameterDirection.Input;

                cmd.Parameters.Add(param);
                try
                {

                    db.Database.Connection.Open();

                    var reader = cmd.ExecuteReader();
                    // Contact Details
                    result.Contact = ((IObjectContextAdapter)db)
                        .ObjectContext
                        .Translate<Contact>(reader).FirstOrDefault();

                    //Communications
                    reader.NextResult();
                    result.Communications = ((IObjectContextAdapter)db)
                        .ObjectContext
                        .Translate<Communication>(reader).ToList();

                    //ContactSocials
                    reader.NextResult();
                    result.Socials = ((IObjectContextAdapter)db)
                        .ObjectContext
                        .Translate<ContactSocial>(reader).ToList();

                    //ContactLinks
                    reader.NextResult();
                    result.Links = ((IObjectContextAdapter)db)
                        .ObjectContext
                        .Translate<Contact>(reader).ToList();

                    //Contact Tempaltes
                    reader.NextResult();
                    result.Templates = ((IObjectContextAdapter)db)
                        .ObjectContext
                        .Translate<CardTemplate>(reader).ToList();
                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }

            return result;
        }

        public void SaveContactCommunications(Guid contactUniqueId, List<Communication> communications)
        {
            using (var db = new omelette_CardsEntities())
            {
                var parameters = new List<SqlParameter>() {
                    new SqlParameter("@ContactUniqueId", contactUniqueId){ DbType=System.Data.DbType.Guid},
                    new SqlParameter("@TVP", System.Data.SqlDbType.Structured){Value = Communication.GetDataTable(communications), TypeName="dbo.SmallIntNVarChar" }
                };
                db.Database.ExecuteSqlCommand("exec dbo.SaveContactCommunications @ContactUniqueId, @TVP", parameters.ToArray());
            }
        }

        public void SaveContactSocials(Guid contactUniqueId, List<ContactSocial> socials)
        {
            using (var db = new omelette_CardsEntities())
            {
                var parameters = new List<SqlParameter>() {
                    new SqlParameter("@ContactUniqueId", contactUniqueId){ DbType=System.Data.DbType.Guid},
                    new SqlParameter("@TVP", System.Data.SqlDbType.Structured){Value = ContactSocial.GetDataTable(socials), TypeName="dbo.SmallIntNVarChar" }
                };
                db.Database.ExecuteSqlCommand("exec dbo.SaveContactSocials @ContactUniqueId, @TVP", parameters.ToArray());
            }
        }

        public void SaveContactLink(Guid contactUniqueId, Guid linkUniqueId)
        {
            dbContext.SaveConatctLink(contactUniqueId, linkUniqueId);
        }

        public void SaveContact(Guid uniqueId, string prefix, string firstName, string middleName, string lastName, string education, string occupation, string ImageName, 
            bool isPrivate, bool encrypt, string companyName)
        {
            dbContext.SaveContact(uniqueId, prefix, firstName, middleName, lastName, education, occupation, ImageName, isPrivate, encrypt, companyName);
        }

        public void DeleteContact(Guid uniqueId)
        {
            dbContext.DeleteContact(uniqueId);
        }
        public List<GetCommunicationTypes_Result> GetCommunicationTypes(bool? isActive, string communicationType = null)
        {
            return dbContext.GetCommunicationTypes(isActive, communicationType).ToList();
        }
        public List<GetSocialTypes_Result> GetSocialTypes(bool? isActive, string socialType = null)
        {
            return dbContext.GetSocialTypes(isActive, socialType).ToList();
        }
        public SaveActivationCode_Result SaveActivationCode(string userId, string activationCode)
        {
            return dbContext.SaveActivationCode(userId, activationCode).FirstOrDefault();
        }
        public MarkActivationCode_Result MarkActivationCode(string userId, string activationCode)
        {
            return dbContext.MarkActivationCode(userId, activationCode).FirstOrDefault();
        }

        public string GetActivationCode(string userId)
        {
            return dbContext.Database.SqlQuery<string>($"SELECT TOP 1 ActivationCode FROM dbo.UserActivationCodes (NOLOCK) WHERE UserId='{userId}'").FirstOrDefault();
        }

        public CardDetails GetCardDetails(Guid cardTemplateUniqueId)
        {
            var result = new CardDetails();
            using (var db = new omelette_CardsEntities())
            {
                // If using Code First we need to make sure the model is built before we open the connection
                // This isn't required for models created with the EF Designer
                db.Database.Initialize(force: false);

                // Create a SQL command to execute the sproc
                var cmd = db.Database.Connection.CreateCommand();
                cmd.CommandText = "EXEC [dbo].[GetCardDetails] @ContactCardTemplateUniqueId";
                cmd.CommandType = System.Data.CommandType.Text;
                var param = cmd.CreateParameter();
                param.ParameterName = "@ContactCardTemplateUniqueId";
                param.Value = cardTemplateUniqueId;
                param.DbType = System.Data.DbType.Guid;
                param.Direction = System.Data.ParameterDirection.Input;

                cmd.Parameters.Add(param);
                try
                {

                    db.Database.Connection.Open();

                    var reader = cmd.ExecuteReader();
                    // Contact Details
                    result.ContactDetails = ((IObjectContextAdapter)db)
                        .ObjectContext
                        .Translate<Card>(reader).FirstOrDefault();

                    //ContactSocials
                    reader.NextResult();
                    result.Socials = ((IObjectContextAdapter)db)
                        .ObjectContext
                        .Translate<ContactSocial>(reader).ToList();

                    //Communications
                    reader.NextResult();
                    result.Communications = ((IObjectContextAdapter)db)
                        .ObjectContext
                        .Translate<Communication>(reader).ToList();

                }
                finally
                {
                    db.Database.Connection.Close();
                }
            }

            return result;
        }

        public void SaveCommunicationType(short id, string name, bool isActive)
        {
            dbContext.SaveCommunicationType(id, name, isActive);
        }
        public List<GetCardTemplates_Result> GetCardTemplates(int companyId)
        {
            return dbContext.GetCardTemplates(true, companyId).ToList();
        }

        public void SaveCardTemplate(int cardId, string name, string template, bool isActive, short cost, int companyId)
        {
            dbContext.SaveCardTemplate(cardId, name, template, isActive, cost, companyId);
        }

        public int? ValidateAppKey(string appKey)
        {
            return dbContext.ValidateAppKey(appKey).FirstOrDefault();
        }

        public void SaveUserAppPurchase(Guid userId, string transactionDetails, decimal purchaseAmount)
        {
            dbContext.SaveUserAppPurchases(userId, transactionDetails, purchaseAmount, true);
        }
        public void ApplyContactCardTemplates(Guid userId, decimal purchaseAmount)
        {
            dbContext.ApplyContactCardTemplates(userId, purchaseAmount);
        }
        public void AssignCostToCard(decimal cost, int[] ids)
        {
            var parameter = new SqlParameter("@Ids", SqlDbType.Structured);
            parameter.Value = CreateSqlDataRecords(ids);
            parameter.TypeName = "dbo.BIGINTType";
            parameter.SqlDbType = SqlDbType.Structured;

            using (omelette_CardsEntities db = new omelette_CardsEntities())
            {
                var command = db.Database.Connection.CreateCommand();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "dbo.AssignCostToCard";
                command.Parameters.Add(parameter);
                command.Parameters.Add(new SqlParameter("Cost", cost));
                try
                {

                    command.Connection.Open();
                    command.ExecuteNonQuery();
                    
                }
                finally
                {
                    if (command.Connection.State != ConnectionState.Closed)
                        command.Connection.Close();
                }
            }
        }
        public void SaveSocialType(short id, string name, bool isActive)
        {
            dbContext.SaveSocialType(id, name, isActive);
        }

        private IEnumerable<SqlDataRecord> CreateSqlDataRecords(IEnumerable<int> ids)
        {
            SqlMetaData[] metaData = new SqlMetaData[1];
            metaData[0] = new SqlMetaData("BigIntValue", SqlDbType.BigInt);
            SqlDataRecord record = new SqlDataRecord(metaData);
            foreach (var id in ids)
            {
                record.SetInt64(0, id);
                yield return record;
            }
        }

        public void SaveCompany(int companyId, string name, string address, string logoName, string website, Guid modifiedBy)
        {
            dbContext.SaveCompany(companyId, name, address, logoName, website, modifiedBy.ToString());
        }
        public List<GetCompanies_Result> GetCompanies(int companyId, string name)
        {
            return dbContext.GetCompanies(companyId, name).ToList();
        }
        public void DeleteCompany(int companyId, Guid modifiedBy)
        {
            dbContext.DeleteCompany(companyId, modifiedBy.ToString());
        }
        public List<GetCompanyContacts_Result> GetCompanyContacts(int companyId, string userSearch, Guid? contactUniqueId)
        {
            return dbContext.GetCompanyContacts(companyId, userSearch, contactUniqueId).ToList();
        }

        public List<OmeletteSoft.Entities.Common.User> GetUsers(bool? emailConfirmed)
        {
            return dbContext.GetUsersList(emailConfirmed).Select(e => new OmeletteSoft.Entities.Common.User()
            {
                UserId = new Guid(e.Id),
                Email = e.Email,
                EmailConfirmed = e.EmailConfirmed,
                Username = e.Username
            }).ToList();
        }
        public void Dispose()
        {

        }
    }
}
