﻿using DALLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Microsoft.SqlServer.Server;

namespace DALayer
{
    public class JIFDA : BaseDA
    {
        public List<SearchJournal_Result> SearchJournal(string title, int rows, long journalHeaderId, string sortColumn = "Title", string sortOrder="ASC")
        {
            return dbContext.SearchJournal(title, rows, journalHeaderId, sortColumn, sortOrder).ToList();
        }

        public List<GetJournalImpactFactor_Result> GetJournalImpactFactor(long journalHeaderId)
        {
            return dbContext.GetJournalImpactFactor(journalHeaderId).ToList();
        }

        public List<GetHighImpactJournals_Result> GetHighImpactJournals(int rows, int? year, decimal? impactFactor)
        {
            return dbContext.GetHighImpactJournals(rows, year, impactFactor).ToList();
        }

        public long SaveJournalImpactFactorHeader(string title, string title20, string title29, string iSSN, string eISSN, 
            string journalCategoryName, string countryName, string publisherName, string languageName, string societyName, 
            string website, string frequency, string APC, string APCAmount, string Currency, string SubmissionFee, string Keywords, 
            string ReviewProcess, string OpenAccess)
        {
            return Convert.ToInt64( dbContext.SaveJournalImpactFactorHeader(title, title20, title29, iSSN, eISSN, journalCategoryName, 
                countryName, publisherName, languageName, societyName,website, frequency, APC, APCAmount, Currency, SubmissionFee, Keywords,
                ReviewProcess, OpenAccess).FirstOrDefault());
        }

        public long SaveJournalImpactFactors(long journalHeaderId, int journalYear, decimal impactFactor )
        {
            return  Convert.ToInt64( dbContext.SaveJournalYearAndImpactFactor(journalHeaderId, journalYear, impactFactor).FirstOrDefault());
        }

        public int GetMissingISSNCount()
        {
            return Convert.ToInt32(dbContext.GetMissingISSNCount().FirstOrDefault());
        }

        public List<GetMissingISSN_Result> GetMissingISSN(string searchString)
        {
            return dbContext.GetMissingISSN(searchString).ToList();
        }

        public void DeleteMissingISSN(long MissingISSNId)
        {
            dbContext.DeleteMissingISSN(MissingISSNId);
        }

        public void SaveJournalHeader(DataTable dt)
        {
            if (dt.Rows.Count > 0)
            {
                using (var db = new OmelleteSoftEntities())
                {
                    var parameter = new SqlParameter("@JIFHeaderTVP", SqlDbType.Structured);
                    parameter.Value = dt.Copy();
                    parameter.TypeName = "JIFHeaderTVP";

                    var rows = db.Database.ExecuteSqlCommand("EXEC dbo.SaveJIFHeader @JIFHeaderTVP", parameter);

                    dt.Rows.Clear();
                }

            }
        }
        public void SaveJournalImpactFactors(DataTable dt)
        {
            if (dt.Rows.Count > 0)
            {
                using (var db = new OmelleteSoftEntities())
                {
                    var parameter = new SqlParameter("@JIFChildTVP", SqlDbType.Structured);
                    parameter.Value = dt.Copy();
                    parameter.TypeName = "JIFChildTVP";

                    dbContext.Database.ExecuteSqlCommand("exec dbo.SaveJIFYearFactors @JIFChildTVP", parameter);

                    dt.Rows.Clear();
                }
            }
        }

        public List<GetJournalSummaryCount_Result> GetJournalSummaryCount()
        {
            return dbContext.GetJournalSummaryCount().ToList();
        }

        public List<SearchJournalByTitle_Result> SearchJournalByTitle(string startingString, int rowCount)
        {
            return dbContext.SearchJournalByTitle(startingString, rowCount).ToList();
        }

        public void DeleteJIFItem(long journalHeaderId)
        {
            dbContext.DeleteJIFItem(journalHeaderId);
        }


        public SaveJournalHeaderCounters_Result SaveJournalHeaderCounters(long journalHeaderId, string counter, int incrementCounter = 1)
        {
            return dbContext.SaveJournalHeaderCounters(journalHeaderId, counter, incrementCounter).FirstOrDefault();
        }

        public List<JournalHeaderCounters> GetJournalHeaderCounters(List<long> journalHeaderIds)
        {
            var result = new List<JournalHeaderCounters>();

            if (!journalHeaderIds.Any())
                return result;

            var parameter = new SqlParameter("@HeaderIds", SqlDbType.Structured);
            parameter.Value = CreateSqlDataRecords(journalHeaderIds);
            parameter.TypeName = "dbo.IntTVP";
            parameter.SqlDbType = SqlDbType.Structured;

            using (OmelleteSoftEntities db = new OmelleteSoftEntities())
            {
                var command = db.Database.Connection.CreateCommand();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "dbo.GetJournalHeaderCounters";
                command.Parameters.Add(parameter);
                try
                {

                    command.Connection.Open();
                    using (var reader = command.ExecuteReader())
                    {
                        var journalHeaderIdOrdinal = reader.GetOrdinal("JournalHeaderId");
                        var markedAsFavoriteOrdinal = reader.GetOrdinal("MarkedAsFavorite");
                        var viewedOrdinal = reader.GetOrdinal("Viewed");
                        var sharedOrdinal = reader.GetOrdinal("Shared");
                        var createdDateOrdinal = reader.GetOrdinal("CreatedDate");
                        var modifiedDateOrdinal = reader.GetOrdinal("ModifiedDate");

                        while (reader.Read())
                        {
                            result.Add(new JournalHeaderCounters()
                            {
                                JournalHeaderId = reader.GetInt64(journalHeaderIdOrdinal),
                                MarkedAsFavorites = reader.GetInt64(markedAsFavoriteOrdinal),
                                Viewed = reader.GetInt64(viewedOrdinal),
                                Shared = reader.GetInt64(sharedOrdinal)
                            });
                        }
                    }
                }
                finally
                {
                    if (command.Connection.State != ConnectionState.Closed)
                        command.Connection.Close();
                }
            }
            return result;
        }

        private IEnumerable<SqlDataRecord> CreateSqlDataRecords(IEnumerable<long> ids)
        {
            SqlMetaData[] metaData = new SqlMetaData[1];
            metaData[0] = new SqlMetaData("Value1", SqlDbType.VarChar, 250);
            SqlDataRecord record = new SqlDataRecord(metaData);
            foreach (var id in ids)
            {
                record.SetInt64(0, id);
                yield return record;
            }
        }
    }

    public class JournalHeaderCounters
    {
        public long JournalHeaderId { get; set; }
        public long MarkedAsFavorites { get; set; }
        public long Viewed { get; set; }
        public long Shared { get; set; }
    }
}
