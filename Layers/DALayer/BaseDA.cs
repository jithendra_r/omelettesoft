﻿using System;

namespace DALayer
{
    public class BaseDA : IDisposable
    {
        public OmelleteSoftEntities dbContext = new OmelleteSoftEntities();
        public BaseDA()
        {
            dbContext = new OmelleteSoftEntities();
        }
        public void Dispose()
        {

        }
    }
}
