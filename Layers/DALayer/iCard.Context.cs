﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DALayer
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class omelette_CardsEntities : DbContext
    {
        public omelette_CardsEntities()
            : base("name=omelette_CardsEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
    
        public virtual int DeleteContactCommunications(Nullable<System.Guid> contactUniqueId, Nullable<short> communicationType)
        {
            var contactUniqueIdParameter = contactUniqueId.HasValue ?
                new ObjectParameter("ContactUniqueId", contactUniqueId) :
                new ObjectParameter("ContactUniqueId", typeof(System.Guid));
    
            var communicationTypeParameter = communicationType.HasValue ?
                new ObjectParameter("CommunicationType", communicationType) :
                new ObjectParameter("CommunicationType", typeof(short));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("DeleteContactCommunications", contactUniqueIdParameter, communicationTypeParameter);
        }
    
        public virtual int DeleteContactLink(Nullable<System.Guid> contactUniqueId, Nullable<System.Guid> linkUniqueId)
        {
            var contactUniqueIdParameter = contactUniqueId.HasValue ?
                new ObjectParameter("ContactUniqueId", contactUniqueId) :
                new ObjectParameter("ContactUniqueId", typeof(System.Guid));
    
            var linkUniqueIdParameter = linkUniqueId.HasValue ?
                new ObjectParameter("LinkUniqueId", linkUniqueId) :
                new ObjectParameter("LinkUniqueId", typeof(System.Guid));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("DeleteContactLink", contactUniqueIdParameter, linkUniqueIdParameter);
        }
    
        public virtual int DeleteContactSocial(Nullable<System.Guid> contactUniqueId, Nullable<short> socialType)
        {
            var contactUniqueIdParameter = contactUniqueId.HasValue ?
                new ObjectParameter("ContactUniqueId", contactUniqueId) :
                new ObjectParameter("ContactUniqueId", typeof(System.Guid));
    
            var socialTypeParameter = socialType.HasValue ?
                new ObjectParameter("SocialType", socialType) :
                new ObjectParameter("SocialType", typeof(short));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("DeleteContactSocial", contactUniqueIdParameter, socialTypeParameter);
        }
    
        public virtual int SaveConatctLink(Nullable<System.Guid> contactUniqueId, Nullable<System.Guid> linkUniqueId)
        {
            var contactUniqueIdParameter = contactUniqueId.HasValue ?
                new ObjectParameter("ContactUniqueId", contactUniqueId) :
                new ObjectParameter("ContactUniqueId", typeof(System.Guid));
    
            var linkUniqueIdParameter = linkUniqueId.HasValue ?
                new ObjectParameter("LinkUniqueId", linkUniqueId) :
                new ObjectParameter("LinkUniqueId", typeof(System.Guid));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SaveConatctLink", contactUniqueIdParameter, linkUniqueIdParameter);
        }
    
        public virtual int DeleteContact(Nullable<System.Guid> uniqueId)
        {
            var uniqueIdParameter = uniqueId.HasValue ?
                new ObjectParameter("UniqueId", uniqueId) :
                new ObjectParameter("UniqueId", typeof(System.Guid));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("DeleteContact", uniqueIdParameter);
        }
    
        public virtual ObjectResult<GetSocialTypes_Result> GetSocialTypes(Nullable<bool> isActive, string socialTypeName)
        {
            var isActiveParameter = isActive.HasValue ?
                new ObjectParameter("IsActive", isActive) :
                new ObjectParameter("IsActive", typeof(bool));
    
            var socialTypeNameParameter = socialTypeName != null ?
                new ObjectParameter("SocialTypeName", socialTypeName) :
                new ObjectParameter("SocialTypeName", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetSocialTypes_Result>("GetSocialTypes", isActiveParameter, socialTypeNameParameter);
        }
    
        public virtual ObjectResult<GetCommunicationTypes_Result> GetCommunicationTypes(Nullable<bool> isActive, string communicationTypeName)
        {
            var isActiveParameter = isActive.HasValue ?
                new ObjectParameter("IsActive", isActive) :
                new ObjectParameter("IsActive", typeof(bool));
    
            var communicationTypeNameParameter = communicationTypeName != null ?
                new ObjectParameter("CommunicationTypeName", communicationTypeName) :
                new ObjectParameter("CommunicationTypeName", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetCommunicationTypes_Result>("GetCommunicationTypes", isActiveParameter, communicationTypeNameParameter);
        }
    
        public virtual ObjectResult<MarkActivationCode_Result> MarkActivationCode(string userId, string activationCode)
        {
            var userIdParameter = userId != null ?
                new ObjectParameter("UserId", userId) :
                new ObjectParameter("UserId", typeof(string));
    
            var activationCodeParameter = activationCode != null ?
                new ObjectParameter("ActivationCode", activationCode) :
                new ObjectParameter("ActivationCode", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<MarkActivationCode_Result>("MarkActivationCode", userIdParameter, activationCodeParameter);
        }
    
        public virtual ObjectResult<SaveActivationCode_Result> SaveActivationCode(string userId, string activationCode)
        {
            var userIdParameter = userId != null ?
                new ObjectParameter("UserId", userId) :
                new ObjectParameter("UserId", typeof(string));
    
            var activationCodeParameter = activationCode != null ?
                new ObjectParameter("ActivationCode", activationCode) :
                new ObjectParameter("ActivationCode", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SaveActivationCode_Result>("SaveActivationCode", userIdParameter, activationCodeParameter);
        }
    
        public virtual int DeleteContactCard(Nullable<System.Guid> contactCardTemplateUniqueId)
        {
            var contactCardTemplateUniqueIdParameter = contactCardTemplateUniqueId.HasValue ?
                new ObjectParameter("ContactCardTemplateUniqueId", contactCardTemplateUniqueId) :
                new ObjectParameter("ContactCardTemplateUniqueId", typeof(System.Guid));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("DeleteContactCard", contactCardTemplateUniqueIdParameter);
        }
    
        public virtual int SaveContactCardTemplate(Nullable<long> contactId, Nullable<int> cardTemplateId)
        {
            var contactIdParameter = contactId.HasValue ?
                new ObjectParameter("ContactId", contactId) :
                new ObjectParameter("ContactId", typeof(long));
    
            var cardTemplateIdParameter = cardTemplateId.HasValue ?
                new ObjectParameter("CardTemplateId", cardTemplateId) :
                new ObjectParameter("CardTemplateId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SaveContactCardTemplate", contactIdParameter, cardTemplateIdParameter);
        }
    
        public virtual int SaveCommunicationType(Nullable<short> communicationTypeId, string communicationTypeName, Nullable<bool> isActive)
        {
            var communicationTypeIdParameter = communicationTypeId.HasValue ?
                new ObjectParameter("CommunicationTypeId", communicationTypeId) :
                new ObjectParameter("CommunicationTypeId", typeof(short));
    
            var communicationTypeNameParameter = communicationTypeName != null ?
                new ObjectParameter("CommunicationTypeName", communicationTypeName) :
                new ObjectParameter("CommunicationTypeName", typeof(string));
    
            var isActiveParameter = isActive.HasValue ?
                new ObjectParameter("IsActive", isActive) :
                new ObjectParameter("IsActive", typeof(bool));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SaveCommunicationType", communicationTypeIdParameter, communicationTypeNameParameter, isActiveParameter);
        }
    
        public virtual ObjectResult<Nullable<int>> ValidateAppKey(string appKey)
        {
            var appKeyParameter = appKey != null ?
                new ObjectParameter("AppKey", appKey) :
                new ObjectParameter("AppKey", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<int>>("ValidateAppKey", appKeyParameter);
        }
    
        public virtual int SaveUserAppPurchases(Nullable<System.Guid> aspnetUserId, string transactionDetails, Nullable<decimal> purchaseAmount, Nullable<bool> enableNewCards)
        {
            var aspnetUserIdParameter = aspnetUserId.HasValue ?
                new ObjectParameter("AspnetUserId", aspnetUserId) :
                new ObjectParameter("AspnetUserId", typeof(System.Guid));
    
            var transactionDetailsParameter = transactionDetails != null ?
                new ObjectParameter("TransactionDetails", transactionDetails) :
                new ObjectParameter("TransactionDetails", typeof(string));
    
            var purchaseAmountParameter = purchaseAmount.HasValue ?
                new ObjectParameter("PurchaseAmount", purchaseAmount) :
                new ObjectParameter("PurchaseAmount", typeof(decimal));
    
            var enableNewCardsParameter = enableNewCards.HasValue ?
                new ObjectParameter("EnableNewCards", enableNewCards) :
                new ObjectParameter("EnableNewCards", typeof(bool));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SaveUserAppPurchases", aspnetUserIdParameter, transactionDetailsParameter, purchaseAmountParameter, enableNewCardsParameter);
        }
    
        public virtual int ApplyContactCardTemplates(Nullable<System.Guid> userId, Nullable<decimal> purchaseAmount)
        {
            var userIdParameter = userId.HasValue ?
                new ObjectParameter("UserId", userId) :
                new ObjectParameter("UserId", typeof(System.Guid));
    
            var purchaseAmountParameter = purchaseAmount.HasValue ?
                new ObjectParameter("PurchaseAmount", purchaseAmount) :
                new ObjectParameter("PurchaseAmount", typeof(decimal));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("ApplyContactCardTemplates", userIdParameter, purchaseAmountParameter);
        }
    
        public virtual int SaveSocialType(Nullable<short> socialTypeId, string socialTypeName, Nullable<bool> isActive)
        {
            var socialTypeIdParameter = socialTypeId.HasValue ?
                new ObjectParameter("SocialTypeId", socialTypeId) :
                new ObjectParameter("SocialTypeId", typeof(short));
    
            var socialTypeNameParameter = socialTypeName != null ?
                new ObjectParameter("SocialTypeName", socialTypeName) :
                new ObjectParameter("SocialTypeName", typeof(string));
    
            var isActiveParameter = isActive.HasValue ?
                new ObjectParameter("IsActive", isActive) :
                new ObjectParameter("IsActive", typeof(bool));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SaveSocialType", socialTypeIdParameter, socialTypeNameParameter, isActiveParameter);
        }
    
        public virtual int SaveContact(Nullable<System.Guid> uniqueId, string prefix, string firstName, string middleName, string lastName, string education, string occupation, string imageName, Nullable<bool> isPrivate, Nullable<bool> encrypt, string companyName)
        {
            var uniqueIdParameter = uniqueId.HasValue ?
                new ObjectParameter("UniqueId", uniqueId) :
                new ObjectParameter("UniqueId", typeof(System.Guid));
    
            var prefixParameter = prefix != null ?
                new ObjectParameter("Prefix", prefix) :
                new ObjectParameter("Prefix", typeof(string));
    
            var firstNameParameter = firstName != null ?
                new ObjectParameter("FirstName", firstName) :
                new ObjectParameter("FirstName", typeof(string));
    
            var middleNameParameter = middleName != null ?
                new ObjectParameter("MiddleName", middleName) :
                new ObjectParameter("MiddleName", typeof(string));
    
            var lastNameParameter = lastName != null ?
                new ObjectParameter("LastName", lastName) :
                new ObjectParameter("LastName", typeof(string));
    
            var educationParameter = education != null ?
                new ObjectParameter("Education", education) :
                new ObjectParameter("Education", typeof(string));
    
            var occupationParameter = occupation != null ?
                new ObjectParameter("Occupation", occupation) :
                new ObjectParameter("Occupation", typeof(string));
    
            var imageNameParameter = imageName != null ?
                new ObjectParameter("ImageName", imageName) :
                new ObjectParameter("ImageName", typeof(string));
    
            var isPrivateParameter = isPrivate.HasValue ?
                new ObjectParameter("IsPrivate", isPrivate) :
                new ObjectParameter("IsPrivate", typeof(bool));
    
            var encryptParameter = encrypt.HasValue ?
                new ObjectParameter("Encrypt", encrypt) :
                new ObjectParameter("Encrypt", typeof(bool));
    
            var companyNameParameter = companyName != null ?
                new ObjectParameter("CompanyName", companyName) :
                new ObjectParameter("CompanyName", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SaveContact", uniqueIdParameter, prefixParameter, firstNameParameter, middleNameParameter, lastNameParameter, educationParameter, occupationParameter, imageNameParameter, isPrivateParameter, encryptParameter, companyNameParameter);
        }
    
        public virtual int DeleteCompany(Nullable<int> companyId, string modifiedBy)
        {
            var companyIdParameter = companyId.HasValue ?
                new ObjectParameter("CompanyId", companyId) :
                new ObjectParameter("CompanyId", typeof(int));
    
            var modifiedByParameter = modifiedBy != null ?
                new ObjectParameter("ModifiedBy", modifiedBy) :
                new ObjectParameter("ModifiedBy", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("DeleteCompany", companyIdParameter, modifiedByParameter);
        }
    
        public virtual int SaveCompany(Nullable<int> companyId, string name, string address, string logoImage, string webSite, string modifiedBy)
        {
            var companyIdParameter = companyId.HasValue ?
                new ObjectParameter("CompanyId", companyId) :
                new ObjectParameter("CompanyId", typeof(int));
    
            var nameParameter = name != null ?
                new ObjectParameter("Name", name) :
                new ObjectParameter("Name", typeof(string));
    
            var addressParameter = address != null ?
                new ObjectParameter("Address", address) :
                new ObjectParameter("Address", typeof(string));
    
            var logoImageParameter = logoImage != null ?
                new ObjectParameter("LogoImage", logoImage) :
                new ObjectParameter("LogoImage", typeof(string));
    
            var webSiteParameter = webSite != null ?
                new ObjectParameter("WebSite", webSite) :
                new ObjectParameter("WebSite", typeof(string));
    
            var modifiedByParameter = modifiedBy != null ?
                new ObjectParameter("ModifiedBy", modifiedBy) :
                new ObjectParameter("ModifiedBy", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SaveCompany", companyIdParameter, nameParameter, addressParameter, logoImageParameter, webSiteParameter, modifiedByParameter);
        }
    
        public virtual ObjectResult<GetCardTemplates_Result> GetCardTemplates(Nullable<bool> isActive, Nullable<int> companyId)
        {
            var isActiveParameter = isActive.HasValue ?
                new ObjectParameter("IsActive", isActive) :
                new ObjectParameter("IsActive", typeof(bool));
    
            var companyIdParameter = companyId.HasValue ?
                new ObjectParameter("CompanyId", companyId) :
                new ObjectParameter("CompanyId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetCardTemplates_Result>("GetCardTemplates", isActiveParameter, companyIdParameter);
        }
    
        public virtual ObjectResult<GetCompanies_Result> GetCompanies(Nullable<int> companyId, string name)
        {
            var companyIdParameter = companyId.HasValue ?
                new ObjectParameter("CompanyId", companyId) :
                new ObjectParameter("CompanyId", typeof(int));
    
            var nameParameter = name != null ?
                new ObjectParameter("Name", name) :
                new ObjectParameter("Name", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetCompanies_Result>("GetCompanies", companyIdParameter, nameParameter);
        }
    
        public virtual ObjectResult<SaveCardTemplate_Result> SaveCardTemplate(Nullable<int> cardTemplateId, string cardTemplateName, string cardTemplatePath, Nullable<bool> isActive, Nullable<short> cost, Nullable<int> companyId)
        {
            var cardTemplateIdParameter = cardTemplateId.HasValue ?
                new ObjectParameter("CardTemplateId", cardTemplateId) :
                new ObjectParameter("CardTemplateId", typeof(int));
    
            var cardTemplateNameParameter = cardTemplateName != null ?
                new ObjectParameter("CardTemplateName", cardTemplateName) :
                new ObjectParameter("CardTemplateName", typeof(string));
    
            var cardTemplatePathParameter = cardTemplatePath != null ?
                new ObjectParameter("CardTemplatePath", cardTemplatePath) :
                new ObjectParameter("CardTemplatePath", typeof(string));
    
            var isActiveParameter = isActive.HasValue ?
                new ObjectParameter("IsActive", isActive) :
                new ObjectParameter("IsActive", typeof(bool));
    
            var costParameter = cost.HasValue ?
                new ObjectParameter("Cost", cost) :
                new ObjectParameter("Cost", typeof(short));
    
            var companyIdParameter = companyId.HasValue ?
                new ObjectParameter("CompanyId", companyId) :
                new ObjectParameter("CompanyId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SaveCardTemplate_Result>("SaveCardTemplate", cardTemplateIdParameter, cardTemplateNameParameter, cardTemplatePathParameter, isActiveParameter, costParameter, companyIdParameter);
        }
    
        public virtual ObjectResult<GetCompanyContacts_Result> GetCompanyContacts(Nullable<int> companyId, string userSearch, Nullable<System.Guid> uniqueId)
        {
            var companyIdParameter = companyId.HasValue ?
                new ObjectParameter("CompanyId", companyId) :
                new ObjectParameter("CompanyId", typeof(int));
    
            var userSearchParameter = userSearch != null ?
                new ObjectParameter("UserSearch", userSearch) :
                new ObjectParameter("UserSearch", typeof(string));
    
            var uniqueIdParameter = uniqueId.HasValue ?
                new ObjectParameter("UniqueId", uniqueId) :
                new ObjectParameter("UniqueId", typeof(System.Guid));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetCompanyContacts_Result>("GetCompanyContacts", companyIdParameter, userSearchParameter, uniqueIdParameter);
        }
    
        public virtual ObjectResult<GetUsersList_Result> GetUsersList(Nullable<bool> emailConfirmed)
        {
            var emailConfirmedParameter = emailConfirmed.HasValue ?
                new ObjectParameter("EmailConfirmed", emailConfirmed) :
                new ObjectParameter("EmailConfirmed", typeof(bool));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetUsersList_Result>("GetUsersList", emailConfirmedParameter);
        }
    }
}
