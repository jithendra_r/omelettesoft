﻿using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DALayer
{
    public class PubMedDA : BaseDA
    {
        public SaveActivationCode_Result SaveActivationCode(string userId, string activationCode)
        {
            return dbContext.SaveActivationCode(userId, activationCode).FirstOrDefault();
        }
        public MarkActivationCode_Result MarkActivationCode(string userId, string activationCode)
        {
            return dbContext.MarkActivationCode(userId, activationCode).FirstOrDefault();
        }

        public SaveUserDetails_Result SaveUserDetails(string userId, string prefix, string firstName, string middleName, string lastName, Guid? imageId)
        {
            return dbContext.SaveUserDetails(userId, prefix, firstName, middleName, lastName, imageId).FirstOrDefault();
        }

        public GetUserDetails_Result GetUserDetails(string userId)
        {
            return dbContext.GetUserDetails(userId).FirstOrDefault();
        }
        public List<OmeletteSoft.Entities.Common.User> GetUsers(bool? emailConfirmed)
        {
            return dbContext.GetUsersList(emailConfirmed).Select(e => new OmeletteSoft.Entities.Common.User()
            {
                UserId = new Guid(e.Id),
                Email = e.Email,
                EmailConfirmed = e.EmailConfirmed,
                Username = e.Username
            }).ToList();
        }
    }

}
