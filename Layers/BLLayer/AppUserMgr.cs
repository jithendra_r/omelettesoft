﻿using DALayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OmeletteSoft.BLLayer
{
    public class AppUserMgr
    {
        public static string GenerateEmailConfirmationToken(int appUserDeviceId)
        {
            var tokenParts = $"{DateTime.UtcNow.Ticks}|{appUserDeviceId}";
            return EncryptionMgr.Encrypt(tokenParts);
        }

        public static int DecodeEmailConfirmationToken(string token)
        {
            var TOKEN_VALID_DURATION_IN_MINUTES = 60;
            var tokenParts = EncryptionMgr.Decrypt(token).Split('|');
            if (tokenParts.Length == 2)
            {
                //Is Token Valid (Valid for 1 hour)
                var tokenTime = new DateTime(Convert.ToInt64(tokenParts[0]));
                if (tokenTime.AddMinutes(TOKEN_VALID_DURATION_IN_MINUTES) > DateTime.UtcNow)
                {
                    return Convert.ToInt32(tokenParts[1]);
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                return -1;
            }

        }

        public static void SaveAppUserUpload(string userId, Guid imageId, string path, string originalFileName, long fileSizeInKB)
        {
            using (var da = new AppUserDA())
            {
                da.SaveAppUserUpload(userId, imageId, path, originalFileName, fileSizeInKB);
            }

        }
        public static void SaveAppUserFavorites(string userId, short favoriteTypeId, List<string> favoriteValues)
        {
            using (var da = new AppUserDA())
            {
                da.SaveAppUserFavorites(userId, favoriteTypeId, favoriteValues);
            }
        }

        public static List<GetAppUserFavorites_Result> GetAppUserFavorites(string userId, short? favoriteTypeId, Guid? imageId)
        {
            using (var da = new AppUserDA())
            {
                return da.GetAppUserFavorites(userId, favoriteTypeId, imageId);
            }
        }

        public static List<GetPubMedAdminDashboard_Result> GetPubMedAdminDashboard()
        {
            using (var da = new AppUserDA())
            {
                return da.GetPubMedAdminDashboard();
            }
        }
        public static List<SearchUser_Result> SearchUser(Guid? userId, string searchString, int resultCount)
        {
            using (var da = new AppUserDA())
            {
                return da.SearchUser(userId, searchString, resultCount);
            }
        }
        public static List<string> DeleteFavorites(string userId, List<string> favoriteValues, byte favoriteTypeId)
        {
            using (var da = new AppUserDA())
            {
                return da.DeleteFavorites(userId, favoriteValues, favoriteTypeId);
            }
        }

        public static void DeleteFailedRecord(int deletionFailedFileId)
        {
            using (var da = new AppUserDA())
            {
                da.DeleteFailedRecord(deletionFailedFileId);
            }
        }
        public static List<GetFailedDeletionList_Result> GetFailedDeletionList(int rows = 50)
        {
            using (var da = new AppUserDA())
            {
                return da.GetFailedDeletionList(rows);
            }
        }
        public static void SaveFailedDeletionRecord(string userId, string failedFilePath)
        {
            using (var da = new AppUserDA())
            {
                da.SaveFailedDeletionRecord(userId, failedFilePath);
            }
        }
    }
}
