﻿using DALayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLLayer
{
    public class PubMedMgr
    {
        public static SaveActivationCode_Result SaveActivationCode(string userId, string activationCode)
        {
            using(var da = new PubMedDA())
            {
                return da.SaveActivationCode(userId, activationCode);
            }
        }
        public static MarkActivationCode_Result MarkActivationCode(string userId, string activationCode)
        {
            using (var da = new PubMedDA())
            {
                return da.MarkActivationCode(userId, activationCode);
            }
        }

        public static SaveUserDetails_Result SaveUserDetails(string userId, string prefix, string firstName, string middleName, string lastName, Guid? imageId)
        {
            using (var da = new PubMedDA())
            {
                return da.SaveUserDetails(userId, prefix, firstName, middleName, lastName, imageId);
            }
        }

        public static GetUserDetails_Result GetUserDetails(string userId)
        {
            using (var da = new PubMedDA())
            {
                return da.GetUserDetails(userId);
            }
        }
        public static List<OmeletteSoft.Entities.Common.User> GetUsers(bool? emailConfirmed)
        {
            using (var da = new iCardDA())
            {
                return da.GetUsers(emailConfirmed);
            }
        }
    }
}
