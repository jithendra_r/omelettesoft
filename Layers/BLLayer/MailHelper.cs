﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Configuration;
using System.Configuration;
using System.Data;
using System.Text;
using System.Net.Mail;
using System.Net.Mime;
using System.IO;

/// <summary>
/// Summary description for MailHelper
/// </summary>
namespace OmeletteSoft.BLLayer
{
    public class MailHelper
    {
        public static void SendMail(string EmailId, string subject, string body, string ccList)
        {
            var config = ConfigurationManager.GetSection("system.net/mailSettings/smtp") as SmtpSection;

            System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage(config.From, EmailId, subject, body);

            if (ccList.Length > 0)
            {
                foreach (String toEmail in ccList.Split(','))
                {
                    if (String.IsNullOrEmpty(toEmail) == false)
                    {
                        mailMessage.CC.Add(toEmail);
                    }
                }
            }

            mailMessage.IsBodyHtml = true;
            //System.Net.NetworkCredential mailAuthentication = new System.Net.NetworkCredential(config.Network.UserName, config.Network.Password);
            //mailAuthentication.Domain = config.Network.ClientDomain;
            System.Net.Mail.SmtpClient mailClient = new System.Net.Mail.SmtpClient();
            mailClient.EnableSsl = false;
            mailClient.UseDefaultCredentials = false;
            //mailClient.Credentials = mailAuthentication;
            mailClient.Send(mailMessage);
        }

        public static void SendMail(String toEmail, string subject, string body, string ccList, String fromEmail, String fromPassword, bool enableSsl=false, int port=25, string host="")
        {
            System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage(fromEmail, toEmail, subject, body);

            if (ccList.Length > 0)
            {
                foreach (String ccEmail in ccList.Split(','))
                {
                    if (String.IsNullOrEmpty(ccEmail) == false)
                    {
                        mailMessage.CC.Add(ccEmail);
                    }
                }
            }

            if (string.IsNullOrEmpty(fromEmail) || string.IsNullOrEmpty(fromPassword))
            {
                var smtp = new System.Net.Mail.SmtpClient();
                var credential = (smtp.Credentials as System.Net.NetworkCredential);
                fromEmail = credential.UserName;
                fromPassword = credential.Password;
            }

            //mailMessage.IsBodyHtml = true;
            //System.Net.NetworkCredential mailAuthentication = new System.Net.NetworkCredential(fromEmail, fromPassword);
            //System.Net.Mail.SmtpClient mailClient = new System.Net.Mail.SmtpClient();
            //mailClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            //mailClient.EnableSsl = enableSsl;
            ////mailClient.UseDefaultCredentials = false;
            //mailClient.Credentials = mailAuthentication;
            //mailClient.Host = host;
            //mailClient.Port = port;
            //mailClient.Send(mailMessage);


            MailMessage mailmessage = new MailMessage(fromEmail, toEmail, subject, body);
            mailmessage.IsBodyHtml = true;
            SmtpClient mailer = new SmtpClient();
            mailer.Credentials = new System.Net.NetworkCredential(fromEmail, fromPassword);
            mailer.Port = port;
            if(!string.IsNullOrEmpty(host))
                mailer.Host = host;
            //mailer.SendMailAsync(mailmessage);
            mailer.Send(mailmessage);
        }
    }
}