﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace OmeletteSoft.BLLayer
{
    public class CitationMgr
    {
        public static CitationResult GetCitationDetails(string doi)
        {
            try
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebClient client = new WebClient();
                client.Headers.Add("Content-Type", "application/json");
                string response = client.DownloadString(string.Format("http://metrics-api.dimensions.ai/doi/{0}", doi));
                if (string.IsNullOrEmpty(response))
                    return null;
                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                var jObject = (json_serializer.DeserializeObject(response) as Dictionary<string, object>);
                var result = new CitationResult();
                if(jObject.ContainsKey("times_cited"))
                    result.times_cited = Convert.ToString(jObject["times_cited"]);
                if (jObject.ContainsKey("field_citation_ratio"))
                    result.field_citation_ratio = Convert.ToString(jObject["field_citation_ratio"]);
                if (jObject.ContainsKey("highly_cited_1"))
                    result.highly_cited_1 = Convert.ToString(jObject["highly_cited_1"]);
                if (jObject.ContainsKey("highly_cited_10"))
                    result.highly_cited_10 = Convert.ToString(jObject["highly_cited_10"]);
                if (jObject.ContainsKey("highly_cited_5"))
                    result.highly_cited_5 = Convert.ToString(jObject["highly_cited_5"]);
                if (jObject.ContainsKey("recent_citations"))
                    result.recent_citations = Convert.ToString(jObject["recent_citations"]);
                if (jObject.ContainsKey("relative_citation_ratio"))
                    result.relative_citation_ratio = Convert.ToString(jObject["relative_citation_ratio"]);

                return result;
                //return JsonConvert.DeserializeObject<CitationResult>(response);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }

    public class CitationResult
    {
        public string doi { get; set; }
        public string times_cited { get; set; }
        public string recent_citations { get; set; }
        public string highly_cited_1 { get; set; }
        public string highly_cited_5 { get; set; }
        public string highly_cited_10 { get; set; }
        public string relative_citation_ratio { get; set; }
        public string field_citation_ratio { get; set; }
        public string license { get; set; }
    }
}
