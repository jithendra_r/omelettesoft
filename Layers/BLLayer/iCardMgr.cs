﻿using DALayer;
using Entities.iCard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLLayer
{
    public class iCardMgr
    {
        public static void DeleteContactCommunication(Guid contactUniqueId, short communicationType)
        {
            using (var da = new iCardDA())
            {
                da.DeleteContactCommunication(contactUniqueId, communicationType);
            }
        }
        public static void DeleteContactSocial(Guid contactUniqueId, short socialType)
        {
            using (var da = new iCardDA())
            {
                da.DeleteContactSocial(contactUniqueId, socialType);
            }
        }
        public static void DeleteContatLink(Guid contactUniqueId, Guid linkUniqueId)
        {
            using (var da = new iCardDA())
            {
                da.DeleteContatLink(contactUniqueId, linkUniqueId);
            }
        }

        public static ContactDetails GetContactDetails(Guid contactUniqueId)
        {
            using (var da = new iCardDA())
            {
                return da.GetContactDetails(contactUniqueId);
            }
        }
        public static void SaveContactCommunications(Guid contactUniqueId, List<Communication> communications)
        {
            using (var da = new iCardDA())
            {
                da.SaveContactCommunications(contactUniqueId, communications);
            }
        }

        public static void SaveContactSocials(Guid contactUniqueId, List<ContactSocial> socials)
        {
            using (var da = new iCardDA())
            {
                da.SaveContactSocials(contactUniqueId, socials);
            }
        }

        public static void SaveContactLink(Guid contactUniqueId, Guid linkUniqueId)
        {
            using (var da = new iCardDA())
            {
                da.SaveContactLink(contactUniqueId, linkUniqueId);
            }
        }

        public static void SaveContact(Guid uniqueId, string prefix, string firstName, string middleName, string lastName, 
            string education, string occupation, string ImageName, bool isPrivate, bool encrypt, string companyName)
        {
            using (var da = new iCardDA())
            {
                da.SaveContact(uniqueId, prefix, firstName, middleName, lastName, education, occupation, ImageName, isPrivate, encrypt, companyName);
            }
        }

        public static void DeleteContact(Guid uniqueId)
        {
            using (var da = new iCardDA())
            {
                da.DeleteContact(uniqueId);
            }
        }

        public static CardDetails GetCardDetails(Guid cardTemplateUniqueId)
        {
            using (var da = new iCardDA())
            {
                return da.GetCardDetails(cardTemplateUniqueId);
            }
        }

        public static Dictionary<short, string> GetCommunicationTypes(bool? isActive, string communicationType = null)
        {
            using (var da = new iCardDA())
            { 
                return da.GetCommunicationTypes(isActive, communicationType).ToDictionary(e=> e.CommunicationTypeId, e=> e.CommunicationTypeName);
            } 
        }
        public static List<GetCommunicationTypes_Result> GetCommunicationTypes()
        {
            using (var da = new iCardDA())
            {
                return da.GetCommunicationTypes(null);
            }
        }
        public static List<GetSocialTypes_Result> GetSocialTypes()
        {
            using (var da = new iCardDA())
            {
                return da.GetSocialTypes(null);
            }
        }
        public static Dictionary<short, string> GetSocialTypes(bool? isActive, string socialType = null)
        {
            using (var da = new iCardDA())
            {
                return da.GetSocialTypes(isActive, socialType).ToDictionary(e => e.SocialTypeId, e => e.SocialTypeName);
            }
        }
        public static SaveActivationCode_Result SaveActivationCode(string userId, string activationCode)
        {
            using (var da = new iCardDA())
            {
                return da.SaveActivationCode(userId, activationCode);
            }
        }
        public static MarkActivationCode_Result MarkActivationCode(string userId, string activationCode)
        {
            using (var da = new iCardDA())
            {
                return da.MarkActivationCode(userId, activationCode);
            }
        }

        public static void SaveCommunicationType(short id, string name, bool isActive)
        {
            using (var da = new iCardDA())
            {
                da.SaveCommunicationType(id, name, isActive);
            }
        }

        public static List<GetCardTemplates_Result> GetCardTemplates(int companyId)
        {
            using (var da = new iCardDA())
            {
                return da.GetCardTemplates(companyId);
            }   
        }
        public static void SaveCardTemplate(int cardId, string name, string template, bool isActive, short cost, int companyId)
        {
            using (var da = new iCardDA())
            {
                da.SaveCardTemplate(cardId, name, template, isActive, cost, companyId);
            }
        }

        public static int? ValidateAppKey(string appKey)
        {
            using (var da = new iCardDA())
            {
                return da.ValidateAppKey(appKey);
            }
        }
        public static void SaveUserAppPurchase(Guid userId, string transactionDetails, decimal purchaseAmount)
        {
            using (var da = new iCardDA())
            {
                da.SaveUserAppPurchase(userId, transactionDetails, purchaseAmount);
            } 
        }
        public static void ApplyContactCardTemplates(Guid userId, decimal purchaseAmount)
        {
            using (var da = new iCardDA())
            {
                da.ApplyContactCardTemplates(userId, purchaseAmount);
            } 
        }

        public static void AssignCostToCard(decimal cost, int[] ids)
        {
            using (var da = new iCardDA())
            {
                da.AssignCostToCard(cost, ids);
            }
        }
        public static void SaveSocialType(short id, string name, bool isActive)
        {
            using (var da = new iCardDA())
            {
                da.SaveSocialType(id, name, isActive);
            }
        }
        public static void SaveCompany(int companyId, string name, string address, string logoName, string website, Guid modifiedBy)
        {
            using (var da = new iCardDA())
            {
                da.SaveCompany(companyId, name, address, logoName, website, modifiedBy);
            } 
        }
        public static List<GetCompanies_Result> GetCompanies(int companyId, string name)
        {
            using (var da = new iCardDA())
            {
                return da.GetCompanies(companyId, name);
            }   
        }
        public static void DeleteCompany(int companyId, Guid modifiedBy)
        {
            using (var da = new iCardDA())
            {
                da.DeleteCompany(companyId, modifiedBy);
            }   
        }

        public static List<GetCompanyContacts_Result> GetCompanyContacts(int companyId, string userSearch, Guid? contactUniqueId)
        {
            using (var da = new iCardDA())
            {
                return da.GetCompanyContacts(companyId, userSearch, contactUniqueId);
            }   
        }

        public static List<OmeletteSoft.Entities.Common.User> GetUsers(bool? emailConfirmed)
        {
            using (var da = new iCardDA())
            {
                return da.GetUsers(emailConfirmed);
            }
        }
    }
}
