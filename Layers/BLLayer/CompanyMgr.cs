﻿using DALayer;
using DALLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OmeletteSoft.BLLayer
{
    public class CompanyMgr
    {
        public static GetCompanyInfo_Result GetCompanyInfo(Guid companyId)
        {
            using (CompanyDA companyDA = new CompanyDA())
            {
                return companyDA.GetCompanyInfo(companyId).FirstOrDefault();
            }
        }

        public static List<GetCompanyJournals_Result> GetCompanyJournals(Guid companyId)
        {
            using (CompanyDA companyDA = new CompanyDA())
            {
                return companyDA.GetCompanyJournals(companyId);
            }
        }
        public static List<GetCompanyLinks_Result> GetCompanyLinks(Guid companyId)
        {
            using (CompanyDA companyDA = new CompanyDA())
            {
                return companyDA.GetCompanyLinks(companyId);
            }
        }
        public static List<ImpactFactor> GetImpactFactor(List<String> iSSNList, string iSSNType)
        {
            using (CompanyDA companyDA = new CompanyDA())
            {
                return companyDA.GetImpactFactor(iSSNList, iSSNType);
            }
        }
        public static void SaveMissingISSNs(List<Tuple<String, String>> iSSNList)
        {
            using (CompanyDA companyDA = new CompanyDA())
            {
                companyDA.SaveMissingISSNs(iSSNList);
            }
        }

        
    }
}
