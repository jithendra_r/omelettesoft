﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OmeletteSoft.BLLayer
{
    public class ConstantMgr
    {
        public static String REST_ROUTE_SERVICE = "/gateway/plugin/RestPlugin/";

        public static class Routes
        {
            public static String JOURNAL_INFO = "journalInfo";
            public static String CURRENT_ISSUE_DATA = "currentIssueData";
            public static String CURRENT_ISSUE_DATA_WITH_ARTICLES = "currentIssueDataWithArticles";
            public static String ALL_ISSUE_DATA = "allIssueData";
            public static String ALL_ISSUE_DATA_WITH_ARTICLES = "allIssueDataWithArticles";
            public static String ISSUE_DATA = "issueData/";
            public static String ISSUE_DATA_WITH_ARTICLES = "issueDataWithArticles/";
            public static String ARTICLE_INFO = "articleInfo/";
            public static String ANNOUNCEMENTS = "announcements";
        }

        public static class PubMed
        {
            public static int DefaultRows = 250;


        }

        public static class ExactLocation
        {
            public static class ErrorMessages
            {
                public static String InvalidDeviceId = "Unauthorized request. Invalid Device Id.";
            }
        }
    }
}