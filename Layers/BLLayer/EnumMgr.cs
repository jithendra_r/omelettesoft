﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OmeletteSoft.BLLayer
{
    public class EnumMgr
    {
        public enum ApplicationEnum
        {
            PubMed=1,
            JournalImpactFactor=2
        }

        public enum FavoriteType
        {
            NotSet=0,
            History = 1,
            Favorites = 2,
            MyArticles = 3,
            PDFBox = 4,
            AudioBox = 5,
            Image =6
        }

        public enum JIFCounterName
        {
            Favorite=1,
            Viewed=2,
            Shared=3
        }
    }
}
