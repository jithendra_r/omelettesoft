﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OmeletteSoft.BLLayer
{
    public class ConfigMgr
    {
        public static String googleAPIKey
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["googleAPIKey"];
            }
        }
        public static String ContactUS_Email
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["ContactUS-Email"];
            }
        }
        public static String CCList
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["CCList"];
            }
        }
        public static String PubMedRootURL
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["PubMedRootURL"];
            }
        }
        public static String googlePushNotificationAPIKey
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["googlePushNotificationAPIKey"];
            }
        }

        public static String FTPHostURL
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["FTPHostURL"];
            }
        }
        public static String FTPUserName
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["FTPUserName"];
            }
        }
        public static String FTPPassword
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["FTPPassword"];
            }
        }
        public static String TestEmails
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["TestEmails"];
            }
        }
    }
}
