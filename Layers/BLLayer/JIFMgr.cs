﻿using DALayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BLLayer
{
    public class JIFMgr
    {
        public static List<SearchJournal_Result> SearchJournal(string title, int rows, long journalHeaderId, string sortColumn, string sortOrder)
        {
            using (var jifDA = new JIFDA())
            {
                return jifDA.SearchJournal(title, rows, journalHeaderId, sortColumn, sortOrder);
            }
        }

        public static List<GetJournalImpactFactor_Result> GetJournalImpactFactor(long journalHeaderId)
        {
            using (var jifDA = new JIFDA())
            {
                return jifDA.GetJournalImpactFactor(journalHeaderId);
            }
        }

        public static List<GetHighImpactJournals_Result> GetHighImpactJournals(int rows, int? year, decimal? impactFactor)
        {
            using (var jifDA = new JIFDA())
            {
                return jifDA.GetHighImpactJournals(rows, year, impactFactor);
            }
        }
        public static long SaveJournalImpactFactorHeader(string title, string title20, string title29, string iSSN, string eISSN,
            string journalCategoryName, string countryName, string publisherName, string languageName, string societyName,
            string website, string frequency, string APC, string APCAmount, string Currency, string SubmissionFee, string Keywords,
            string ReviewProcess, string OpenAccess)
        {
            using (var jifDA = new JIFDA())
            {
                return jifDA.SaveJournalImpactFactorHeader(title, title20, title29, iSSN, eISSN, journalCategoryName, countryName, publisherName, 
                    languageName, societyName, website, frequency, APC, APCAmount, Currency, SubmissionFee, Keywords,
                ReviewProcess, OpenAccess);
            }
        }

        public static long SaveJournalImpactFactors(long journalHeaderId, int journalYear, decimal impactFactor)
        {
            using (var jifDA = new JIFDA())
            {
                return jifDA.SaveJournalImpactFactors(journalHeaderId, journalYear, impactFactor);
            }
        }

        public static int GetMissingISSNCount()
        {
            using (var jifDA = new JIFDA())
            {
                return jifDA.GetMissingISSNCount();
            }
        }

        public static List<GetMissingISSN_Result> GetMissingISSN(string searchString)
        {
            using (var jifDA = new JIFDA())
            {
                return jifDA.GetMissingISSN(searchString);
            }
        }

        public static void DeleteMissingISSN(long MissingISSNId)
        {
            using (var jifDA = new JIFDA())
            {
                jifDA.DeleteMissingISSN(MissingISSNId);
            }
        }

        #region JIF Factor
        public static int ImportJournalImpactFactor(string fileName)
        {
            int rowCount = 1;
            using (var jifDA = new JIFDA())
            {
                try
                {
                    var jifDataTable = ReadDataTableFromCSV(fileName);
                    int totalCount = jifDataTable.Rows.Count;
                    
                    DataTable dtHeader = new DataTable();
                    dtHeader.Columns.Add("Title", typeof(string)).MaxLength = 2000;
                    dtHeader.Columns.Add("Title20", typeof(string)).MaxLength = 2000;
                    dtHeader.Columns.Add("Title29", typeof(string)).MaxLength = 2000;
                    dtHeader.Columns.Add("ISSN", typeof(string)).MaxLength = 50;
                    dtHeader.Columns.Add("EISSN", typeof(string)).MaxLength = 50;
                    dtHeader.Columns.Add("JournalCategoryName", typeof(string)).MaxLength = 500;
                    dtHeader.Columns.Add("CountryName", typeof(string)).MaxLength = 250;
                    dtHeader.Columns.Add("PublisherName", typeof(string)).MaxLength = 500;
                    dtHeader.Columns.Add("LanguageName", typeof(string)).MaxLength = 250;
                    dtHeader.Columns.Add("WebSite", typeof(string)).MaxLength = 250;
                    dtHeader.Columns.Add("Frequency", typeof(string)).MaxLength = 250;
                    dtHeader.Columns.Add("SocietyName", typeof(string)).MaxLength = 250;
                    dtHeader.Columns.Add("APC", typeof(string)).MaxLength = 250;
                    dtHeader.Columns.Add("APCAmount", typeof(string)).MaxLength = 250;
                    dtHeader.Columns.Add("Currency", typeof(string)).MaxLength = 250;
                    dtHeader.Columns.Add("SubmissionFee", typeof(string)).MaxLength = 250;
                    dtHeader.Columns.Add("Keywords", typeof(string)).MaxLength = 500;
                    dtHeader.Columns.Add("ReviewProcess", typeof(string)).MaxLength = 500;
                    dtHeader.Columns.Add("OpenAccess", typeof(string)).MaxLength = 250;


                    DataTable dtImpactFactor = new DataTable();
                    dtImpactFactor.Columns.Add("Title", typeof(string)).MaxLength=2000;
                    dtImpactFactor.Columns.Add("ISSN", typeof(string)).MaxLength = 50;
                    dtImpactFactor.Columns.Add("EISSN", typeof(string)).MaxLength = 50;
                    dtImpactFactor.Columns.Add("JournalYear", typeof(int));
                    dtImpactFactor.Columns.Add("ImpactFactor", typeof(decimal));

                    HashSet<string> issns = new HashSet<string>();
                    HashSet<string> issn_years = new HashSet<string>();

                    foreach (DataRow dataRow in jifDataTable.Rows)
                    {
                        var headerRow = dtHeader.NewRow();
                        var issn = Convert.ToString(dataRow["ISSN"]).ToUpper().Trim();
                        var title = Convert.ToString(dataRow["Full Journal Title"]).ToUpper().Trim();
                        headerRow["Title"] = title;
                        headerRow["Title20"] = jifDataTable.Columns.Contains("Title20") ? Convert.ToString(dataRow["Title20"]).ToUpper().Trim() : "";
                        headerRow["Title29"] = jifDataTable.Columns.Contains("Title29") ? Convert.ToString(dataRow["Title29"]).ToUpper().Trim() : "";
                        headerRow["ISSN"] = Convert.ToString(dataRow["ISSN"]).ToUpper().Trim();
                        headerRow["EISSN"] = jifDataTable.Columns.Contains("EISSN") ? Convert.ToString(dataRow["EISSN"]).ToUpper().Trim() : "";
                        headerRow["JournalCategoryName"] = jifDataTable.Columns.Contains("CategoryName") ? Convert.ToString(dataRow["CategoryName"]).ToUpper().Trim() : "";
                        headerRow["CountryName"] = jifDataTable.Columns.Contains("CountryName") ?  Convert.ToString(dataRow["CountryName"]).ToUpper().Trim() : "";
                        headerRow["PublisherName"] = jifDataTable.Columns.Contains("PublisherName") ? Convert.ToString(dataRow["PublisherName"]).ToUpper().Trim() : "";
                        headerRow["LanguageName"] = jifDataTable.Columns.Contains("LanguageName") ? Convert.ToString(dataRow["LanguageName"]).ToUpper().Trim() : "";
                        headerRow["Website"] = jifDataTable.Columns.Contains("WebSite") ? Convert.ToString(dataRow["Website"]).ToUpper().Trim() : "";
                        headerRow["Frequency"] = jifDataTable.Columns.Contains("Frequency") ? Convert.ToString(dataRow["Frequency"]).ToUpper().Trim() : "";
                        headerRow["SocietyName"] = jifDataTable.Columns.Contains("SocietyName") ? Convert.ToString(dataRow["SocietyName"]).ToUpper() : "";
                        headerRow["APC"] = jifDataTable.Columns.Contains("APC") ? Convert.ToString(dataRow["APC"]) : "";
                        headerRow["APCAmount"] = jifDataTable.Columns.Contains("APCAmount") ? Convert.ToString(dataRow["APCAmount"]) : "";
                        headerRow["Currency"] = jifDataTable.Columns.Contains("Currency") ? Convert.ToString(dataRow["Currency"]) : "";
                        headerRow["SubmissionFee"] = jifDataTable.Columns.Contains("SubmissionFee") ? Convert.ToString(dataRow["SubmissionFee"]) : "";
                        headerRow["Keywords"] = jifDataTable.Columns.Contains("Keywords") ? Convert.ToString(dataRow["Keywords"]) : "";
                        headerRow["ReviewProcess"] = jifDataTable.Columns.Contains("ReviewProcess") ? Convert.ToString(dataRow["ReviewProcess"]) : "";
                        headerRow["OpenAccess"] = jifDataTable.Columns.Contains("OpenAccess") ? Convert.ToString(dataRow["OpenAccess"]) : "";

                        if (issns.Add(issn))
                        {
                            dtHeader.Rows.Add(headerRow);

                            foreach (DataColumn dataColumn in jifDataTable.Columns)
                            {
                                var columnName = dataColumn.ColumnName.ToUpper();
                                if (columnName.StartsWith("JOURNAL IF "))
                                {
                                    decimal impactFactor;
                                    if (Decimal.TryParse(Convert.ToString(dataRow[columnName]), out impactFactor))
                                    {
                                        string eissn = string.Empty;
                                        var year = Convert.ToInt32(columnName.Replace("JOURNAL IF ", string.Empty));
                                        if (jifDataTable.Columns.Contains("EISSN"))
                                        {
                                            eissn= Convert.ToString(dataRow["EISSN"]).ToUpper();
                                        }

                                        if (issn_years.Add($"{issn}_{eissn}_{year}"))
                                        {
                                            var ifRow = dtImpactFactor.NewRow();
                                            ifRow["Title"] = title;
                                            ifRow["ISSN"] = issn;
                                            if (jifDataTable.Columns.Contains("EISSN"))
                                            {
                                                ifRow["EISSN"] = eissn;
                                            }
                                            ifRow["JournalYear"] = year;
                                            ifRow["ImpactFactor"] = impactFactor;
                                            dtImpactFactor.Rows.Add(ifRow);
                                        }
                                    }
                                }
                            }

                            rowCount++;

                            if (rowCount % 100 == 0)
                            {
                                jifDA.SaveJournalHeader(dtHeader);
                                jifDA.SaveJournalImpactFactors(dtImpactFactor);
                                issn_years = new HashSet<string>();
                            }
                        }
                    }

                    jifDA.SaveJournalHeader(dtHeader);
                    jifDA.SaveJournalImpactFactors(dtImpactFactor);

                    return rowCount;
                }
                catch (Exception ex)
                {
                    throw new Exception(string.Concat("Total procesed:", rowCount.ToString(), ".Error: ", ex.Message));
                }
            }
        }
        private static DataTable ReadDataTableFromCSV(string fileName)
        {
            if (!File.Exists(fileName))
                return new DataTable();

            string[] Lines = File.ReadAllLines(fileName);
            string[] Fields;
            Fields = Lines[0].Split(new char[] { ',' });
            int Cols = Fields.GetLength(0);
            DataTable dt = new DataTable();
            //1st row must be column names; force lower case to ensure matching later on.
            for (int i = 0; i < Cols; i++)
                dt.Columns.Add(Fields[i].ToUpper(), typeof(string));

            DataRow Row;
            for (int i = 1; i < Lines.Count(); i++)
            {
                //Fields = Lines[i].Split(new char[] { ',' });
                Regex CSVParser = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
                Fields = CSVParser.Split(Lines[i]);

                Row = dt.NewRow();
                for (int f = 0; f < Cols; f++)
                    Row[f] = Convert.ToString(Fields[f]).Trim();
                dt.Rows.Add(Row);
            }

            return dt;
        }
        public static List<GetJournalSummaryCount_Result> GetJournalSummaryCount()
        {
            using (var jifDA = new JIFDA()) {
                return jifDA.GetJournalSummaryCount();
            }
        }

        public static List<SearchJournalByTitle_Result> SearchJournalByTitle(string startingString, int rowCount)
        {
            using (var jifDA = new JIFDA())
            {
                return jifDA.SearchJournalByTitle(startingString, rowCount);
            } 
        }
        #endregion

        public static void DeleteJIFItem(long journalHeaderId)
        {
            using (var jifDA = new JIFDA())
            {
                jifDA.DeleteJIFItem(journalHeaderId);
            }
        }

        public static SaveJournalHeaderCounters_Result SaveJournalHeaderCounters(long journalHeaderId, string counter, int incrementCounter = 1)
        {
            using (var da = new JIFDA())
            {
                return da.SaveJournalHeaderCounters(journalHeaderId, counter, incrementCounter);
            }
        }

        public static List<JournalHeaderCounters> GetJournalHeaderCounters(List<long> journalHeaderIds)
        {
            using (var da = new JIFDA())
            {
                return da.GetJournalHeaderCounters(journalHeaderIds);
            }
        }
    }
}
