﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Entities.iCard
{
    public class ContactSocial
    {
        public Int16? SocialTypeId { get; set; }
        public string SocialTypeName { get; set; }
        public string SocialDetails { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public static DataTable GetDataTable(List<ContactSocial> input)
        {
            var resultDt = new DataTable();
            resultDt.Columns.Add("SmallIntValue", typeof(byte));
            resultDt.Columns.Add("NVarCharValue", typeof(string));

            foreach (var item in input)
            {
                resultDt.Rows.Add((byte)item.SocialTypeId, item.SocialDetails);
            }

            return resultDt;
        }
    }
}
