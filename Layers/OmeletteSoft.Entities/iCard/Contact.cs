﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.iCard
{
    public class Contact
    {
        public Guid? UniqueId { get; set; }
        public string Prefix { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Education { get; set; }
        public string Occupation { get; set; }
        public string ImageName { get; set; }
        public bool IsPrivate { get; set; }
        public bool Encrypt { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string CardTemplateName { get; set; }
        public string CardTemplatePath { get; set; }
		public Guid? ContactCardTemplateUniqueId { get; set; }
        public Guid? CardUniqueId { get; set; }
        public string CompanyName { get; set; }

        public string Email { get; set; }
    }
    public class CardDetails
    {
        public CardDetails()
        {
            this.ContactDetails = new Card();
            this.Communications = new List<Communication>();
            this.Socials = new List<ContactSocial>();
        }
        public Card ContactDetails { get; set; }
        public List<Communication> Communications { get; set; }
        public List<ContactSocial> Socials { get; set; }
    }

    public class Card
    {
        public Guid? ContactCardTemplateUniqueId { get; set; }
        public long ContactId { get; set; }
        public int CardTemplateId { get; set; }
        public string CardTemplateName { get; set; }
        public string CardTemplatePath { get; set; }
        public string Prefix { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Education { get; set; }
        public string ImageName { get; set; }
        public bool IsPrivate { get; set; }
        public bool Encrypt { get; set; }
        public string CompanyName { get; set; }
    }
    public class ContactDetails
    {
        public ContactDetails()
        {
            this.Contact = new Contact();
            this.Communications = new List<Communication>();
            this.Socials = new List<ContactSocial>();
            this.Links = new List<Contact>();
            this.Templates = new List<CardTemplate>();
        }
        public Contact Contact { get; set; }
        public List<Communication> Communications { get; set; }
        public List<ContactSocial> Socials { get; set; }
        public List<Contact> Links { get; set; }
        public List<CardTemplate> Templates { get; set; }
    }

    public class UserAppPurchase
    {
        public string TransactionDetails { get; set; }
        public decimal PurchaseAmount { get; set; }
    }
}
