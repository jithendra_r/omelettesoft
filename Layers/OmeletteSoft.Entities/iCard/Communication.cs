﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Entities.iCard
{
    public class Communication
    {
        public Int16? CommunicationTypeId { get; set; }
        public string CommunicationTypeName { get; set; }
        public string CommunicationDetails { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public static DataTable GetDataTable(List<Communication> input)
        {
            var resultDt = new DataTable();
            resultDt.Columns.Add("SmallIntValue", typeof(byte));
            resultDt.Columns.Add("NVarCharValue", typeof(string));

            foreach (var item in input)
            {
                resultDt.Rows.Add((byte)item.CommunicationTypeId, item.CommunicationDetails);
            }

            return resultDt;
        }
    }

    
}
