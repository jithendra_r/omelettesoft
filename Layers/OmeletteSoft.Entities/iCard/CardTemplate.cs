﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.iCard
{
    public class CardTemplate
    {
        public long ContactCardTemplateId { get; set; }
        public Guid? ContactCardTemplateUniqueId { get; set; }
        public Guid? CardUniqueId { get; set; }
        public int CardTemplateId { get; set; }
        public string CardTemplateName { get; set; }
        public string CardTemplatePath { get; set; }
        public short Cost { get; set; }
    }
}
