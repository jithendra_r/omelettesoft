﻿using OmeletteSoftTesting.iCard.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OmeletteSoftTesting.iCard.Models
{

    public class CommonType
    {
        public Dictionary<string, string> Types { get; set; }
        public Error Error { get; set; }
    }

    public class Login
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }

    public class ValidateUserInput
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }

    public class ChangePasswordInput
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
    }

    public class ContactRequest
    {
        public string Prefix { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Education { get; set; }
        public string Occupation { get; set; }
        public string IsPrivate { get; set; }
        public string Encrypt { get; set; }
        public string CompanyName { get; set; }
    }


    public class ContactDetailsRequest
    {
        public ContactRequest Contact { get; set; }
        public List<Communication> Communications { get; set; }
        public List<Social> Socials { get; set; }
    }

    public class RegisterInput
    {
        public Login Login { get; set; }
        public ContactDetailsRequest ContactDetails { get; set; }
    }

    public class VerificationCodeConfirmInput { 
        public string UserId { get; set; }
        public string Code { get; set; }
    }
}
