﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OmeletteSoftTesting.iCard.Models.Response
{
    public class Contact
    {
        public string UniqueId { get; set; }
        public string Prefix { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Education { get; set; }
        public string Occupation { get; set; }
        public string ImageName { get; set; }
        public bool IsPrivate { get; set; }
        public bool Encrypt { get; set; }
        public string CompanyName { get; set; }
        //public DateTime CreatedDate { get; set; }
        //public DateTime ModifiedDate { get; set; }
        public string CardTemplateName { get; set; }
        public string CardTemplatePath { get; set; }
        public string ContactCardTemplateUniqueId { get; set; }
    }

    public class Communication
    {
        public int CommunicationTypeId { get; set; }
        public string CommunicationTypeName { get; set; }
        public string CommunicationDetails { get; set; }
        //public DateTime CreatedDate { get; set; }
        //public DateTime ModifiedDate { get; set; }
    }

    public class Social
    {
        public int SocialTypeId { get; set; }
        public string SocialTypeName { get; set; }
        public string SocialDetails { get; set; }
        //public DateTime CreatedDate { get; set; }
        //public DateTime ModifiedDate { get; set; }
    }

    public class Template
    {
        public int ContactCardTemplateId { get; set; }
        public string ContactCardTemplateUniqueId { get; set; }
        public int CardTemplateId { get; set; }
        public string CardTemplateName { get; set; }
        public string CardTemplatePath { get; set; }
        public int Cost { get; set; }
    }

    public class ContactDetails
    {
        public Contact Contact { get; set; }
        public List<Communication> Communications { get; set; }
        public List<Social> Socials { get; set; }
        public List<CardTemplate> Links { get; set; }
        public List<Template> Templates { get; set; }
    }
    public class CardTemplate
    {
        public long ContactCardTemplateId { get; set; }
        public Guid ContactCardTemplateUniqueId { get; set; }
        public int CardTemplateId { get; set; }
        public string CardTemplateName { get; set; }
        public string CardTemplatePath { get; set; }
        public short Cost { get; set; }
    }

    public class Error
    {
        public int ErrorCode { get; set; }
        public string ErrorDetails { get; set; }
    }

    public class RegisterOutput
    {
        public ContactDetails ContactDetails { get; set; }
        public Error Error { get; set; }
    }

    public class CardDetails
    {
        public CardDetails()
        {
            this.ContactDetails = new Card();
            this.Communications = new List<Communication>();
            this.Socials = new List<Social>();
        }
        public Card ContactDetails { get; set; }
        public List<Communication> Communications { get; set; }
        public List<Social> Socials { get; set; }
    }


    public class Card
    {
        public Guid ContactCardTemplateUniqueId { get; set; }
        public long ContactId { get; set; }
        public int CardTemplateId { get; set; }
        public string CardTemplateName { get; set; }
        public string CardTemplatePath { get; set; }
        public string Prefix { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Education { get; set; }
        public string ImageName { get; set; }
        public bool IsPrivate { get; set; }
        public bool Encrypt { get; set; }
        public string CompanyName { get; set; }
    }

    public class CardDetailsOutput
    {
        public CardDetailsOutput()
        {
            this.ContactDetails = new CardDetails();
            this.Error = new Error();
        }
        public CardDetails ContactDetails { get; set; }
        public Error Error { get; set; }
    }
}
