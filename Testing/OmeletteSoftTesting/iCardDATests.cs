﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace OmeletteSoftTesting
{
    [TestClass]
    public class iCardDATests
    {
        [TestMethod]
        public void RegresionTest()
        {
            var contact1 = Guid.NewGuid();
            var contact2 = Guid.NewGuid();
            ValidateContactSave(contact1);
            ValidateContactSave(contact2);

            var iCardDA = new DALayer.iCardDA();
            var contact2Details = iCardDA.GetContactDetails(contact2);
            iCardDA.SaveContactLink(contact1, contact2Details.Templates.First().ContactCardTemplateUniqueId.Value);
            var contact = iCardDA.GetContactDetails(contact1);
            Assert.AreEqual(contact.Links.Count, 1, "Failed to read contact links");
            Assert.IsTrue(contact.Links.Exists(e => e.FirstName.Equals($"First Name {contact2}")), "Failed to read contact link first name.");
            var cardDetails = iCardDA.GetCardDetails(contact.Templates.First().ContactCardTemplateUniqueId.Value);
            Assert.IsNotNull(cardDetails.ContactDetails.ContactCardTemplateUniqueId);

            iCardDA.DeleteContact(contact2);
            contact = iCardDA.GetContactDetails(contact2);
            Assert.IsNull(contact.Contact, "Failed to delete Contact's profile");

            iCardDA.DeleteContact(contact1);
            contact = iCardDA.GetContactDetails(contact1);
            Assert.IsNull(contact.Contact, "Failed to delete Contact's profile");

            

        }

        private void ValidateContactSave(Guid uniqueId)
        {
            var iCardDA = new DALayer.iCardDA();
            string fax = "123-123-1234", mobile = "123-123-1111", personalEmail = "testemail@testdomain.com";
            string facebook = $"https://www.facebook.com/profile/{uniqueId}", twitter = $"https://www.twitter.com/profile/{uniqueId}",
                linkedIn = $"https://www.linkedin.com/profile/{uniqueId}", instagram = $"https://www.instagram.com/profile/{uniqueId}";

            var companyName = $"CompanyName-{uniqueId}";
            iCardDA.SaveContact(uniqueId, "Mr", $"First Name {uniqueId}", "MN", $"Last Name {uniqueId}", "B.Tech", "SE", string.Empty, false, false, companyName);
            iCardDA.SaveContactCommunications(uniqueId, new List<Entities.iCard.Communication>() {
                new Entities.iCard.Communication{CommunicationTypeId = 1, CommunicationDetails=fax },
                new Entities.iCard.Communication{CommunicationTypeId = 2, CommunicationDetails=mobile },
                new Entities.iCard.Communication{CommunicationTypeId = 3, CommunicationDetails=personalEmail }
            });
            iCardDA.SaveContactSocials(uniqueId, new List<Entities.iCard.ContactSocial>() {
                new Entities.iCard.ContactSocial(){SocialTypeId = 1, SocialDetails =facebook},
                new Entities.iCard.ContactSocial(){SocialTypeId = 2, SocialDetails =twitter},
                new Entities.iCard.ContactSocial(){SocialTypeId = 3, SocialDetails =linkedIn},
                new Entities.iCard.ContactSocial(){SocialTypeId = 4, SocialDetails =instagram}
            });

            var contact = iCardDA.GetContactDetails(uniqueId);
            Assert.IsNotNull(contact.Contact, "Failed to read Contact's profile");
            Assert.AreEqual(contact.Communications.Count, 3, "Failed to read Contact's Communication details");
            Assert.AreEqual(contact.Socials.Count, 4, "Failed to read Contact's Social details");
            Assert.AreEqual(contact.Contact.CompanyName, companyName, "Failed to read Contact's Company Name");

            Assert.IsTrue(contact.Communications.Exists(e => e.CommunicationTypeId == 1 && e.CommunicationDetails.Equals(fax)), "Failed to read fax");
            Assert.IsTrue(contact.Communications.Exists(e => e.CommunicationTypeId == 2 && e.CommunicationDetails.Equals(mobile)), "Failed to read mobile");
            Assert.IsTrue(contact.Communications.Exists(e => e.CommunicationTypeId == 3 && e.CommunicationDetails.Equals(personalEmail)), "Failed to read personal Email");

            Assert.IsTrue(contact.Socials.Exists(e => e.SocialTypeId == 1 && e.SocialDetails.Equals(facebook)), "Failed to read Facebook record");
            Assert.IsTrue(contact.Socials.Exists(e => e.SocialTypeId == 2 && e.SocialDetails.Equals(twitter)), "Failed to read Twitter record");
            Assert.IsTrue(contact.Socials.Exists(e => e.SocialTypeId == 3 && e.SocialDetails.Equals(linkedIn)), "Failed to read LinkedIn record");
            Assert.IsTrue(contact.Socials.Exists(e => e.SocialTypeId == 4 && e.SocialDetails.Equals(instagram)), "Failed to read Instagram record");
        }
    }
}
