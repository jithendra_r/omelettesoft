﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OmeletteSoftTesting.iCard.Models;
using OmeletteSoftTesting.iCard.Models.Response;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OmeletteSoftTesting
{
    [TestClass]
    public class iCardApiTest
    {
        private const string BASE_URL = "https://card.omeletteinc.com/";//"http://localhost:44353/";
        [TestMethod]
        public void RegressionTesting()
        {
            var client = new RestClient(BASE_URL);
            //Add Contact 1
            var contact1Details = ValidateContactRoutes();
            var contact1 = contact1Details.ContactDetails.Contact.UniqueId;
            //Add Contact 2
            var contact2Details = ValidateContactRoutes();
            var contact2 = contact2Details.ContactDetails.Contact.UniqueId;
            var contact2TemplateId = contact2Details.ContactDetails.Templates.Last();


            //Contact 1 Links must be empty
            contact1Details = ValidateLinkSave(contact1Details, contact2TemplateId);

            #region Delete Contact 2
            var request = new RestRequest(RouteDetails.DeleteContact.URL, Method.DELETE);
            AddRequestHeadres(request, contact2);
            var response = client.Execute(request);
            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
            var error = SimpleJson.DeserializeObject<Error>(response.Content);
            Assert.IsTrue(error.ErrorCode == 0, $"Failed to Delete Contact. {error.ErrorDetails}.");
            LogMessage("Delete Contact", request, response);
            #endregion

            contact1Details = ValidateGetContactDetails(contact1);
            Assert.IsFalse(contact1Details.ContactDetails.Links.Any(), "Failed to unlink Contact 2 after deletion");

            #region Delete Contact 2
            request = new RestRequest(RouteDetails.DeleteContact.URL, Method.DELETE);
            AddRequestHeadres(request, contact1);
            response = client.Execute(request);
            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
            error = SimpleJson.DeserializeObject<Error>(response.Content);
            Assert.IsTrue(error.ErrorCode == 0, $"Failed to Delete Contact. {error.ErrorDetails}.");
            LogMessage("Delete Contact", request, response);
            #endregion

        }

        private RegisterOutput ValidateGetContactDetails(string contact1)
        {
            var client = new RestClient(BASE_URL);

            var request = new RestRequest(string.Format(RouteDetails.CommonRoutes.GetContactDetails, contact1), Method.GET);
            AddRequestHeadres(request, contact1);
            var response = client.Execute(request);
            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
            var contact1Details = SimpleJson.DeserializeObject<RegisterOutput>(response.Content);
            Assert.IsTrue(contact1Details.Error.ErrorCode == 0, "Failed to get Contact Details. Error occurred.");
            Assert.IsFalse(string.IsNullOrEmpty(contact1Details.ContactDetails.Contact.UniqueId), "Failed to get Contact Details. Contact object is empty");
            Assert.IsTrue(contact1Details.ContactDetails.Contact.CompanyName.StartsWith("CompanyName-"), "Failed to read Company Name");
            LogMessage("Get Contact / User Details", request, response);
            return contact1Details;
        }
        private RegisterOutput ValidateContactRoutes()
        {
            var client = new RestClient(BASE_URL);

            #region  Get Social Types
            var request = new RestRequest(RouteDetails.CommonRoutes.SocialTypes, Method.GET);
            var response = client.Execute(request);
            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
            var socialTypes = SimpleJson.DeserializeObject<CommonType>(response.Content);
            Assert.IsTrue(socialTypes.Error.ErrorCode == 0, "Failed to read Social Types. Error occurred.");
            Assert.IsTrue(socialTypes.Types.Count > 0, "Failed to read Social Types. Recieved zero types.");
            LogMessage("Get Social Types", request, response);
            #endregion

            #region  Get Communication Types
            request = new RestRequest(RouteDetails.CommonRoutes.CommunicationTypes, Method.GET);
            response = client.Execute(request);
            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
            var communicationTypes = SimpleJson.DeserializeObject<CommonType>(response.Content);
            Assert.IsTrue(communicationTypes.Error.ErrorCode == 0, "Failed to read Communication Types. Error occurred.");
            Assert.IsTrue(communicationTypes.Types.Count > 0, "Failed to read Communication Types. Recieved zero types.");
            LogMessage("Get Communication Types", request, response);
            #endregion

            #region Register Contact 1
            var registerInput = RouteDetails.Register.GetRequestData(socialTypes, communicationTypes);
            request = new RestRequest(RouteDetails.Register.URL, Method.POST);
            request.AddJsonBody(registerInput);
            response = client.Execute(request);
            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
            var registerOutput = SimpleJson.DeserializeObject<RegisterOutput>(response.Content);
            Assert.IsTrue(registerOutput.Error.ErrorCode == 0, "Failed to Register. Error occurred.");
            Assert.IsTrue(registerOutput.ContactDetails != null, "Failed to Register. Recieved null response.");
            LogMessage("Register", request, response);
            #endregion

            //Get Code from DB
            var iCardDA = new DALayer.iCardDA();
            var verficationCode = iCardDA.GetActivationCode(registerOutput.ContactDetails.Contact.UniqueId.ToString());

            var contact1 = registerOutput.ContactDetails.Contact.UniqueId.ToString();

            #region Verify Activation Code - Contact 1
            request = new RestRequest(RouteDetails.VerifyActivationCode.URL, Method.POST);
            request.AddJsonBody(RouteDetails.VerifyActivationCode.GetRequestData(contact1, verficationCode));
            response = client.Execute(request);
            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
            var verifiationCodeOutput = SimpleJson.DeserializeObject<Error>(response.Content);
            Assert.IsTrue(verifiationCodeOutput.ErrorCode == 0, "Failed to Verify Activation Code. Error occurred.");
            LogMessage("Verify Activation Code", request, response);
            #endregion

            #region Get Contact Details - Contact 1
            var contact1Details = ValidateGetContactDetails(contact1);
            #endregion

            #region Change Password - Contact 1
            request = new RestRequest(RouteDetails.ChangePassword.URL, Method.POST);
            AddRequestHeadres(request, contact1);
            request.AddJsonBody(RouteDetails.ChangePassword.GetChangePasswordInput());
            response = client.Execute(request);
            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
            var changePasswordOutput = SimpleJson.DeserializeObject<Error>(response.Content);
            Assert.IsTrue(changePasswordOutput.ErrorCode == 0, $"Failed to Update Password. {changePasswordOutput.ErrorDetails}.");
            LogMessage("Change Password", request, response);
            #endregion

            #region Validate User - Contact 1
            request = new RestRequest(RouteDetails.ValidateUser.URL, Method.POST);
            request.AddJsonBody(RouteDetails.ValidateUser.GetRequestInput(registerInput.Login.Email));
            response = client.Execute(request);
            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
            contact1Details = SimpleJson.DeserializeObject<RegisterOutput>(response.Content);
            Assert.IsTrue(contact1Details.Error.ErrorCode == 0, $"Failed to Validate User. {contact1Details.Error.ErrorDetails}.");
            LogMessage("Validate User", request, response);
            #endregion

            #region Upload User Profile Picture
            Assert.IsTrue(string.IsNullOrEmpty( contact1Details.ContactDetails.Contact.ImageName), "New Contact is having wrong ImageName");
            
            request = new RestRequest(RouteDetails.UploadProfileImage.URL, Method.POST);
            //client.AddHandler("application/octet-stream", new RestSharp.Deserializers.JsonDeserializer());
            AddRequestHeadres(request, contact1, "multipart/form-data");
            request.AlwaysMultipartFormData = true;
            
            var dirName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location.Replace("bin\\Debug", string.Empty));
            var filePath = $"{dirName}\\iCard\\Sample01.jpg";
            request.AddFile("files[SampleImage01.jpg]", filePath);

            response = client.Execute(request);
            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
            var uploadResponse = SimpleJson.DeserializeObject<Error>(response.Content);
            Assert.IsTrue(uploadResponse.ErrorCode == 0, $"Failed to Upload file.");

            //Execute same upload again and make sure file is saved
            response = client.Execute(request);
            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
            uploadResponse = SimpleJson.DeserializeObject<Error>(response.Content);
            Assert.IsTrue(uploadResponse.ErrorCode == 0, $"Failed to Upload file.");
            LogMessage("Upload Contact Profile Image", request, response);
            #endregion

            #region Update Contact Details - Contact 1
            request = new RestRequest(RouteDetails.UpdateContact.URL, Method.POST);
            AddRequestHeadres(request, contact1);
            request.AddJsonBody(RouteDetails.UpdateContact.GetRequestInput(contact1Details.ContactDetails.Contact));
            response = client.Execute(request);
            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
            contact1Details = SimpleJson.DeserializeObject<RegisterOutput>(response.Content);
            Assert.IsTrue(contact1Details.Error.ErrorCode == 0, $"Failed to Update Contact Details. {contact1Details.Error.ErrorDetails}.");
            Assert.IsTrue(contact1Details.ContactDetails.Contact.LastName.Contains("Updated"), $"Failed to Update Contact LastName. {contact1Details.ContactDetails.Contact.LastName}.");
            //Check image is persisted
            Assert.IsTrue(contact1Details.ContactDetails.Contact.ImageName.Contains(contact1), "User profile image is not stored or retrieved.");
            LogMessage("Update Contact Details", request, response);
            #endregion

            #region Update Contact Communications - Contact 1
            request = new RestRequest(RouteDetails.UpdateContactCommunications.URL, Method.POST);
            AddRequestHeadres(request, contact1);
            request.AddJsonBody(RouteDetails.UpdateContactCommunications.GetRequestInput(contact1Details.ContactDetails.Communications));
            response = client.Execute(request);
            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
            contact1Details = SimpleJson.DeserializeObject<RegisterOutput>(response.Content);
            Assert.IsTrue(contact1Details.Error.ErrorCode == 0, $"Failed to Update Contact Communications. {contact1Details.Error.ErrorDetails}.");
            Assert.IsTrue(contact1Details.ContactDetails.Communications.Exists(e => e.CommunicationDetails.Contains("Updated")), $"Failed to Update Contact Communications. {contact1Details.ContactDetails.Communications.First().CommunicationDetails}.");
            LogMessage("Update Contact Communications", request, response);
            #endregion

            #region Update Contact Socials - Contact 1
            request = new RestRequest(RouteDetails.UpdateContactSocials.URL, Method.POST);
            AddRequestHeadres(request, contact1);
            request.AddJsonBody(RouteDetails.UpdateContactSocials.GetRequestInput(contact1Details.ContactDetails.Socials));
            response = client.Execute(request);
            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
            contact1Details = SimpleJson.DeserializeObject<RegisterOutput>(response.Content);
            Assert.IsTrue(contact1Details.Error.ErrorCode == 0, $"Failed to Update Contact Socials. {contact1Details.Error.ErrorDetails}.");
            Assert.IsTrue(contact1Details.ContactDetails.Socials.Exists(e => e.SocialDetails.Contains("Updated")), $"Failed to Update Contact Socials. {contact1Details.ContactDetails.Socials.First().SocialDetails}.");
            LogMessage("Update Contact Socials", request, response);
            #endregion

            #region Delete Contact Communication - Contact 1
            var communicationDeleteId = contact1Details.ContactDetails.Communications.First().CommunicationTypeId;

            request = new RestRequest(string.Format(RouteDetails.DeleteContactCommunication.URL, communicationDeleteId), Method.DELETE);
            AddRequestHeadres(request, contact1);
            response = client.Execute(request);
            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
            contact1Details = SimpleJson.DeserializeObject<RegisterOutput>(response.Content);
            Assert.IsTrue(contact1Details.Error.ErrorCode == 0, $"Failed to Delete Contact Communication. {contact1Details.Error.ErrorDetails}.");
            Assert.IsFalse(contact1Details.ContactDetails.Communications.Exists(e => e.CommunicationTypeId == communicationDeleteId), $"Failed to Delete Contact Communication. Id={communicationDeleteId}.");
            LogMessage("Delete Contact Communication", request, response);
            #endregion

            #region Delete Contact Social - Contact 1
            var socialDeleteId = contact1Details.ContactDetails.Socials.First().SocialTypeId;

            request = new RestRequest(string.Format(RouteDetails.DeleteContactSocial.URL, socialDeleteId), Method.DELETE);
            AddRequestHeadres(request, contact1);
            response = client.Execute(request);
            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
            contact1Details = SimpleJson.DeserializeObject<RegisterOutput>(response.Content);
            Assert.IsTrue(contact1Details.Error.ErrorCode == 0, $"Failed to Delete Contact Socials. {contact1Details.Error.ErrorDetails}.");
            Assert.IsFalse(contact1Details.ContactDetails.Socials.Exists(e => e.SocialTypeId == socialDeleteId), $"Failed to Delete Contact Socials. Id={socialDeleteId}.");
            LogMessage("Delete Contact Social", request, response);
            #endregion

            #region Get Card Details - Contact 1
            request = new RestRequest(string.Format(RouteDetails.CommonRoutes.GetCardDetails, contact1Details.ContactDetails.Templates.First().ContactCardTemplateUniqueId), Method.GET);
            response = client.Execute(request);
            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
            var cardDetails = SimpleJson.DeserializeObject<CardDetailsOutput>(response.Content);
            Assert.IsTrue(cardDetails.Error.ErrorCode == 0, "Failed to get Card Details. Error occurred.");
            Assert.IsFalse(string.IsNullOrEmpty(cardDetails.ContactDetails.ContactDetails.CardTemplatePath), "Failed to get Card Details. CardTemplatePath is empty");
            LogMessage("Get Card Details", request, response);
            #endregion

            return contact1Details;
        }

        private RegisterOutput ValidateLinkSave(RegisterOutput contact1Details, Template contact2TemplateId)
        {
            var client = new RestClient(BASE_URL);
            var contact1 = contact1Details.ContactDetails.Contact.UniqueId;
            #region Link Contact 1 and Contact 2
            var request = new RestRequest(string.Format(RouteDetails.SaveContactLink.URL, contact2TemplateId.ContactCardTemplateUniqueId), Method.POST);
            AddRequestHeadres(request, contact1);
            request.AddJsonBody(RouteDetails.UpdateContactSocials.GetRequestInput(contact1Details.ContactDetails.Socials));
            var response = client.Execute(request);
            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
            contact1Details = SimpleJson.DeserializeObject<RegisterOutput>(response.Content);
            Assert.IsTrue(contact1Details.Error.ErrorCode == 0, $"Failed to link Contact2 with Contact 1. {contact1Details.Error.ErrorDetails}.");
            Assert.IsTrue(contact1Details.ContactDetails.Links.Exists(e => e.ContactCardTemplateUniqueId == new Guid(contact2TemplateId.ContactCardTemplateUniqueId)), $"Failed to link Contact2 with Contact1. {contact2TemplateId.ContactCardTemplateUniqueId}.");
            LogMessage("Save Contact Link", request, response);
            #endregion

            #region Delete Link 2
            request = new RestRequest(string.Format(RouteDetails.DeleteContactLink.URL, contact2TemplateId.ContactCardTemplateUniqueId), Method.DELETE);
            AddRequestHeadres(request, contact1);
            response = client.Execute(request);
            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
            contact1Details = SimpleJson.DeserializeObject<RegisterOutput>(response.Content);
            Assert.IsTrue(contact1Details.Error.ErrorCode == 0, $"Failed to Delete Contact Link. {contact1Details.Error.ErrorDetails}.");
            Assert.IsFalse(contact1Details.ContactDetails.Links.Any(), $"Failed to Delete Contact Link. Id={contact2TemplateId.ContactCardTemplateUniqueId}.");
            LogMessage("Delete Contact Link", request, response);
            #endregion

            return contact1Details;
        }
        private void AddRequestHeadres(RestRequest request, string contact, string contentType= "application/json")
        {
            request.AddHeader("Authorization", contact);
            request.AddHeader("Content-Type", contentType);
            request.AddHeader("AppKey", "86E441C0-E608-4037-B446-14260A031FB1");
        }
        
        public class RouteDetails
        {
            public class CommonRoutes
            {
                public const string SocialTypes = "api/CardUser/SocialTypes";
                public const string CommunicationTypes = "api/CardUser/CommunicationTypes";
                public const string GetContactDetails = "api/CardUser/UserDetails";
                public const string GetCardDetails = "api/CardUser/CardDetails?cardId={0}";
            }
            public class Register
            {
                public const string URL = "api/account/register";
                public static RegisterInput GetRequestData(CommonType socialTypes, CommonType communicationTypes)
                {
                    var uniqueName = Guid.NewGuid().ToString().Replace("-", string.Empty);
                    return new RegisterInput()
                    {

                        Login = new Login()
                        {
                            Email = $"{uniqueName}@testmail.com",
                            Password = "Test@1234",
                            ConfirmPassword = "Test@1234"
                        },
                        ContactDetails = new ContactDetailsRequest()
                        {
                            Contact = new ContactRequest()
                            {
                                Education = "B.Tech",
                                Encrypt = "false",
                                FirstName = $"FN-{uniqueName}",
                                IsPrivate = "false",
                                LastName = $"LN-{uniqueName}",
                                MiddleName = "MN",
                                Occupation = $"OCCP {uniqueName}-1, OCCP {uniqueName}-2",
                                Prefix = "Mr.",
                                CompanyName =$"CompanyName-{uniqueName}"
                            },
                            Communications = communicationTypes.Types.Select((value, index) => new Communication() { CommunicationTypeId = Convert.ToInt32(value.Key), CommunicationDetails = $"Communication {uniqueName} {index}" }).ToList(),
                            Socials = socialTypes.Types.Select((value, index) => new Social() { SocialTypeId = Convert.ToInt32(value.Key), SocialDetails = $"Social {uniqueName} {index}" }).ToList()
                        }
                    };
                }
            }

            public class VerifyActivationCode
            {
                public const string URL = "api/account/VerifyCode";
                public static VerificationCodeConfirmInput GetRequestData(string userId, string code)
                {
                    return new VerificationCodeConfirmInput()
                    {
                        UserId = userId,
                        Code = code
                    };
                }
            }

            public class ChangePassword
            {
                public const string URL = "api/CardUser/ChangePassword";
                public static ChangePasswordInput GetChangePasswordInput()
                {
                    return new ChangePasswordInput()
                    {
                        OldPassword = "Test@1234",
                        NewPassword = "Test@12345",
                        ConfirmPassword = "Test@12345"
                    };
                }
            }

            public class ValidateUser
            {
                public const string URL = "api/account/Validate";
                public static ValidateUserInput GetRequestInput(string username)
                {
                    return new ValidateUserInput()
                    {
                        Email = username,
                        Password = "Test@12345"
                    };
                }
            }
            public class UploadProfileImage
            {
                public const string URL = "api/CardUser/UploadProfileImage";
                public static Contact GetRequestInput(Contact contact)
                {
                    contact.LastName += " Updated";
                    return contact;
                }
            }
            public class UpdateContact
            {
                public const string URL = "api/CardUser/SaveContact";
                public static Contact GetRequestInput(Contact contact)
                {
                    contact.LastName += " Updated";
                    return contact;
                }
            }
            public class UpdateContactCommunications
            {
                public const string URL = "api/CardUser/SaveContactCommunications";
                public static List<Communication> GetRequestInput(List<Communication> communications)
                {
                    communications.First().CommunicationDetails = " Updated";
                    return communications;
                }
            }

            public class UpdateContactSocials
            {
                public const string URL = "api/CardUser/SaveContactSocials";
                public static List<Social> GetRequestInput(List<Social> socials)
                {
                    socials.First().SocialDetails = " Updated";
                    return socials;
                }
            }

            public class DeleteContactCommunication
            {
                public const string URL = "api/CardUser/Communication?type={0}";
            }
            public class DeleteContactSocial
            {
                public const string URL = "api/CardUser/Social?type={0}";
            }
            public class SaveContactLink
            {
                public const string URL = "api/CardUser/SaveContactLink?linkId={0}";
            }
            public class DeleteContactLink
            {
                public const string URL = "api/CardUser/Link?linkId={0}";
            }
            public class DeleteContact
            {
                public const string URL = "api/CardUser/Contact";
            }
        }

        ;

        public void LogMessage(string methodName, RestRequest request, IRestResponse response)
        {
            var sb = new StringBuilder();

            sb.AppendLine($"Route Name: {methodName}");
            sb.AppendLine($"URL: {BASE_URL}{request.Resource}");
            sb.AppendLine($"Method: {request.Method}");
            sb.AppendLine($"Headers: {string.Join(";", request.Parameters.Select(e=> e.Name + "=" + e.Value))}");
            if(request.Body != null)
                sb.AppendLine($"Request Body: {SimpleJson.SerializeObject( request.Body.Value)}");
            sb.Append(Environment.NewLine);
            sb.AppendLine($"Response Output: {response.Content}");

            new LogWriter(sb.ToString());
        }

        public class LogWriter
        {
            private string m_exePath = string.Empty;
            public LogWriter(string logMessage)
            {
                LogWrite(logMessage);
            }
            public void LogWrite(string logMessage)
            {
                m_exePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                try
                {
                    using (StreamWriter w = File.AppendText(m_exePath + "\\" + "log.txt"))
                    {
                        Log(logMessage, w);
                    }
                }
                catch (Exception)
                {
                }
            }

            public void Log(string logMessage, TextWriter txtWriter)
            {
                try
                {
                    txtWriter.WriteLine($"{Environment.NewLine}-------------------------------");
                    txtWriter.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
                        DateTime.Now.ToLongDateString());
                    txtWriter.WriteLine("{0}", logMessage);
                    txtWriter.WriteLine("-------------------------------");
                }
                catch (Exception )
                {
                }
            }
        }
    }
}