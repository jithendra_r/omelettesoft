﻿using BLLayer;
using JournalIF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace JournalIF.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SearchResults()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        [Route("Search")]
        public JsonResult Search(SearchInput input)
        {
            var results = new List<JIFHeaderResulItem>();

            try
            {
                string sortColumn = "Title", sortOrder = "ASC";
                if (!string.IsNullOrEmpty(input.SortField))
                {
                    if (input.SortField.Equals("Title", StringComparison.InvariantCultureIgnoreCase))
                    {
                        sortColumn = "Title";
                        sortOrder = "ASC";
                    }
                    if (input.SortField.Equals("ImpactFactor", StringComparison.InvariantCultureIgnoreCase))
                    {
                        sortColumn = "ImpactFactor";
                        sortOrder = "DESC";
                    }

                }
                var rows = input.Rows.HasValue ? input.Rows.Value : 500;
                results = JIFMgr.SearchJournal(input.Query, rows, 0, sortColumn, sortOrder).Select(e => new JIFHeaderResulItem()
                {
                    Id = e.JournalHeaderId,
                    Category = e.JournalCategoryName,
                    Country = e.CountryName,
                    EISSN = e.EISSN,
                    ISSN = e.ISSN,
                    Language = e.LanguageName,
                    Publisher = e.PublisherName,
                    Title = e.Title,
                    Title20 = e.Title20,
                    Title29 = e.Title29,
                    Website = e.WebSite,
                    Frequency = e.Frequency,
                    Year = e.CurrentYear.Value,
                    ImpactFactor = e.ImpactFactor,
                    APC =e.APC,
                    APCAmount = e.APCAmount,
                    Currency = e.Currency,
                    Keywords = e.Keywords,
                    OpenAccess = e.OpenAccess,
                    ReviewProcess =e.ReviewProcess,
                    SubmissionFee = e.SubmissionFee,
                    Counters = new DALayer.JournalHeaderCounters()
                    {
                        MarkedAsFavorites = e.MarkedAsFavorite,
                        Viewed = e.Viewed,
                        Shared = e.Shared
                    }
                }).ToList();

            }
            catch (Exception)
            {
                return Json(new {Error =  "Error occured during Journal Search."});
            }
            return Json(new { results = results});
        }

        [HttpGet]
        [Route("factor")]
        public JsonResult Factor(long? id)
        {
            var results = new List<JIFImpactFactor>();

            try
            {
                if (id.HasValue)
                {
                    results = JIFMgr.GetJournalImpactFactor(id.Value).OrderByDescending(e=> e.JournalYear).Select(e => new JIFImpactFactor()
                    {
                        Year = e.JournalYear.Value,
                        Factor = e.ImpactFactor.Value
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                return Json(new { Error = "Error occured during Journal Search." });
            }
            return Json(new { results = results }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Route("HighImpactJournals")]
        public JsonResult GetHighImpactJournals(HighImpactFactorJournalsInput input)
        {
            var results = new List<JIFHeaderResulItem>();

            try
            {

                results = JIFMgr.GetHighImpactJournals(input.RecordCount, input.Year, input.ImpactFactor).Select(e => new JIFHeaderResulItem()
                {
                    Id = e.JournalHeaderId,
                    Category = e.JournalCategoryName,
                    Country = e.CountryName,
                    EISSN = e.EISSN,
                    ISSN = e.ISSN,
                    Language = e.LanguageName,
                    Publisher = e.PublisherName,
                    Title = e.Title,
                    Title20 = e.Title20,
                    Title29 = e.Title29,
                    Website = e.WebSite,
                    Frequency = e.Frequency,
                    Year = e.JournalYear.Value,
                    ImpactFactor = e.ImpactFactor
                }).ToList();

            }
            catch (Exception)
            {
                return Json(new { Error = "Error occured while retrieving High Impact Journals." });
            }
            return Json(new { results = results });
        }

        [HttpPost]
        [Route("UpdateCounter")]
        public JsonResult UpdateCounter(CounterInput input)
        {
            if (input.Id < 0)
                return Json(new { Error = "Invalid Id." });

            //EnumMgr.JIFCounterName jifCounterName;
            if (!Enum.TryParse(input.Name, true, out OmeletteSoft.BLLayer.EnumMgr.JIFCounterName jifCounterName))
                return Json(new { Error = "Invalid Counter." });


            var result = JIFMgr.SaveJournalHeaderCounters(input.Id, jifCounterName.ToString());
            return Json(new { result = result });


        }
    }
}