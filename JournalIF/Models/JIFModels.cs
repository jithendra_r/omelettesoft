﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JournalIF.Models
{

    public class SearchInput
    {
        public String Query { get; set; }
        public int? Page { get; set; }
        public int? Rows { get; set; }
        public string SortField { get; set; }
    }
    public class JIFHeaderResulItem
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Title20 { get; set; }
        public string Title29 { get; set; }
        public string ISSN { get; set; }
        public string EISSN { get; set; }
        public string Category { get; set; }
        public string Country { get; set; }
        public string Publisher { get; set; }
        public string Language { get; set; }
        public string Website { get; set; }
        public string Frequency { get; set; }
        public int Year { get; set; }
        public decimal ImpactFactor { get; set; }
        public string APC { get; set; }
        public string APCAmount { get; set; }
        public string Currency { get; set; }
        public string SubmissionFee { get; set; }
        public string Keywords { get; set; }
        public string ReviewProcess { get; set; }
        public string OpenAccess { get; set; }

        public DALayer.JournalHeaderCounters Counters { get; set; }

    }

    public class JIFImpactFactor
    {
        public int Year { get; set; }
        public decimal Factor { get; set; }
    }

    public class HighImpactFactorJournalsInput {
        public int RecordCount { get; set; }
        public int? Year { get; set; }
        public decimal? ImpactFactor { get; set; }
    }

    public class CounterInput
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}