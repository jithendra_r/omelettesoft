﻿using Microsoft.AspNet.Identity;
using OmeletteSoft.BLLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ScholarProfileApi.Providers
{
    public class EmailService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            return configSendGridasync(message);
        }

        private Task configSendGridasync(IdentityMessage message)
        {
            var smtp = new System.Net.Mail.SmtpClient();
            var credential = (smtp.Credentials as System.Net.NetworkCredential);
            var username = credential.UserName;
            var password = credential.Password;

            OmeletteSoft.BLLayer.MailHelper.SendMail(message.Destination, message.Subject, message.Body, string.Empty,
                username, password);

            return Task.FromResult(0);
        }
    }
}