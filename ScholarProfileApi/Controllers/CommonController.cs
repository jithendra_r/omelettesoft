﻿using ScholarProfile.BLLayer;
using ScholarProfileApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace ScholarProfileApi.Controllers
{
    [RoutePrefix("api/Common")]
    public class CommonController : ApiController
    {
        [Route("Countries")]
        public List<CommonModels.Country> Countries()
        {
            return CommonMgr.GetCountries().Select(country => new CommonModels.Country() {
                Id = country.CountryID.Value,
                Code = country.TwoCharCountryCode,
                Name = country.CountryName
            }).ToList();
        }
    }
}