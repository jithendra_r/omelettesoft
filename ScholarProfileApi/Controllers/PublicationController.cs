﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ScholarProfile.BLLayer;
using ScholarProfileApi.Models;
using ScholarProfileApi.Models.Publication;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace ScholarProfileApi.Controllers
{
    [Authorize]
    [RoutePrefix("api/Publication")]
    public class PublicationController : ApiController
    {
        ApplicationUser _currentUser;
        private ApplicationUser CurrentUser
        {
            get
            {
                if (_currentUser == null)
                {
                    ApplicationDbContext context = new ApplicationDbContext();
                    var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                    _currentUser = UserManager.FindById(User.Identity.GetUserId());
                }

                return _currentUser;
            }
        }

        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("Search")]
        [HttpGet]
        public PublicationSearchModel Search()
        {
            var result = new PublicationSearchModel();
            try
            {
                var url = string.Format("https://api.elsevier.com/content/search/scidir?query={0} {1}&apiKey=d449e97f6c2a59f4d00f41c2619386b1&sort=-date", CurrentUser.FirstName, CurrentUser.LastName);

                WebRequest webRequest = WebRequest.Create(url);
                webRequest.Method = "GET";
                WebResponse webResp = webRequest.GetResponse();

                using (var reader = new StreamReader(webResp.GetResponseStream(), Encoding.UTF8))
                {
                    dynamic response = Newtonsoft.Json.JsonConvert.DeserializeObject(reader.ReadToEnd());
                    if (response != null && response["search-results"] != null)
                    {
                        dynamic entries = response["search-results"]["entry"];
                        if (entries != null)
                        {
                            if (entries.Count > 0)
                            {
                                //Get values from DB
                                var userDOIs = PublicationMgr.GetUserPublications(CurrentUser.Id);

                                foreach (dynamic entry in entries)
                                {
                                    if (entry["authors"] != null && entry["authors"]["author"] != null)
                                    {
                                        var searchResult = new SearchResult();

                                        var title = entry["dc:title"];
                                        if (title != null)
                                            searchResult.Title = Convert.ToString(title);

                                        var publicationName = entry["prism:publicationName"];
                                        if (publicationName != null)
                                            searchResult.PublicationName = Convert.ToString(publicationName);

                                        var issn = entry["prism:issn"];
                                        if (issn != null)
                                            searchResult.ISSN = Convert.ToString(issn);

                                        var volume = entry["prism:volume"];
                                        if (volume != null)
                                            searchResult.Volume = Convert.ToString(volume);

                                        var issueIdentifier = entry["prism:issueIdentifier"];
                                        if (issueIdentifier != null)
                                            searchResult.IssueIdentifier = Convert.ToString(issueIdentifier);

                                        var startingPage = entry["prism:startingPage"];
                                        if (startingPage != null)
                                            searchResult.StartingPage = Convert.ToString(startingPage);

                                        var endingPage = entry["prism:endingPage"];
                                        if (endingPage != null)
                                            searchResult.EndingPage = Convert.ToString(endingPage);

                                        var doi = entry["prism:doi"];
                                        if (doi != null)
                                        {
                                            searchResult.DOI = Convert.ToString(doi);
                                            searchResult.IsMarked = userDOIs.Exists(e => e.DOI.Equals(searchResult.DOI, StringComparison.InvariantCultureIgnoreCase));
                                        }   

                                        var teaser = entry["prism:teaser"];
                                        if (teaser != null)
                                            searchResult.Teaser = Convert.ToString(teaser);

                                        

                                        //Fill authors
                                        searchResult.Authors = new List<CommonModels.Author>();
                                        foreach (var author in entry["authors"]["author"])
                                        {
                                            var authorItem = new CommonModels.Author();

                                            var givenName = author["given-name"];
                                            if (givenName != null)
                                                authorItem.GivenName = Convert.ToString(givenName);

                                            var surname = author["surname"];
                                            if (surname != null)
                                                authorItem.SurName = Convert.ToString(surname);

                                            if (string.IsNullOrEmpty(authorItem.GivenName) != null || string.IsNullOrEmpty(authorItem.SurName))
                                                searchResult.Authors.Add(authorItem);
                                        }

                                        result.Results.Add(searchResult);
                                    }
                                    else
                                    {
                                        var error = entry["error"];
                                        if (error != null)
                                        {
                                            result.Error.Code = -1;
                                            result.Error.Message = Convert.ToString(error);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                result.Error.Message = "No results found.";
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                result.Error.Code = -1;
                result.Error.Message = string.Format("Failed to Search publication. Details : {0}", ex.Message);
            }
            return result;
        }

        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("ClaimDOI")]
        public IHttpActionResult SaveDOI(SaveDOIModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            PublicationMgr.SaveUserDOI(CurrentUser.Id, model.DOI, model.IsActive);

            return Ok();
        }
    }
}