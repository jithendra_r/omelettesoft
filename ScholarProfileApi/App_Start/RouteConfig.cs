﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ScholarProfileApi
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "ConfirmEmail",
                url: "Home/ConfirmEmail",
                defaults: new { controller = "Home", action = "ConfirmEmail", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "ResetPassword",
                url: "Home/ResetPassword",
                defaults: new { controller = "Home", action = "ResetPassword", id = UrlParameter.Optional }
            );

        }
    }
}
