﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ScholarProfileApi.Models
{
    public class CommonModels
    {
        public class Country
        {
            public int Id { get; set; }
            public string Code { get; set; }
            public string Name { get; set; }
        }

        public class Author
        {
            public string GivenName { get; set; }
            public string SurName { get; set; }
        }

        public class Error
        {
            public Error()
            {
                Code = 0;
                Message = string.Empty;
            }
            public int Code { get; set; }
            public string Message { get; set; }
        }
    }
}