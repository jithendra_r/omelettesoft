﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ScholarProfileApi.Models.Publication
{
    public class PublicationSearchModel
    {
        public PublicationSearchModel()
        {
            Results = new List<SearchResult>();
            Error = new CommonModels.Error();
        }
        public List<SearchResult> Results { get; set; }
        public ScholarProfileApi.Models.CommonModels.Error Error { get; set; }
    }

    public class SearchResult
    {
        //dc:title, prism:publicationName, prism:issn, prism:volume, prism:issueIdentifier, prism:startingPage, prism:endingPage, prism:doi, authors/author
        public string Title { get; set; }
        public string PublicationName { get; set; }
        public string ISSN { get; set; }
        public string Volume { get; set; }
        public string IssueIdentifier { get; set; }
        public string StartingPage { get; set; }
        public string EndingPage { get; set; }
        public string DOI { get; set; }
        public string Teaser { get; set; }
        public bool IsMarked { get; set; }
        public List<ScholarProfileApi.Models.CommonModels.Author> Authors { get; set; }
    }
    public class SaveDOIModel
    {
        [Required]
        [Display(Name = "DOI")]
        public string DOI { get; set; }

        [Required]
        [Display(Name = "Claim or Unclaim")]
        public bool IsActive { get; set; }
    }
    
}