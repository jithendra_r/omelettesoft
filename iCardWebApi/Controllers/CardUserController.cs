﻿using iCardWebApi.Atrributes;
using iCardWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Text;
using Entities.iCard;
using System.IO;
using System.Drawing;
using System.Web.Hosting;
using System.Web;
using System.Web.Routing;
using OmeletteSoft.BLLayer;

namespace iCardWebApi.Controllers
{
    [LicenseAttribute]
    [UserIdAuthorizeAttribute]
    [RoutePrefix("api/CardUser")]
    public class CardUserController : ApiController
    {
        private ApplicationUserManager _userManager;
        public CardUserController()
        {

        }
        public CardUserController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        
        [Route("ChangePassword")]
        [HttpPost]
        public async Task<Error> ChangePassword(ChangePasswordBindingModel model)
        {
            var result = new Error();
            if (!ModelState.IsValid)
            {
                result.ErrorCode = -1;
                var sb = new StringBuilder();
                foreach (var item in ModelState)
                {
                    sb.AppendLine($"{item.Key}: {item.Value}");
                }
                result.ErrorDetails = sb.ToString();
                return result;
            }

            if(Request.Properties.ContainsKey("UserDetails"))
            {
                var userDetails = Request.Properties["UserDetails"] as ApplicationUser;

                IdentityResult identityResult = await UserManager.ChangePasswordAsync(userDetails.Id, model.OldPassword,
                model.NewPassword);

                if (identityResult.Succeeded)
                {
                    result.ErrorCode = 0;
                    result.ErrorDetails = "Password updated successfully.";
                }
                else
                {
                    result.ErrorCode = -1;
                    result.ErrorDetails = string.Join(". ", identityResult.Errors);
                }
                return result;

            }
            else
            {
                result.ErrorCode = -1;
                result.ErrorDetails = "Failed to retrieve User Details.";
                return result;
            }
        }

        [Route("SaveContact")]
        [HttpPost]
        public async Task<CardUserViewModel> SaveContact(Contact contact)
        {
            var result = new CardUserViewModel();
            
            if (Request.Properties.ContainsKey("UserDetails"))
            {
                var userDetails = Request.Properties["UserDetails"] as ApplicationUser;

                try
                {
                    BLLayer.iCardMgr.SaveContact(new Guid(userDetails.Id), contact.Prefix, contact.FirstName, contact.MiddleName, contact.LastName, 
                        contact.Education, contact.Occupation, null, contact.IsPrivate, contact.Encrypt, contact.CompanyName);
                    result.Error.ErrorCode = 0;
                    result.Error.ErrorDetails = "Contact details updated successfully.";
                    result.ContactDetails = BLLayer.iCardMgr.GetContactDetails(new Guid(userDetails.Id));

                }
                catch (Exception)
                {
                    result.Error.ErrorCode = -1;
                    result.Error.ErrorDetails = "Failed to update Contact Details.";
                }

                return result;

            }
            else
            {
                result.Error.ErrorCode = -1;
                result.Error.ErrorDetails = "Failed to retrieve User Details.";
                return result;
            }
        }

        [Route("SaveContactCommunications")]
        [HttpPost]
        public async Task<CardUserViewModel> SaveContactCommunications(List<Communication> communications)
        {
            var result = new CardUserViewModel();

            if (Request.Properties.ContainsKey("UserDetails"))
            {
                var userDetails = Request.Properties["UserDetails"] as ApplicationUser;

                try
                {
                    BLLayer.iCardMgr.SaveContactCommunications(new Guid(userDetails.Id), communications);
                    result.Error.ErrorCode = 0;
                    result.Error.ErrorDetails = "Contact communications updated successfully.";
                    result.ContactDetails = BLLayer.iCardMgr.GetContactDetails(new Guid(userDetails.Id));

                }
                catch (Exception)
                {
                    result.Error.ErrorCode = -1;
                    result.Error.ErrorDetails = "Failed to update Contact communications.";
                }

                return result;

            }
            else
            {
                result.Error.ErrorCode = -1;
                result.Error.ErrorDetails = "Failed to retrieve User Details.";
                return result;
            }
        }

        [Route("SaveContactSocials")]
        [HttpPost]
        public async Task<CardUserViewModel> SaveContactSocials(List<ContactSocial> socials)
        {
            var result = new CardUserViewModel();

            if (Request.Properties.ContainsKey("UserDetails"))
            {
                var userDetails = Request.Properties["UserDetails"] as ApplicationUser;

                try
                {
                    BLLayer.iCardMgr.SaveContactSocials(new Guid(userDetails.Id), socials);
                    result.Error.ErrorCode = 0;
                    result.Error.ErrorDetails = "Contact socials updated successfully.";
                    result.ContactDetails = BLLayer.iCardMgr.GetContactDetails(new Guid(userDetails.Id));

                }
                catch (Exception)
                {
                    result.Error.ErrorCode = -1;
                    result.Error.ErrorDetails = "Failed to update Contact socials.";
                }

                return result;

            }
            else
            {
                result.Error.ErrorCode = -1;
                result.Error.ErrorDetails = "Failed to retrieve User Details.";
                return result;
            }
        }

        [Route("SaveContactLink")]
        [HttpPost]
        public async Task<CardUserViewModel> SaveContactLink(Guid linkId)
        {
            var result = new CardUserViewModel();

            if (Request.Properties.ContainsKey("UserDetails"))
            {
                var userDetails = Request.Properties["UserDetails"] as ApplicationUser;

                try
                {
                    BLLayer.iCardMgr.SaveContactLink(new Guid(userDetails.Id), linkId);
                    result.Error.ErrorCode = 0;
                    result.Error.ErrorDetails = "Contact linked successfully.";
                    result.ContactDetails = BLLayer.iCardMgr.GetContactDetails(new Guid(userDetails.Id));

                }
                catch (Exception)
                {
                    result.Error.ErrorCode = -1;
                    result.Error.ErrorDetails = "Failed to update Contact link.";
                }

                return result;

            }
            else
            {
                result.Error.ErrorCode = -1;
                result.Error.ErrorDetails = "Failed to retrieve User Details.";
                return result;
            }
        }

        [Route("CommunicationTypes")]
        [AllowAnonymous]
        [HttpGet]
        public async Task<TypesViewModel> GetCommunicationTypes()
        {
            var result = new TypesViewModel();

            try
            {
                result.Error.ErrorCode = 0;
                result.Error.ErrorDetails = string.Empty;
                result.Types = BLLayer.iCardMgr.GetCommunicationTypes(true);

            }
            catch (Exception)
            {
                result.Error.ErrorCode = -1;
                result.Error.ErrorDetails = "Failed to get Communication Types.";
            }

            return result;
        }

        [Route("SocialTypes")]
        [AllowAnonymous]
        [HttpGet]
        public async Task<TypesViewModel> GetSocialTypes()
        {
            var result = new TypesViewModel();

            try
            {
                result.Error.ErrorCode = 0;
                result.Error.ErrorDetails = string.Empty;
                result.Types = BLLayer.iCardMgr.GetSocialTypes(true);

            }
            catch (Exception)
            {
                result.Error.ErrorCode = -1;
                result.Error.ErrorDetails = "Failed to get Social Types.";
            }

            return result;
        }

        [Route("Communication")]
        [HttpDelete]
        public async Task<CardUserViewModel> DeleteCommunication(short type)
        {
            var result = new CardUserViewModel();

            if (Request.Properties.ContainsKey("UserDetails"))
            {
                var userDetails = Request.Properties["UserDetails"] as ApplicationUser;

                try
                {
                    BLLayer.iCardMgr.DeleteContactCommunication(new Guid(userDetails.Id), type);
                    result.Error.ErrorCode = 0;
                    result.Error.ErrorDetails = "Contact Communication deleted successfully.";
                    result.ContactDetails = BLLayer.iCardMgr.GetContactDetails(new Guid(userDetails.Id));

                }
                catch (Exception)
                {
                    result.Error.ErrorCode = -1;
                    result.Error.ErrorDetails = "Failed to delete Contact Communication.";
                }

                return result;

            }
            else
            {
                result.Error.ErrorCode = -1;
                result.Error.ErrorDetails = "Failed to retrieve User Details.";
                return result;
            }
        }

        [Route("Social")]
        [HttpDelete]
        public async Task<CardUserViewModel> DeleteSocial(short type)
        {
            var result = new CardUserViewModel();

            if (Request.Properties.ContainsKey("UserDetails"))
            {
                var userDetails = Request.Properties["UserDetails"] as ApplicationUser;

                try
                {
                    BLLayer.iCardMgr.DeleteContactSocial(new Guid(userDetails.Id), type);
                    result.Error.ErrorCode = 0;
                    result.Error.ErrorDetails = "Contact Social deleted successfully.";
                    result.ContactDetails = BLLayer.iCardMgr.GetContactDetails(new Guid(userDetails.Id));

                }
                catch (Exception)
                {
                    result.Error.ErrorCode = -1;
                    result.Error.ErrorDetails = "Failed to delete Contact Social.";
                }

                return result;

            }
            else
            {
                result.Error.ErrorCode = -1;
                result.Error.ErrorDetails = "Failed to retrieve User Details.";
                return result;
            }
        }

        [Route("Link")]
        [HttpDelete]
        public async Task<CardUserViewModel> DeleteLink(Guid linkId)
        {
            var result = new CardUserViewModel();

            if (Request.Properties.ContainsKey("UserDetails"))
            {
                var userDetails = Request.Properties["UserDetails"] as ApplicationUser;

                try
                {
                    BLLayer.iCardMgr.DeleteContatLink(new Guid(userDetails.Id), linkId);
                    result.Error.ErrorCode = 0;
                    result.Error.ErrorDetails = "Contact Link deleted successfully.";
                    result.ContactDetails = BLLayer.iCardMgr.GetContactDetails(new Guid(userDetails.Id));

                }
                catch (Exception)
                {
                    result.Error.ErrorCode = -1;
                    result.Error.ErrorDetails = "Failed to delete Contact Link.";
                }

                return result;

            }
            else
            {
                result.Error.ErrorCode = -1;
                result.Error.ErrorDetails = "Failed to retrieve User Details.";
                return result;
            }
        }

        [Route("Contact")]
        [HttpDelete]
        public async Task<Error> DeleteContact()
        {
            var result = new Error();

            if (Request.Properties.ContainsKey("UserDetails"))
            {
                var userDetails = Request.Properties["UserDetails"] as ApplicationUser;

                try
                {
                    var userId = new Guid(userDetails.Id);
                    
                    string code = await UserManager.GenerateEmailConfirmationTokenAsync(userId.ToString());
                    //var callbackUrl = this.Url.Link("ConfirmEmail", new { Controller = "Home", Action = "ConfirmDelete", userId = userId, code = code });
                    var routeValues = new RouteValueDictionary();
                    routeValues.Add("userId", userId.ToString());
                    routeValues.Add("code", code);

                    var urlHelper = new System.Web.Mvc.UrlHelper(HttpContext.Current.Request.RequestContext, RouteTable.Routes);
                    string callbackUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority) + urlHelper.Action(
                        "ConfirmDelete",
                        "Home",
                        routeValues
                        );

                    //await UserManager.SendEmailAsync(userId.ToString(), "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");
                    var smtp = new System.Net.Mail.SmtpClient();
                    var host = smtp.Host;
                    var ssl = smtp.EnableSsl;
                    var port = smtp.Port;

                    var credential = (smtp.Credentials as System.Net.NetworkCredential);
                    var username = credential.UserName;
                    var password = credential.Password;

                    //MailHelper.SendMail(applicationUser.UserName, "iCard Activation Code", $"<html><body>Activation code for iCard: <h1>{saveActivationCodeResult.ActivationCode}</h1></body></html>", string.Empty);

                    MailHelper.SendMail(userDetails.UserName, "iCard Profile Deletion Email", $"<html><body>Please click link for deleting your Profile.<br><h2><a href='{callbackUrl}'>Click Here</a></h2></body></html>", string.Empty,
                        username, password, ssl, port, host);
                    result.ErrorCode = 0;
                    result.ErrorDetails = "Confirmation Email sent to Registered Email. Please click link in Email for deleting your Profile.";
                }
                catch (Exception)
                {
                    result.ErrorCode = -1;
                    result.ErrorDetails = "Failed to delete Contact.";
                }

                return result;

            }
            else
            {
                result.ErrorCode = -1;
                result.ErrorDetails = "Failed to retrieve User Details.";
                return result;
            }
        }
        

        [Route("CardDetails")]
        [AllowAnonymous]
        [HttpGet]
        public async Task<CardDetailsViewModel> GetCardDetails(Guid cardId)
        {
            var result = new CardDetailsViewModel();

            try
            {
                result.Error.ErrorCode = 0;
                result.Error.ErrorDetails = string.Empty;
                result.ContactDetails = BLLayer.iCardMgr.GetCardDetails(cardId);

            }
            catch (Exception ex)
            {
                result.Error.ErrorCode = -1;
                result.Error.ErrorDetails = $"Failed to get Card details.{ex.Message}. {ex.StackTrace}";
            }

            return result;
        }

        [HttpGet]
        [Route("UserDetails")]
        public async Task<ValidateOutput> GetUserDetails()
        {
            var output = new ValidateOutput();
            if (Request.Properties.ContainsKey("UserDetails"))
            {
                var userDetails = Request.Properties["UserDetails"] as ApplicationUser;
                try
                {
                    output.ContactDetails = BLLayer.iCardMgr.GetContactDetails(new Guid( userDetails.Id));
                }
                catch (Exception)
                {
                    output.Error.ErrorCode = -1;
                    output.Error.ErrorDetails = "Server Error. Please try again.";
                }
            }
            else
            {
                output.Error.ErrorCode = -1;
                output.Error.ErrorDetails = "Failed to retrieve User Details.";
                return output;
            }
            

            return output;
        }

        [HttpPost]
        [Route("UploadProfileImage")]
        public async Task<Error> UploadProfileImage()
        {
            var output = new Error();
            if (Request.Properties.ContainsKey("UserDetails"))
            {
                var userDetails = Request.Properties["UserDetails"] as ApplicationUser;
                var httpRequest = HttpContext.Current.Request;
                var fileName = string.Empty;
                if (httpRequest.Files.Count > 0)
                {
                    var docfiles = new List<string>();
                    foreach (string file in httpRequest.Files)
                    {
                        //Make sure path exists
                        var folderName = HttpContext.Current.Server.MapPath("~/UserProfile/");
                        if (!Directory.Exists(folderName))
                            Directory.CreateDirectory(folderName);

                        var postedFile = httpRequest.Files[file];

                        var extension = Path.GetExtension(postedFile.FileName);
                        if (string.IsNullOrEmpty(extension))
                            extension = ".jpg";
                        fileName = $"{userDetails.Id}{extension}";

                        var filePath = $"{folderName}/{fileName}";
                        //Delete if file exists
                        if (File.Exists(filePath))
                        {
                            File.Delete(filePath);
                        }
                        postedFile.SaveAs(filePath);

                        //Update Contact Record
                        var contactProfileDetails = BLLayer.iCardMgr.GetContactDetails(new Guid(userDetails.Id));
                        var contactDetails = contactProfileDetails.Contact;

                        contactDetails.ImageName = fileName;
                        BLLayer.iCardMgr.SaveContact(contactDetails.UniqueId.Value, contactDetails.Prefix, contactDetails.FirstName, contactDetails.MiddleName, contactDetails.LastName,
                            contactDetails.Education, contactDetails.Occupation, contactDetails.ImageName, contactDetails.IsPrivate, contactDetails.Encrypt, contactDetails.CompanyName);

                    }
                    output.ErrorCode = 0;
                    output.ErrorDetails = $"{fileName} uploaded successfully and attached to profile.";
                }
                else
                {
                    output.ErrorCode = -1;
                    output.ErrorDetails = $"No file to upload.";
                }
                return output;
            }
            else
            {
                output.ErrorCode = -1;
                output.ErrorDetails = "Failed to retrieve User Details.";
                return output;
            }
            


        }


        [HttpPost]
        [Route("SaveUserAppPurchase")]
        public async Task<Error> SaveUserAppPurchase(UserAppPurchase userAppPurchase)
        {
            var output = new Error();
            if (Request.Properties.ContainsKey("UserDetails"))
            {
                var userDetails = Request.Properties["UserDetails"] as ApplicationUser;
                
                if(userAppPurchase != null)
                {
                    var contactId = new Guid(userDetails.Id);
                    BLLayer.iCardMgr.SaveUserAppPurchase(contactId, userAppPurchase.TransactionDetails, userAppPurchase.PurchaseAmount);
                    BLLayer.iCardMgr.ApplyContactCardTemplates(contactId, userAppPurchase.PurchaseAmount);

                    output.ErrorCode = 0;
                    output.ErrorDetails = "Successfully applied Card templates to User.";
                    return output;
                }
                else
                {
                    output.ErrorCode = -1;
                    output.ErrorDetails = "User App purchase details are missing.";
                    return output;
                }

                return output;
            }
            else
            {
                output.ErrorCode = -1;
                output.ErrorDetails = "Failed to retrieve User Details.";
                return output;
            }



        }

    }
}
