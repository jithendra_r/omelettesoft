﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace iCardWebApi.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public HomeController()
        {

        }
        public HomeController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        [AllowAnonymous]
        public System.Web.Mvc.ActionResult ConfirmDelete(string userId, string code)
        {
            if (userId == null || code == null)
            {
                ViewBag.errorMessage = "Invalid Link";
                return View();
            }
            IdentityResult result;
            try
            {
                result = UserManager.ConfirmEmailAsync(userId, code).Result;
            }
            catch (InvalidOperationException ioe)
            {
                // ConfirmEmailAsync throws when the userId is not found.
                ViewBag.errorMessage = ioe.Message;
                return View();
            }

            if (result.Succeeded)
            {
                var userGuid = new Guid(userId);
                
                var contactDetails = BLLayer.iCardMgr.GetContactDetails(userGuid);
                BLLayer.iCardMgr.DeleteContact(new Guid(userId));
                
                if (!string.IsNullOrEmpty(contactDetails.Contact.ImageName))
                {
                    var folderName = HttpContext.Server.MapPath("~/UserProfile/");
                    var filePath = $"{folderName}/{contactDetails.Contact.ImageName}";

                    try
                    {
                        System.IO.File.Delete(filePath);
                    }
                    catch (Exception)
                    {
                        
                    }

                }
                // If we got this far, something failed.
                ViewBag.errorMessage = "Your profile deleted successfully.";

                return View();
            }

            return View();
        }
    }
}
