﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using iCardWebApi.Models;
using iCardWebApi.Providers;
using iCardWebApi.Results;
using System.Text;
using Entities.iCard;
using BLLayer;
using OmeletteSoft.BLLayer;
using iCardWebApi.Atrributes;

namespace iCardWebApi.Controllers
{
    [LicenseAttribute]
    [Authorize]
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private const int ACTIVAITON_CODE_LENGTH = 6;
        private const string LocalLoginProvider = "Local";
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }


        // POST api/Account/Register
        [AllowAnonymous]
        [Route("Register")]
        public async Task<RegisterOutput> Register(RegisterBindingModel model)
        {
            var output = new RegisterOutput();
            if (!ModelState.IsValid)
            {
                output.Error.ErrorCode = -1;
                var sb = new StringBuilder();
                foreach(var item in ModelState)
                {
                    sb.AppendLine($"{item.Key}: {item.Value}");
                }
                output.Error.ErrorDetails = sb.ToString();
                return output;
            }

            var user = new ApplicationUser() { UserName = model.Login.Email, Email = model.Login.Email };

            IdentityResult result = await UserManager.CreateAsync(user, model.Login.Password);

            if (!result.Succeeded)
            {
                output.Error.ErrorCode = -1;
                output.Error.ErrorDetails = string.Join(Environment.NewLine, result.Errors);
                return output;
            }
            else
            {
                //Create contact and other details
                var userDetails = UserManager.FindByEmail(model.Login.Email);
                if(userDetails != null)
                {
                    var userId = new Guid(userDetails.Id);
                    //Save Contact details
                    iCardMgr.SaveContact(userId, model.ContactDetails.Contact.Prefix, model.ContactDetails.Contact.FirstName,
                        model.ContactDetails.Contact.MiddleName, model.ContactDetails.Contact.LastName, model.ContactDetails.Contact.Education,
                        model.ContactDetails.Contact.Occupation, null, model.ContactDetails.Contact.IsPrivate, model.ContactDetails.Contact.Encrypt,
                        model.ContactDetails.Contact.CompanyName);

                    iCardMgr.SaveContactCommunications(userId, model.ContactDetails.Communications);
                    iCardMgr.SaveContactSocials(userId, model.ContactDetails.Socials);

                    output.ContactDetails = BLLayer.iCardMgr.GetContactDetails(new Guid(userDetails.Id));
                    output.Error = ResendCode(userDetails.Id).Result;
                }
                else
                {
                    output.Error.ErrorCode = -1;
                    output.Error.ErrorDetails = "Failed to retrieve User Details";
                }
            }
            return output;
        }
        [HttpPost]
        [AllowAnonymous]
        [Route("Validate")]
        public async Task<ValidateOutput> ValidateUser(ValidateBindingModel model)
        {
            var output = new ValidateOutput();
            if (!ModelState.IsValid)
            {
                output.Error.ErrorCode = -1;
                var sb = new StringBuilder();
                foreach (var item in ModelState)
                {
                    sb.AppendLine($"{item.Key}: {item.Value}");
                }
                output.Error.ErrorDetails = sb.ToString();
                return output;
            }

            try
            {
                var userDetails = UserManager.FindByEmail(model.Email);
                var validUser = UserManager.CheckPassword(userDetails, model.Password);
                if (validUser)
                {
                    output.Username = userDetails.UserName;
                    output.EmailConfirmed = userDetails.EmailConfirmed;
                    output.ContactDetails = BLLayer.iCardMgr.GetContactDetails(new Guid(userDetails.Id));
                    return output;
                }
                else
                {
                    output.Error.ErrorCode = -1;
                    output.Error.ErrorDetails = "Failed to Validate User.";
                }
            }
            catch(Exception)
            {
                output.Error.ErrorCode = -1;
                output.Error.ErrorDetails = "Server Error. Please try again.";
            }

            return output;
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }
        
        [AllowAnonymous]
        [HttpGet]
        [Route("ResendCode")]
        public async Task<Error> ResendCode(string id)
        {
            var errorCode = new Error();
            if (string.IsNullOrEmpty(id))
            {
                errorCode.ErrorCode = -1;
                errorCode.ErrorDetails = "Invalid Input";
                return errorCode;
            }

            //Check if user is valid
            var applicationUser = UserManager.FindById(id);
            if (applicationUser == null)
            {
                errorCode.ErrorCode = -1;
                errorCode.ErrorDetails = "Invalid User. Failed to identify User.";
                return errorCode;
            }

            var saveActivationCodeResult = iCardMgr.SaveActivationCode(id, CommonMgr.GenerateCode(ACTIVAITON_CODE_LENGTH));
            //Send Activation code
            try
            {
                var smtp = new System.Net.Mail.SmtpClient();
                var host = smtp.Host;
                var ssl = smtp.EnableSsl;
                var port = smtp.Port;

                var credential = (smtp.Credentials as System.Net.NetworkCredential);
                var username = credential.UserName;
                var password = credential.Password;

                //MailHelper.SendMail(applicationUser.UserName, "iCard Activation Code", $"<html><body>Activation code for iCard: <h1>{saveActivationCodeResult.ActivationCode}</h1></body></html>", string.Empty);

                MailHelper.SendMail(applicationUser.UserName, "iBC Activation Code", $"<html><body>Activation code for iBC: <h1>{saveActivationCodeResult.ActivationCode}</h1></body></html>", string.Empty,
                    username, password, ssl, port, host);
            }
            catch (Exception ex)
            {
                errorCode.ErrorCode = -1;
                errorCode.ErrorDetails = $"Failed to send email. {ex.Message}. {ex.InnerException?.Message}";
                return errorCode;
            }
            finally { }

            errorCode.ErrorDetails = "Please check for your Email for Activation Code.";
            return errorCode;
        }

        [HttpPost]
        [Route("VerifyCode")]
        [AllowAnonymous]
        public async Task<Error> VerifyCode(VerifyCodeViewBindingModel model)
        {
            var result = new Error();
            if (!ModelState.IsValid)
            {
                result.ErrorCode = -1;
                var sb = new StringBuilder();
                foreach (var item in ModelState)
                {
                    sb.AppendLine($"{item.Key}: {item.Value}");
                }
                result.ErrorDetails = sb.ToString();
                return result;
            }

            var activationResult = BLLayer.iCardMgr.MarkActivationCode(model.UserId, model.Code);

            if (activationResult != null)
            {
                var userDetails = UserManager.FindById(model.UserId);
                if (userDetails != null)
                {
                    userDetails.EmailConfirmed = true;
                    var userUpdateResult = UserManager.Update(userDetails);
                    if (userUpdateResult.Succeeded)
                    {
                        result.ErrorCode = 0;
                        result.ErrorDetails = "Code validated successfully.";
                        return result;
                    }
                    else
                    {
                        result.ErrorCode = -1;
                        result.ErrorDetails = string.Join(". ", userUpdateResult.Errors);
                        return result;
                    }
                }
                else
                {
                    result.ErrorCode = -1;
                    result.ErrorDetails = "Failed to retrieve User details.";
                    return result;
                }
            }
            else
            {
                result.ErrorCode = -1;
                result.ErrorDetails = "Invalid User / Code.";
                return result;
            }
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("ForgotPasswordCode")]
        public async Task<Error> ForgotPasswordCode(string email)
        {
            var errorCode = new Error();
            if (string.IsNullOrEmpty(email))
            {
                errorCode.ErrorCode = -1;
                errorCode.ErrorDetails = "Invalid Input";
                return errorCode;
            }

            //Check if user is valid
            var applicationUser = UserManager.FindByEmail(email);
            if (applicationUser == null)
            {
                errorCode.ErrorCode = -1;
                errorCode.ErrorDetails = "Invalid User. Failed to identify User.";
                return errorCode;
            }

            var saveActivationCodeResult = iCardMgr.SaveActivationCode(applicationUser.Id, CommonMgr.GenerateCode(ACTIVAITON_CODE_LENGTH));
            //Send Activation code
            try
            {
                var smtp = new System.Net.Mail.SmtpClient();
                var host = smtp.Host;
                var ssl = smtp.EnableSsl;
                var port = smtp.Port;

                var credential = (smtp.Credentials as System.Net.NetworkCredential);
                var username = credential.UserName;
                var password = credential.Password;

                MailHelper.SendMail(applicationUser.UserName, "iBC: Forgot Password Code", $"<html><body>Forgot Password One Time Code for iBC: <h1>{saveActivationCodeResult.ActivationCode}</h1></body></html>", string.Empty,
                    username, password, ssl, port, host);
            }
            catch (Exception ex)
            {
                errorCode.ErrorCode = -1;
                errorCode.ErrorDetails = $"Failed to send email. {ex.Message}. {ex.InnerException?.Message}";
                return errorCode;
            }
            finally { }

            errorCode.ErrorDetails = "Please check for your Email for One Time Code and use it for Forgot Password screen.";
            return errorCode;
        }

        [HttpPost]
        [Route("SetPassword")]
        [AllowAnonymous]
        public async Task<Error> SetPassword(SetPasswordViewBindingModel model)
        {
            var result = new Error();
            if (!ModelState.IsValid)
            {
                result.ErrorCode = -1;
                var sb = new StringBuilder();
                foreach (var item in ModelState)
                {
                    sb.AppendLine($"{item.Key}: {item.Value}");
                }
                result.ErrorDetails = sb.ToString();
                return result;
            }
            //Check if user is valid
            var userDetails = UserManager.FindByEmail(model.Email);
            if (userDetails == null)
            {
                result.ErrorCode = -1;
                result.ErrorDetails = "Invalid User. Failed to identify User.";
                return result;
            }

            var activationResult = BLLayer.iCardMgr.MarkActivationCode(userDetails.Id, model.Code);

            if (activationResult != null)
            {
                string resetToken = await UserManager.GeneratePasswordResetTokenAsync(userDetails.Id);
                IdentityResult passwordChangeResult = await UserManager.ResetPasswordAsync(userDetails.Id, resetToken, model.NewPassword);

                if (passwordChangeResult.Succeeded)
                {
                    result.ErrorCode = 0;
                    result.ErrorDetails = "Password updated successfully.";
                    return result;
                }
                else
                {
                    result.ErrorCode = -1;
                    result.ErrorDetails = string.Join(". ", passwordChangeResult.Errors);
                    return result;
                }
            }
            else
            {
                result.ErrorCode = -1;
                result.ErrorDetails = "Invalid Code. Please check your Email for Code.";
                return result;
            }
        }

        #region Commented Methods
        //// GET api/Account/UserInfo
        //[HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        //[Route("UserInfo")]
        //public UserInfoViewModel GetUserInfo()
        //{
        //    ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

        //    return new UserInfoViewModel
        //    {
        //        Email = User.Identity.GetUserName(),
        //        HasRegistered = externalLogin == null,
        //        LoginProvider = externalLogin != null ? externalLogin.LoginProvider : null
        //    };
        //}


        //// GET api/Account/ManageInfo?returnUrl=%2F&generateState=true
        //[Route("ManageInfo")]
        //public async Task<ManageInfoViewModel> GetManageInfo(string returnUrl, bool generateState = false)
        //{
        //    IdentityUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

        //    if (user == null)
        //    {
        //        return null;
        //    }

        //    List<UserLoginInfoViewModel> logins = new List<UserLoginInfoViewModel>();

        //    foreach (IdentityUserLogin linkedAccount in user.Logins)
        //    {
        //        logins.Add(new UserLoginInfoViewModel
        //        {
        //            LoginProvider = linkedAccount.LoginProvider,
        //            ProviderKey = linkedAccount.ProviderKey
        //        });
        //    }

        //    if (user.PasswordHash != null)
        //    {
        //        logins.Add(new UserLoginInfoViewModel
        //        {
        //            LoginProvider = LocalLoginProvider,
        //            ProviderKey = user.UserName,
        //        });
        //    }

        //    return new ManageInfoViewModel
        //    {
        //        LocalLoginProvider = LocalLoginProvider,
        //        Email = user.UserName,
        //        Logins = logins
        //    };
        //}


        //// POST api/Account/Logout
        //[Route("Logout")]
        //public IHttpActionResult Logout()
        //{
        //    Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
        //    return Ok();
        //}

        //// POST api/Account/SetPassword
        //[Route("SetPassword")]
        //public async Task<IHttpActionResult> SetPassword(SetPasswordBindingModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);

        //    if (!result.Succeeded)
        //    {
        //        return GetErrorResult(result);
        //    }

        //    return Ok();
        //}

        //// POST api/Account/RemoveLogin
        //[Route("RemoveLogin")]
        //public async Task<IHttpActionResult> RemoveLogin(RemoveLoginBindingModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    IdentityResult result;

        //    if (model.LoginProvider == LocalLoginProvider)
        //    {
        //        result = await UserManager.RemovePasswordAsync(User.Identity.GetUserId());
        //    }
        //    else
        //    {
        //        result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(),
        //            new UserLoginInfo(model.LoginProvider, model.ProviderKey));
        //    }

        //    if (!result.Succeeded)
        //    {
        //        return GetErrorResult(result);
        //    }

        //    return Ok();
        //}

        #endregion

        #region Helpers

        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        private class ExternalLoginData
        {
            public string LoginProvider { get; set; }
            public string ProviderKey { get; set; }
            public string UserName { get; set; }

            public IList<Claim> GetClaims()
            {
                IList<Claim> claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.NameIdentifier, ProviderKey, null, LoginProvider));

                if (UserName != null)
                {
                    claims.Add(new Claim(ClaimTypes.Name, UserName, null, LoginProvider));
                }

                return claims;
            }

            public static ExternalLoginData FromIdentity(ClaimsIdentity identity)
            {
                if (identity == null)
                {
                    return null;
                }

                Claim providerKeyClaim = identity.FindFirst(ClaimTypes.NameIdentifier);

                if (providerKeyClaim == null || String.IsNullOrEmpty(providerKeyClaim.Issuer)
                    || String.IsNullOrEmpty(providerKeyClaim.Value))
                {
                    return null;
                }

                if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
                {
                    return null;
                }

                return new ExternalLoginData
                {
                    LoginProvider = providerKeyClaim.Issuer,
                    ProviderKey = providerKeyClaim.Value,
                    UserName = identity.FindFirstValue(ClaimTypes.Name)
                };
            }
        }

        private static class RandomOAuthStateGenerator
        {
            private static RandomNumberGenerator _random = new RNGCryptoServiceProvider();

            public static string Generate(int strengthInBits)
            {
                const int bitsPerByte = 8;

                if (strengthInBits % bitsPerByte != 0)
                {
                    throw new ArgumentException("strengthInBits must be evenly divisible by 8.", "strengthInBits");
                }

                int strengthInBytes = strengthInBits / bitsPerByte;

                byte[] data = new byte[strengthInBytes];
                _random.GetBytes(data);
                return HttpServerUtility.UrlTokenEncode(data);
            }
        }

        #endregion
    }
}
