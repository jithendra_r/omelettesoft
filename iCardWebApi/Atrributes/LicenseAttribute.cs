﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace iCardWebApi.Atrributes
{
    [AttributeUsageAttribute(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class LicenseAttribute : AuthorizeAttribute
    {
        public LicenseAttribute()
        {

        }

        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            var appKey = actionContext.Request.Headers.FirstOrDefault(e => e.Key.Equals("AppKey", StringComparison.InvariantCultureIgnoreCase));
            if(appKey.Value != null && appKey.Value.Any())
            {
                var licenseId = BLLayer.iCardMgr.ValidateAppKey(appKey.Value.FirstOrDefault());
                if(licenseId.HasValue && licenseId.Value > 0)
                {
                    return true;
                }
            }

            return false;
        }
        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            var result = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
            result.Content = new StringContent(JsonConvert.SerializeObject(new Models.Error() {ErrorCode=-1, ErrorDetails="Invalid License Key" }), Encoding.UTF8, "application/json");
            actionContext.Response = result;
        }
    }
}