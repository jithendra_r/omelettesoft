﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity.Owin;
using iCardWebApi.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;

namespace iCardWebApi.Atrributes
{
    [AttributeUsageAttribute(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class UserIdAuthorizeAttribute : AuthorizeAttribute
    {
        private ApplicationUserManager _userManager = null;
        public UserIdAuthorizeAttribute(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }
        public UserIdAuthorizeAttribute()
        {

        }
        public ApplicationUserManager UserManager
        {
            get
            {
                if(_userManager == null)
                {
                    var db = new ApplicationDbContext();
                    var userStore = new UserStore<ApplicationUser>(db);
                    _userManager = new ApplicationUserManager(userStore);

                }
                return _userManager;
            }
            private set
            {
                _userManager = value;
            }
        }
        //protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        //{

        //}
        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            var result = false;
            var userId = string.Empty;

            if (actionContext.Request.Headers.Contains("Authorization"))
                userId = actionContext.Request.Headers.Authorization.ToString();

            
            if (string.IsNullOrEmpty(userId) )
            {
                var userIdKey = actionContext.Request.Properties.Keys.FirstOrDefault(e=> e.Equals("userId", StringComparison.InvariantCultureIgnoreCase));
                if (!string.IsNullOrEmpty(userIdKey))
                {
                    userId = actionContext.Request.Properties[userIdKey].ToString();
                }
            }

            if (!string.IsNullOrEmpty(userId))
            {
                var userDetails = UserManager.FindByIdAsync(userId).Result;
                if (userDetails != null && userDetails.EmailConfirmed)
                {
                    actionContext.Request.Properties.Add("UserDetails", userDetails);
                    return true;
                }
                    
            }
            
            return result;
        }
        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            var result = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
            result.Content = new StringContent(JsonConvert.SerializeObject(new Models.Error() { ErrorCode = -1, ErrorDetails = "Failed to Authorize request." }), Encoding.UTF8, "application/json");
            actionContext.Response = result;
        }
    }
}