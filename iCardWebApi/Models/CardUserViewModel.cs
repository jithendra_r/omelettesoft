﻿using Entities.iCard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace iCardWebApi.Models
{
    public class CardUserViewModel
    {
        public CardUserViewModel()
        {
            this.ContactDetails = new ContactDetails();
            this.Error = new Error();
        }
        public ContactDetails ContactDetails { get; set; }
        public Error Error { get; set; }
    }

    public class CardDetailsViewModel
    {
        public CardDetailsViewModel()
        {
            this.ContactDetails = new CardDetails();
            this.Error = new Error();
        }
        public CardDetails ContactDetails { get; set; }
        public Error Error { get; set; }
    }

    public class CardUserCommunicationViewModel
    {
        public CardUserCommunicationViewModel()
        {
            this.Communications = new Communication();
            this.Error = new Error();
        }
        public Communication Communications { get; set; }
        public Error Error { get; set; }
    }

    public class TypesViewModel
    {
        public TypesViewModel()
        {
            this.Types = new Dictionary<short, string>();
            this.Error = new Error();
        }
        public Dictionary<short, string> Types { get; set; }
        public Error Error { get; set; }
    }
}