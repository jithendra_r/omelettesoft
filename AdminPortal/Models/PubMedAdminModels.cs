﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminPortal.Models
{
    public class PubMedDasboard
    {
        public long NumberOfUsers { get; set; }
        public long NumberOfPendingUsers { get; set; }
        public long NumberOfDevices { get; set; }
        public long NumberOfPendingDevices { get; set; }
        public long NumberOfFiles { get; set; }
        public long StorageSpaceOnServer { get; set; }
    }

    public class PubmedUserDetails
    {
        public string UserId { get; set; }
        public string Prefix { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
    }
}