﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace AdminPortal.Models
{
    public class ImpactFactorInput
    {
        public long Id { get; set; }
        public List<ImpactFactor> Factors { get; set; }
    }
    public class ImpactFactor
    {
        public int Year { get; set; }
        public string Factor { get; set; }
    }

    public class JournalHeader
    {
        public long? Id { get; set; }
        public string Title { get; set; }
        public string Title20 { get; set; }
        public string Title29 { get; set; }
        public string ISSN { get; set; }
        public string EISSN { get; set; }
        public string Category { get; set; }
        public string Publisher { get; set; }
        public string Country { get; set; }
        public string Language { get; set; }
        public string Website { get; set; }
        public string Frequency { get; set; }
        public string Society { get; set; }
        public string APC { get; set; }
        public string APCAmount { get; set; }
        public string Currency { get; set; }
        public string SubmissionFee { get; set; }
        public string Keywords { get; set; }
        public string ReviewProcess { get; set; }
        public string OpenAccess { get; set; }
    }

    public class JIFFiles {
        public IEnumerable<FileInfo> UploadedFiles { get; set; }
        public IEnumerable<FileInfo> ProcessedFiles { get; set; }
    }
}