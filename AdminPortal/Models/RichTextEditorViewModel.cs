﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminPortal.Models
{
    public class RichTextEditorViewModel
    {
        public string ApplicationName { get; set; }
        public string Subject { get; set; }
        [AllowHtml]
        [Display(Name = "Message")]
        public string Message
        {
            get;
            set;
        }
    }

    public class MailingDetails
    {
        public string ApplicationName { get; set; }
        public string Subject { get; set; }
        public string Message
        {
            get;
            set;
        }
    }
}