﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminPortal.Models
{
    public class Error
    {
        public Error()
        {

        }
        public Error(int id, string message)
        {
            this.Id = id;
            this.Message = message;
        }
        public int Id { get; set; }
        public string Message { get; set; }
    }
}