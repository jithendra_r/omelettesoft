﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminPortal.Models
{
    public class AssignCostViewModel
    {
        public int Cost { get; set; }
        public List<int> Ids { get; set; } 
    }
    public class ItemDetails
    {
        public short Id { get; set; }
        public string Value { get; set; }
        public bool IsActive { get; set; }
    }

    public class iCardCompanyDetails
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Logo { get; set; }
        public string Website { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
    
    public class CompanyContact
    {
        public int CompanyId { get; set; }
        public Guid? Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
    }
}