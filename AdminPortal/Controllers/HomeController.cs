﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminPortal.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (HttpContext.User.Identity.IsAuthenticated && HttpContext.User.IsInRole("Admin"))
            {
                var count = BLLayer.JIFMgr.GetMissingISSNCount();
                if (count > 0)
                {
                    ViewData["MissingISSNCount"] = string.Format("<span class='badge badge-primary'>{0}</span>", count);
                }
                
            }
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "OmeletteSoft.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Contact page.";

            return View();
        }
    }
}