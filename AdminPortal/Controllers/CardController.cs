﻿using AdminPortal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using System.IO;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;

namespace AdminPortal.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CardController : Controller
    {
        private ApplicationUserManager _userManager = null;
        private const int ACTIVAITON_CODE_LENGTH = 6;
        public ApplicationUserManager UserManager
        {
            get
            {
                if(_userManager == null)
                {

                    //var dbContext = new IdentityDbContext(System.Configuration.ConfigurationManager.ConnectionStrings["omelette_CardsEntities_Identity"].ConnectionString);
                    //var userStore = new UserStore<ApplicationUser>(dbContext);
                    //_userManager = new ApplicationUserManager(userStore);
                    _userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(new ApplicationDbContext(System.Configuration.ConfigurationManager.ConnectionStrings["omelette_CardsEntities_Identity"].ConnectionString)));

                    //_userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext(System.Configuration.ConfigurationManager.ConnectionStrings["omelette_CardsEntities_Identity"].ConnectionString)));
                }

                return _userManager;
            }
        }
        // GET: Card
        public ActionResult Index()
        {
            return View();
        }

        #region Templates
        public ActionResult Template()
        {

            return View();
        }

        public PartialViewResult GetTemplateList()
        {
            var listFromDB = BLLayer.iCardMgr.GetCardTemplates(0);
            return PartialView("_TemplateList", listFromDB);
        }
        public JsonResult RefreshTemplates()
        {
            var ftpFiles = OmeletteSoft.BLLayer.FTPMgr.DirectoryListSimple("/card.omeletteinc.com/Cards");
            var listFromDB = BLLayer.iCardMgr.GetCardTemplates(0);
            var cardTemplates = new List<Entities.iCard.CardTemplate>();

            foreach (var file in ftpFiles)
            {
                var dbItem = listFromDB.FirstOrDefault(e => e.CardTemplatePath.ToUpper().EndsWith(file.ToUpper()));
                if (dbItem == null && !string.IsNullOrEmpty(file))
                {
                    cardTemplates.Add(new Entities.iCard.CardTemplate()
                    {
                        CardTemplateId = 0,
                        CardTemplateName = file,
                        CardTemplatePath = $"http://card.omeletteinc.com/Cards/{file}",
                        Cost = 0
                    });
                }
            }

            foreach (var cardTemplate in cardTemplates)
            {
                BLLayer.iCardMgr.SaveCardTemplate(cardTemplate.CardTemplateId, cardTemplate.CardTemplateName, cardTemplate.CardTemplatePath, true, cardTemplate.Cost, 0);
            }
            listFromDB = BLLayer.iCardMgr.GetCardTemplates(0);
            return Json(new {Message="Refreshed Images from Server." });
        }
        [HttpPost]
        public JsonResult AssignCost(AssignCostViewModel input)
        {
            if(input.Cost >= 0)
            {
                if (input.Ids.Any())
                {
                    BLLayer.iCardMgr.AssignCostToCard(input.Cost, input.Ids.ToArray());
                    return Json(new { Message = "Assigned Cost successfully.", Id=0 });
                }
                else
                {
                    return Json(new { Message = "No files are selected.", Id = -1 });
                }
            }
            else
            {
                return Json(new { Message = "Invalid Cost.", Id = -1 });
            }
            
        }
        #endregion

        
        #region Communication Types
        public ActionResult CommunicationType()
        {
            return View();
        }
        public PartialViewResult GetCommunicationDetails(int id)
        {
            DALayer.GetCommunicationTypes_Result communicationTypeDetails = null;
            if (id > 0)
                communicationTypeDetails = BLLayer.iCardMgr.GetCommunicationTypes().FirstOrDefault(e => e.CommunicationTypeId == id);

            if (communicationTypeDetails == null)
                communicationTypeDetails = new DALayer.GetCommunicationTypes_Result() {
                    CommunicationTypeId = 0,
                    IsActive = true,
                    CommunicationTypeName = string.Empty
                };

            return PartialView("_CommunicationTypeDetails", communicationTypeDetails);
        }

        public PartialViewResult GetCommunicationTypeList()
        {
            var communicationTypes = BLLayer.iCardMgr.GetCommunicationTypes();
            return PartialView("_CommunicationTypeList", communicationTypes);
        }

        [HttpPost]
        public JsonResult SaveCommunicationType(ItemDetails item)
        {
            if(string.IsNullOrEmpty(item.Value))
            {
                return Json(new { Message = "Can't Save empty Communication Type.", Id = -1 });
            }
            BLLayer.iCardMgr.SaveCommunicationType(item.Id, item.Value, item.IsActive);
            return Json(new { Message = "Succesfully saved Communication Type.", Id = 0 });
        }
        #endregion

        #region Social
        
        public ActionResult SocialType()
        {
            return View();
        }
        public PartialViewResult GetSocialTypeDetails(int id)
        {
            DALayer.GetSocialTypes_Result socialTypes = null;
            if (id > 0)
                socialTypes = BLLayer.iCardMgr.GetSocialTypes().FirstOrDefault(e => e.SocialTypeId == id);

            if (socialTypes == null)
                socialTypes = new DALayer.GetSocialTypes_Result()
                {
                    SocialTypeId=0,
                    IsActive = true,
                    SocialTypeName = string.Empty
                };

            return PartialView("_SocialTypeDetails", socialTypes);
        }

        public PartialViewResult GetSocialTypeList()
        {
            var communicationTypes = BLLayer.iCardMgr.GetSocialTypes();
            return PartialView("_SocialTypeList", communicationTypes);
        }

        [HttpPost]
        public JsonResult SaveSocialType(ItemDetails item)
        {
            if (string.IsNullOrEmpty(item.Value))
            {
                return Json(new { Message = "Can't Save empty Social Type.", Id = -1 });
            }
            BLLayer.iCardMgr.SaveSocialType(item.Id, item.Value, item.IsActive);
            return Json(new { Message = "Succesfully saved Social Type.", Id = 0 });
        }
        #endregion

        #region Campanies
        public ActionResult Campanies()
        {
            return View();
        }
        public PartialViewResult GetCompanyDetails(int? id, string name)
        {
            var companies = BLLayer.iCardMgr.GetCompanies(id ?? 0, name ?? string.Empty);

            return PartialView("_CompaniesList", companies);
        }
        public PartialViewResult CompanyDetails(int id)
        {
            var companyDetails = new iCardCompanyDetails();
            if (id > 0)
            {
                companyDetails = BLLayer.iCardMgr.GetCompanies(id, string.Empty)
                    .Select(e=> new iCardCompanyDetails()
                    {
                        Id = e.CompanyId,
                        Name = e.Name,
                        Address= e.Address,
                        Logo = e.LogoImage,
                        Website = e.WebSite,
                        IsActive = e.IsActive.Value,
                        CreatedDate = e.CreateDate,
                        ModifiedDate = e.ModifiedDate
                    })
                    .FirstOrDefault();
            }

            if(companyDetails == null)
            {
                companyDetails = new iCardCompanyDetails()
                {
                    IsActive = true,
                    Id = 0
                };
            }
            return PartialView("_CompanyDetails", companyDetails);
        }
        [HttpPost]
        public JsonResult SaveCompany(iCardCompanyDetails company)
        {
            if (string.IsNullOrEmpty(company.Name))
            {
                return Json(new { Message = "Can't Save empty Company. Name is empty", Id = -1 });
            }
            if (string.IsNullOrEmpty(company.Address))
            {
                return Json(new { Message = "Can't Save empty Company. Address is empty", Id = -1 });
            }
            var userId = User.Identity.GetUserId();


            BLLayer.iCardMgr.SaveCompany(company.Id, company.Name, company.Address, company.Logo, company.Website, new Guid(userId));
            return Json(new { Message = "Succesfully saved Social Type.", Id = 0 });
        }
        public ActionResult ViewCompanyDetails(int id)
        {
            var companyDetails = BLLayer.iCardMgr.GetCompanies(id, string.Empty)
                    .Select(e => new iCardCompanyDetails()
                    {
                        Id = e.CompanyId,
                        Name = e.Name,
                        Address = e.Address,
                        Logo = e.LogoImage,
                        Website = e.WebSite,
                        IsActive = e.IsActive.Value,
                        CreatedDate = e.CreateDate,
                        ModifiedDate = e.ModifiedDate
                    })
                    .FirstOrDefault();
            return View(companyDetails);
        }

        public PartialViewResult CompanyCardList(int id)
        {
            var listFromDB = BLLayer.iCardMgr.GetCardTemplates(id);
            return PartialView("_CompanyCardTemplateList", listFromDB);
        }

        public PartialViewResult CompanyCardUpload(int id)
        {
            return PartialView("_CompanyCardUpload", id);
        }
        [HttpPost]
        public JsonResult CompanyCardUploadFile(int id)
        {
            var result = new List<string>();
            int totalProcessedCount = 0;
            try
            {
                var companyInfo = BLLayer.iCardMgr.GetCompanies(id, string.Empty).FirstOrDefault();
                if (companyInfo != null)
                {
                    var companyFolder = $"/card.omeletteinc.com/Cards/{companyInfo.CompanyUniqueId}";
                    OmeletteSoft.BLLayer.FTPMgr.CreateDirectory(companyFolder);

                    for (var i = 0; i < Request.Files.Count; i++)
                    {
                        var uploadedFile = Request.Files[i];

                        var _FileName = $"{Guid.NewGuid()}{Path.GetExtension(uploadedFile.FileName)}";
                        var _path = Path.Combine(Server.MapPath("~/UploadedFiles/Cards"), _FileName);
                        uploadedFile.SaveAs(_path);

                        result.Add(_path);

                        //Move file to processed folder
                        //System.IO.File.Move(_path, Path.Combine(Server.MapPath("~/UploadedFiles/Cards/Processed"), _FileName));
                    }
                    foreach(var filePathName in result)
                    {
                        var fileName = Path.GetFileName(filePathName);
                        //Move file to FTP folder
                        OmeletteSoft.BLLayer.FTPMgr.Upload($"{companyFolder}/{fileName}", filePathName);
                        BLLayer.iCardMgr.SaveCardTemplate(0, fileName, $"http://card.omeletteinc.com/Cards/{companyInfo.CompanyUniqueId}/{fileName}", true, 0, companyInfo.CompanyId);
                    }
                }
                else
                {
                    return Json(new { result = new { files = result, totalProcessedCount = 0, error = "Failed to retrieve Company Details." } });
                }
                

                //Lets start import process
                return Json(new { result = new { files = result, totalProcessedCount = totalProcessedCount } });
            }
            catch (Exception ex)
            {
                return Json(new { result = new { files = result, totalProcessedCount = totalProcessedCount, error = ex.Message } });
            }
        }

        public PartialViewResult CompanyUserList(int id, string userSearch="")
        {
            var listFromDB = BLLayer.iCardMgr.GetCompanyContacts(id, userSearch, null);
            return PartialView("_CompanyUserList", listFromDB);
        }
        public PartialViewResult CompanyUserAddEdit(int cid, Guid? uniqueId)
        {
            var contactDetails = BLLayer.iCardMgr.GetCompanyContacts(cid, string.Empty, uniqueId).FirstOrDefault();
            if(contactDetails == null)
            {
                contactDetails = new DALayer.GetCompanyContacts_Result();
                contactDetails.CompanyId = cid;
            }
            
            return PartialView("_CompanyUserAddEdit", contactDetails);
        }
        [HttpPost]
        public JsonResult SaveCompanyContact(CompanyContact companyContact)
        {
            var error = new Error();

            if (string.IsNullOrEmpty(companyContact.Email))
            {
                error.Id = -1;
                error.Message = "Can't Save Contact. Email is empty";
            }
            if (companyContact.CompanyId <=0)
            {
                error.Id = -1;
                error.Message = "Can't Save Contact. Company details missing";
            }

            var companyContactDetails = BLLayer.iCardMgr.GetCompanyContacts(companyContact.CompanyId, string.Empty, companyContact.Id).FirstOrDefault();

            if(companyContactDetails == null)
            {
                var applicationUser = UserManager.FindByEmail(companyContact.Email);
                if(applicationUser != null)
                {
                    error.Id = -1;
                    error.Message = "User already exists.";
                }
                else
                {
                    var user = new ApplicationUser() { UserName = companyContact.Email, Email = companyContact.Email };
                    
                    IdentityResult result = UserManager.Create(user, $"Password{companyContact.CompanyId}");

                    if (!result.Succeeded)
                    {
                        error.Id = -1;
                        error.Message = string.Join(Environment.NewLine, result.Errors);
                    }
                    else
                    {
                        //Create contact and other details
                        var userDetails = UserManager.FindByEmail(companyContact.Email);
                        if (userDetails != null)
                        {
                            var userId = new Guid(userDetails.Id);
                            //Save Contact details
                            BLLayer.iCardMgr.SaveContact(userId, string.Empty, companyContact.FirstName,
                                string.Empty, companyContact.LastName, string.Empty,
                                string.Empty, null, true, false,
                                string.Empty);

                            error = ResendCode(userDetails.Id);
                            if(error.Id == 0)
                            {
                                error.Message = "Succesfully add Contact to Company.";
                            }
                        }
                        else
                        {
                            error.Id = -1;
                            error.Message = "Failed to retrieve User Details";
                        }
                    }
                }
            }
            else
            {
                //Update new one
                error.Id = 0;
                error.Message = "Succesfully updated Contact to Company.";
            }
            return Json(error);
        }

        [HttpPost]
        public JsonResult CompanyUserUpload(int cid)
        {
            var error = new Error();

            return error;
        }
        public Error ResendCode(string id)
        {
            var errorCode = new Error();
            if (string.IsNullOrEmpty(id))
            {
                errorCode.Id = -1;
                errorCode.Message = "Invalid Input";
                return errorCode;
            }

            //Check if user is valid
            var applicationUser = UserManager.FindById(id);
            if (applicationUser == null)
            {
                errorCode.Id = -1;
                errorCode.Message = "Invalid User. Failed to identify User.";
                return errorCode;
            }

            var saveActivationCodeResult = BLLayer.iCardMgr.SaveActivationCode(id, OmeletteSoft.BLLayer.CommonMgr.GenerateCode(ACTIVAITON_CODE_LENGTH));
            //Send Activation code
            try
            {
                var smtp = new System.Net.Mail.SmtpClient();
                var host = smtp.Host;
                var ssl = smtp.EnableSsl;
                var port = smtp.Port;

                var credential = (smtp.Credentials as System.Net.NetworkCredential);
                var username = credential.UserName;
                var password = credential.Password;

                OmeletteSoft.BLLayer.MailHelper.SendMail(applicationUser.UserName, "iBC Activation Code", $"<html><body>Activation code for iBC: <h1>{saveActivationCodeResult.ActivationCode}</h1></body></html>", string.Empty,
                    username, password, ssl, port, host);
            }
            catch (Exception ex)
            {
                errorCode.Id = -1;
                errorCode.Message = $"Failed to send email. {ex.Message}. {ex.InnerException?.Message}";
                return errorCode;
            }
            finally { }

            errorCode.Message = "Please check for your Email for Activation Code.";
            return errorCode;
        }
        #endregion
    }
}