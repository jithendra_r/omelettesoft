﻿using AdminPortal.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminPortal.Controllers
{
    [Authorize(Roles="Admin")]
    public class JIFController : Controller
    {
        // GET: JIF
        public ActionResult Index()
        {
            return View(BLLayer.JIFMgr.GetJournalSummaryCount());
        }

        public PartialViewResult GetList(string searchString, int? count, string searchType)
        {
            searchString = searchString ?? " ";
            count = count ?? 10;
            var searchResult = new List<DALayer.SearchJournal_Result>();
            if(!string.IsNullOrEmpty(searchType) && searchType.Equals("TITLE", StringComparison.OrdinalIgnoreCase))
            {
                searchResult = BLLayer.JIFMgr.SearchJournalByTitle(searchString, count.Value)
                    .Select(e=> new DALayer.SearchJournal_Result() {
                        CountryName = e.CountryName,
                        EISSN = e.EISSN,
                        Frequency = e.Frequency,
                        ISSN = e.ISSN,
                        JournalCategoryName = e.JournalCategoryName,
                        JournalHeaderId = e.JournalHeaderId,
                        LanguageName = e.LanguageName,
                        PublisherName = e.PublisherName,
                        SocietyName = e.SocietyName,
                        Title = e.Title,
                        Title20 = e.Title20,
                        Title29 = e.Title29,
                        WebSite = e.WebSite
                    }).ToList();
            }
            else
            {
                searchResult = BLLayer.JIFMgr.SearchJournal(searchString, count.Value, 0, "Title", "ASC");
            }
            return PartialView("_JIFList", searchResult);
        }

        public PartialViewResult ImpactFactor(long id)
        {
            return PartialView("ImpactFactor", BLLayer.JIFMgr.GetJournalImpactFactor(id).ToDictionary(e=> e.JournalYear.Value, e=> e.ImpactFactor.Value));
        }
        public PartialViewResult DeleteItem(long id, string searchString, int? count, string searchType)
        {
            BLLayer.JIFMgr.DeleteJIFItem(id);
            return GetList(searchString, count, searchType);
        }

        public PartialViewResult Details(long id)
        {
            var input = BLLayer.JIFMgr.SearchJournal(string.Empty, 1, id, "Title", "ASC").FirstOrDefault() ?? new DALayer.SearchJournal_Result();
            return PartialView("Details", input);
        }

        [HttpPost]
        public void SaveImpactFactor(ImpactFactorInput input)
        {
            foreach (var ifItem in input.Factors)
            {
                decimal factor;
                if(!string.IsNullOrEmpty( ifItem.Factor) && decimal.TryParse(ifItem.Factor, out factor)){
                    BLLayer.JIFMgr.SaveJournalImpactFactors(input.Id, ifItem.Year, factor);
                }
            }
        }
        [HttpPost]
        public void SaveHeader(JournalHeader input)
        {
            BLLayer.JIFMgr.SaveJournalImpactFactorHeader(
                input.Title, input.Title20, input.Title29, input.ISSN, input.EISSN,
                input.Category, input.Country, input.Publisher, input.Language, input.Society,
                input.Website, input.Frequency, input.APC, input.APCAmount, input.Currency,
                input.SubmissionFee, input.Keywords, input.ReviewProcess, input.OpenAccess);
        }

        public ActionResult MissingISSNs()
        {
            var count = BLLayer.JIFMgr.GetMissingISSNCount();
            if (count > 0)
            {
                ViewData["MissingISSNCount"] = string.Format("<span class='badge badge-primary'>{0}</span>", count);
            }
            return View();
        }

        public PartialViewResult GetMissingISSNList(string searchString="")
        {
            return PartialView("_MissingISSNList", BLLayer.JIFMgr.GetMissingISSN(searchString));
        }

        [HttpDelete]
        public void MissingISSN(long id)
        {
            BLLayer.JIFMgr.DeleteMissingISSN(id);
        }

        #region Import
        public ActionResult Import()
        {
            //Retrieve recently processed and attempted files and path
            var procesedFolder = Server.MapPath("~/UploadedFiles/Processed");
            var uploadedFolder = Server.MapPath("~/UploadedFiles");
            List<FileInfo> processedFiles = null, uploadedFiles = null;
            DirectoryInfo directoryInfo = null ;

            if (Directory.Exists(procesedFolder))
            {
                directoryInfo = new DirectoryInfo(procesedFolder);
                processedFiles = directoryInfo.GetFiles().OrderByDescending(f => f.LastWriteTime).Take(5).ToList();
            }
            if (Directory.Exists(uploadedFolder))
            {
                directoryInfo = new DirectoryInfo(uploadedFolder);
                uploadedFiles = directoryInfo.GetFiles().OrderByDescending(f => f.LastWriteTime).Take(5).ToList();
            }
            return View(new JIFFiles { ProcessedFiles = processedFiles, UploadedFiles = uploadedFiles});
        }
        [HttpPost]
        public JsonResult UploadFile()
        {
            var result = new List<string>();
            int totalProcessedCount = 0;
            try
            {
                for(var i=0; i< Request.Files.Count; i++)
                {
                    var uploadedFile = Request.Files[i];

                    string _FileName = $"{DateTime.Now.ToString("MMddyyyy_HHmmss")}_{Path.GetFileName(uploadedFile.FileName)}";
                    string _path = Path.Combine(Server.MapPath("~/UploadedFiles"), _FileName);
                    uploadedFile.SaveAs(_path);

                    result.Add(_path);

                    totalProcessedCount = BLLayer.JIFMgr.ImportJournalImpactFactor(_path);

                    //Move file to processed folder
                    System.IO.File.Move(_path, Path.Combine(Server.MapPath("~/UploadedFiles/Processed"), _FileName));
                }

                //Lets start import process
                return Json( new { result = new { files= result, totalProcessedCount = totalProcessedCount } });
            }
            catch(Exception ex)
            {
                return Json(new { result = new { files = result, totalProcessedCount = totalProcessedCount, error = ex.Message } });
            }
        }
        #endregion
    }
}