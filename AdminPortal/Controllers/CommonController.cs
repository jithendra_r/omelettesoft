﻿using AdminPortal.Models;
using OmeletteSoft.BLLayer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminPortal.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CommonController : Controller
    {
        // GET: Common
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SendEmails()
        {
            ViewBag.Message = "Send Emails";

            return View(new RichTextEditorViewModel() { 
                Message = @"<p>AA</p><p><img style=""width: 400px;"" src=""http://www.reactiongifs.us/wp-content/uploads/2013/07/its_working_star_wars.gif""><br></p><p>BB<br></p>"
            });
        }

        [HttpPost]
        public string UploadImages()
        {

            for (var i = 0; i < Request.Files.Count; i++)
            {
                var uploadedFile = Request.Files[i];

                var _FileName = $"{Guid.NewGuid()}{Path.GetExtension(uploadedFile.FileName)}";
                var _path = Path.Combine(Server.MapPath("~/UploadedFiles/Mails"), _FileName);
                uploadedFile.SaveAs(_path);

                return $"{Request.Url.Scheme}://{Request.Url.Authority}{Url.Content("~")}UploadedFiles/Mails/{_FileName}";
            }

            return "";
        }
        [HttpPost]
        public JsonResult SendTestEmailToApplication(RichTextEditorViewModel mailMessageObject)
        //public JsonResult SendTestEmail(string input)
        {
            var error = new Error();
            //RichTextEditorViewModel message = new RichTextEditorViewModel();
            try
            {
                var smtp = new System.Net.Mail.SmtpClient();
                var credential = (smtp.Credentials as System.Net.NetworkCredential);
                var username = credential.UserName;
                var password = credential.Password;

                var users = new List<OmeletteSoft.Entities.Common.User>();

                switch (mailMessageObject.ApplicationName.ToUpper())
                {
                    case "PUBMED":
                        users = BLLayer.PubMedMgr.GetUsers(null);
                        break;
                    case "CARDS":
                        users = BLLayer.iCardMgr.GetUsers(null);
                        break;
                }

                foreach (var user in users)
                {
                    MailHelper.SendMail(user.Email, mailMessageObject.Subject, $"<html><body>{mailMessageObject.Message}</body></html>", string.Empty,
                    username, password);
                }

                error.Message = "Test Email sent succesfully.";
            }
            catch (Exception ex)
            {
                error.Message = ex.Message;
            }
            return Json(error);
        }
        [HttpPost]
        public JsonResult SendTestEmail(RichTextEditorViewModel mailMessageObject)
        //public JsonResult SendTestEmail(string input)
        {
            var error = new Error();
            //RichTextEditorViewModel message = new RichTextEditorViewModel();
            try
            {
                var smtp = new System.Net.Mail.SmtpClient();
                var credential = (smtp.Credentials as System.Net.NetworkCredential);
                var username = credential.UserName;
                var password = credential.Password;

                foreach(var toEmailAddress in ConfigMgr.TestEmails.Split(','))
                {
                    MailHelper.SendMail(toEmailAddress, mailMessageObject.Subject, $"<html><body>{mailMessageObject.Message}</body></html>", string.Empty,
                    username, password);
                }
                
                error.Message = "Test Email sent succesfully.";
            }
            catch(Exception ex)
            {
                error.Message = ex.Message;
            }
            return Json(error);
        }
    }
}