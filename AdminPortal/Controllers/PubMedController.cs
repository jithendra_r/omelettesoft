﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace AdminPortal.Controllers
{
    [Authorize(Roles = "Admin")]
    public class PubMedController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        public PubMedController()
        {
        }

        public PubMedController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        // GET: PubMed
        public ActionResult Index()
        {
            var model = new AdminPortal.Models.PubMedDasboard();
            foreach(var item in OmeletteSoft.BLLayer.AppUserMgr.GetPubMedAdminDashboard())
            {
                switch (item.MetricName.ToUpper())
                {
                    case "USERS_ACTIVE":
                        model.NumberOfUsers = item.MetricValue.Value;
                        break;
                    case "USERS_REGISTERED":
                        model.NumberOfPendingUsers = item.MetricValue.Value;
                        break;
                    case "DEVICES_ACTIVE":
                        model.NumberOfDevices = item.MetricValue.Value;
                        break;
                    case "DEVICES_REGISTERD":
                        model.NumberOfPendingDevices = item.MetricValue.Value;
                        break;
                    case "FILES_UPLOADED":
                        model.NumberOfFiles = item.MetricValue.Value;
                        break;
                    case "STORAGE_SIZE":
                        model.StorageSpaceOnServer = item.MetricValue.Value;
                        break;
                }
            }
            return View(model);
        }

        public ActionResult Users()
        {
            return View();
        }

        public PartialViewResult GetUserList(string searchString, int? count, string searchType)
        {
            searchString = searchString ?? "";
            count = count ?? 10;

            var context = new Models.ApplicationDbContext();

            var allUsers = context.Users.Where(e=> e.Email.Contains(searchString)).Take(count.Value).ToList();
            return PartialView("_UserList", allUsers);
        }
        public ActionResult UserDetails(Guid id)
        {
            var userDetails = UserManager.FindById(id.ToString());

            return View(userDetails);
        }
        public PartialViewResult GetUserInfo(string id)
        {
            var userInfo = BLLayer.PubMedMgr.GetUserDetails(id);
            return PartialView("_EditUserInfo", userInfo);
        }
        public ActionResult DeleteUser(Guid id)
        {
            try
            {
                var userDetails = UserManager.FindById(id.ToString());
                var result = UserManager.Delete(userDetails);
                var favorites = OmeletteSoft.BLLayer.AppUserMgr.GetAppUserFavorites(id.ToString(), null, null);
                OmeletteSoft.BLLayer.AppUserMgr.DeleteFavorites(id.ToString(), favorites.Select(e => e.FavoriteValue).ToList(), 0);
                if (result.Succeeded)
                    ViewBag.Success = $"UserName:{userDetails.UserName} successfully deleted.";
                else
                    ViewBag.Error = string.Join(". ", result.Errors);
            }
            catch(Exception ex)
            {
                ViewBag.Error = ex.Message;
            }
            return View("Users");
        }

        public PartialViewResult GetUserDeviceList(Guid id)
        {
            //var userDevices = OmeletteSoft.BLLayer.AppUserMgr.GetAppUserDevices(id);
            return PartialView("_UserDeviceList");
        }
        public PartialViewResult GetUserUploadList(Guid id)
        {
            var userDetails = OmeletteSoft.BLLayer.AppUserMgr.SearchUser(id, string.Empty, 1).FirstOrDefault();
            var userUploads = OmeletteSoft.BLLayer.AppUserMgr.GetAppUserFavorites(userDetails.Id, (short)OmeletteSoft.BLLayer.EnumMgr.FavoriteType.NotSet, null);
            userUploads.ForEach((e)=>{
                if (!string.IsNullOrEmpty(e.ImagePath) && e.ImagePath.Length > 20)
                    e.ImagePath = "<a href='http://pubmed.omeletteinc.com/" + e.ImagePath.Replace("~/", "") + "' target='_blank'>View</a>";

            });
            return PartialView("_UserUploadList", userUploads);
        }
        
        public JsonResult SaveUserDetails(Models.PubmedUserDetails userDetails)
        {
            BLLayer.PubMedMgr.SaveUserDetails(userDetails.UserId, userDetails.Prefix, userDetails.FirstName, userDetails.MiddleName, userDetails.LastName, null);
            return Json(new { Id=0, Message="Updated User Details."});
        }
    }
}